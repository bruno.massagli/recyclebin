import mongoose from 'mongoose'

export class Stocks {

  stocks():any {

    const stocks = new mongoose.Schema(
      {
        quantity: Number,
        reserved: Number,
        unit: String,
        createdAt: Date,
        updatedAt: Date
      }

      , { collection: 'stocks', timestamps: true }
    );
    return stocks

    }
    public stocksModel(): any {
        return mongoose.models.stocks || mongoose.model('stocks', this.stocks(), 'stocks')
    }
} 