import mongoose from 'mongoose'

export class Categories {

    public categories(): any {

        const categories = new mongoose.Schema({
            _id: mongoose.Schema.Types.ObjectId,
            company: String,
            id_category: Number,
            name: String,
            parent_id: String,
            createdAt: Date,
            updatedAt: Date

        }, { collection: 'categories', timestamps: true }
        );

        return categories

    }
    public categoriesModel(): any {
        return mongoose.models.categories || mongoose.model('categories', this.categories(), 'categories')
    }
}

