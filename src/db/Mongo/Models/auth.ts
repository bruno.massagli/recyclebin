import * as  mongoose from 'mongoose'
import {Customers} from './customers'



export class Auth {

  public auth():any {
    let customers = new Customers()
    const auth = new mongoose.Schema({
      _id : mongoose.Schema.Types.ObjectId,
      email: String,
      password: String,
      access_level: String,
      customer: customers.customers(),
      createdAt: Date,
      updatedAt: Date

    }, { collection: 'auth', timestamps: true }
    )
    return auth

  }
  public authModel():any {
    return mongoose.models.auth || mongoose.model('auth', this.auth(), 'auth')
  }

}