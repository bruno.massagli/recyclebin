import mongoose from 'mongoose'
import { Shippings } from './shippings'
import { Customers } from './customers'

export class Orders {

  orders(): any {
    let shippings = new Shippings()
    let customers = new Customers()

    const orders = new mongoose.Schema({
      _id: mongoose.Schema.Types.ObjectId,
      company: String,
      order_number: String,
      order_date: Date,
      total_gross: Number,
      status: String,
      shipping: shippings.shippings(),
      customer: customers.customers(),
      createdAt: Date,
      updatedAt: Date
    }, { collection: 'orders', timestamps: true }
    );
    return orders

    }
    public ordersModel(): any {
        return mongoose.models.orders || mongoose.model('orders', this.orders(), 'orders')
    }
}
