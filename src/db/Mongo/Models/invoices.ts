import mongoose from 'mongoose'
import { Items } from './items'

export class Invoices {

  public invoices(): any {
    let items_invoice = new Items()
    const invoices = new mongoose.Schema({
      _id: mongoose.Schema.Types.ObjectId,
      key: String,
      issue_date: Date,
      items: [items_invoice.items()],
      createdAt: Date,
      updatedAt: Date

    }, { collection: 'invoices', timestamps: true }
    );
    return invoices

    }
    public invoicesModel(): any {
        return mongoose.models.invoices || mongoose.model('invoices', this.invoices(), 'invoices')
    }
}

