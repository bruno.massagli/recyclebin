import * as  mongoose from 'mongoose'
import { Address } from './adresses'
import { Images } from './images'
import { Document } from './document'
import { Customers } from './customers'



export class Companies {

  public companies(): any {
    let customers = new Customers()
    let address = new Address()
    let images = new Images()
    let document = new Document()
    const companies = new mongoose.Schema({
      _id: mongoose.Schema.Types.ObjectId,
      name: String,
      email: String,
      api_key: String,
      document: document.document(),
      phones: [String],
      address: address.address(),
      avatar: images.images(),
      customers: [customers.customers()],
      createdAt: Date,
      updatedAt: Date

    }, { collection: 'companies', timestamps: true }
    );

    return companies

  }
  public companiesModel(): any {
    return mongoose.models.companies || mongoose.model('companies', this.companies(), 'companies')
  }

}