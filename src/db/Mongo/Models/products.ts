import mongoose from 'mongoose'
import {Variations} from './variations'
import {Categories} from './categories'
export class Products {

  products(): any {
    let variations = new Variations()
    let categories = new Categories()
    const products = new mongoose.Schema({
      _id : mongoose.Schema.Types.ObjectId,
      name: String,
      company: String,
      id_product: String,
      brand: String,
      description: String,
      categories: [categories.categories()],
      warranty: String,
      variations: [variations.variations()],
      createdAt: Date,
      updatedAt: Date

    }, { collection: 'products', timestamps: true }
    );
    return products

    }
    public productsModel(): any {
        return mongoose.models.products || mongoose.model('products', this.products(), 'products')
    }
}
