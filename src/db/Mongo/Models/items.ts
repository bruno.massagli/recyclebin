import mongoose from 'mongoose'

export class Items {

  public items():any {

    const items = new mongoose.Schema({
      _id : mongoose.Schema.Types.ObjectId,
      sku: String,
      name: String,
      quantity: Number,
      original_price: Number,
      special_price: Number,
      createdAt: Date,
      updatedAt: Date

    }, { collection: 'items', timestamps: true }
    );
    return items

    }
    public itemsModel(): any {
        return mongoose.models.items || mongoose.model('items', this.items(), 'items')
    }
}
