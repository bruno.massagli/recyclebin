import mongoose from 'mongoose'

export class Specifications {

  specifications():any {

    const specifications = new mongoose.Schema({
      key: String,
      value: String,
      createdAt: Date,
      updatedAt: Date

    }, { collection: 'specifications', timestamps: true }
    );
    return specifications

    }
    public specificationsModel(): any {
        return mongoose.models.specifications || mongoose.model('specifications', this.specifications(), 'specifications')
    }
} 