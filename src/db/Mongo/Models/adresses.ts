import mongoose from 'mongoose'

export class Address {

  public address(): any {
    const address = new mongoose.Schema({
      street: String,
      number: Number,
      detail: String,
      neighborhood: String,
      city: String,
      region: String,
      country: String,
      postcode: String,
      createdAt: Date,
      updatedAt: Date

    }, { collection: 'address', timestamps: true }
    );
    
    return address
  }
  public addressModel():any {
    return mongoose.models.address || mongoose.model('address', this.address(), 'address')
  }
}