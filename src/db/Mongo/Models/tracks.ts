import mongoose from 'mongoose'
import {Items} from './items'

export class Tracks {

  tracks():any {
    let items_tracks = new Items()
    const tracks = new mongoose.Schema({
      _id : mongoose.Schema.Types.ObjectId,
      key: String,
      tracking_link: String,
      tracking_number: String,
      items: [items_tracks.items()],
      createdAt: Date,
      updatedAt: Date

    }, { collection: 'tracks', timestamps: true }
    );
    return tracks

    }
    public tracksModel(): any {
        return mongoose.models.tracks || mongoose.model('tracks', this.tracks(), 'tracks')
    }
} 