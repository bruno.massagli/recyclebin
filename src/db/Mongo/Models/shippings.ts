import mongoose from 'mongoose'
import { Items } from './items'
import { Tracks } from './tracks'
import { Invoices } from './invoices'
import { Address } from './adresses'

export class Shippings {

    shippings(): any {
        let items = new Items()
        let tracks = new Tracks()
        let invoices = new Invoices()
        let address = new Address()
        const shippings = new mongoose.Schema({
            _id : mongoose.Schema.Types.ObjectId,
            carrier: String,
            shipping_method: String,
            shipping_date: Date,
            shipping_code: String,
            shipping_cost: Number,
            shipping_estimated_delivery: Date,
            delivery_date: Date,
            shipping_address: address.address(),
            items: [items.items()],
            tracks: [tracks.tracks()],
            invoices: [invoices.invoices()]
        }, { collection: 'shippings', timestamps: true }
        );
        return shippings

    }
    public shippingsModel(): any {
        return mongoose.models.shippings || mongoose.model('shippings', this.shippings(), 'shippings')
    }
}


