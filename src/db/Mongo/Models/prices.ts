import mongoose from 'mongoose'

export class Prices {

    prices():any {

        const prices = new mongoose.Schema(
            {
                list_price: Number,
                sale_price: Number,
                createdAt: Date,
                updatedAt: Date
            }

            , { collection: 'prices', timestamps: true }
        );
        return prices

    }
    public pricesModel(): any {
        return mongoose.models.prices || mongoose.model('prices', this.prices(), 'prices')
    }
} 