import * as  mongoose from 'mongoose'
import {Address} from './adresses'
import {Images} from './images'
import {Document} from './document'

export class Customers {
  public customers():any {
    let address = new Address()
    let images = new Images()
    let document = new Document()
    const customers = new mongoose.Schema({
      _id : mongoose.Schema.Types.ObjectId,
      id_customer: Number,
      name: String,
      email: String,
      document: document.document(),
      phones: [String],
      address: address.address(),
      avatar: images.images(),
      createdAt: Date,
      updatedAt: Date

    }, { collection: 'customers', timestamps: true }
    );
      return customers

  }
  public customersModel():any {
    return mongoose.models.customers || mongoose.model('customers', this.customers(), 'customers')
  }
}