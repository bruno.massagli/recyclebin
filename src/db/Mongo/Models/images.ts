import mongoose from 'mongoose'

export class Images {

  public images():any {

    const images = new mongoose.Schema({
      id: String,
      name: String,
      url: String,
      createdAt: Date,
      updatedAt: Date

    }, { collection: 'images', timestamps: true }
    );
    return images

    }
    public imagesModel(): any {
        return mongoose.models.images || mongoose.model('images', this.images(), 'images')
    }
} 