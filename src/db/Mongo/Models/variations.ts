import mongoose from 'mongoose'
import {Images} from './images'
import {Videos} from './videos'
import {Specifications} from './specifications'
import {Stocks} from './stocks'
import {Prices} from './prices'
import {Dimensions} from './dimensions'
export class Variations {

  variations():any {
    let images = new Images()
    let videos = new Videos()
    let specifications = new Specifications()
    let stocks = new Stocks()
    let prices = new Prices()
    let dimensions = new Dimensions()

    const variations = new mongoose.Schema(
      {
        _id : mongoose.Schema.Types.ObjectId,
        id_product: String,
        sku: String,
        stock: stocks.stocks(),
        ean: String,
        nbm: String,
        prices: prices.prices(),
        dimensions: dimensions.dimensions(),
        images: [images.images()],
        videos: [videos.videos()],
        specifications: [specifications.specifications()],
        createdAt: Date,
        updatedAt: Date
      }, { collection: 'variations', timestamps: true }
    );
    return variations

    }
    public variationsModel(): any {
        return mongoose.models.variations || mongoose.model('variations', this.variations(), 'variations')
    }
} 