import mongoose from 'mongoose'

export class Document {

    public document(): any {

        const document = new mongoose.Schema({
            number: String,
            type: String

        }, { collection: 'document', timestamps: true }
        );
        
        return document

    }
    public documentModel(): any {
        return mongoose.models.document || mongoose.model('document', this.document(), 'document')
    }
}

