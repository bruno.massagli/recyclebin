import mongoose from 'mongoose'
export class Videos {

  videos(): any {

    const videos = new mongoose.Schema({
      id: String,
      name: String,
      url: String,
      createdAt: Date,
      updatedAt: Date

    }, { collection: 'videos', timestamps: true }
    );

    return videos

  }
  public videosModel(): any {
    return mongoose.models.videos || mongoose.model('videos', this.videos(), 'videos')
  }
}