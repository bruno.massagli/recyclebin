import mongoose from 'mongoose'

export class Dimensions {

    public dimensions(): any {

        const dimensions = new mongoose.Schema({
            weight: Number,
            length: Number,
            width: Number,
            height: Number,
            cubic_wheight: Number,
            createdAt: Date,
            updatedAt: Date

        }, { collection: 'dimensions', timestamps: true }
        );
        return dimensions

    }
    public dimensionsModel(): any {
        return mongoose.models.dimensions || mongoose.model('dimensions', this.dimensions(), 'dimensions')
    }
}