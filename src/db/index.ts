import { Env } from '../env'
import mongoose from 'mongoose'
import { Sequelize } from 'sequelize'

export class IndexDB {

  public mongoose: any
  public sequelize: any
  constructor() {
    let environment = new Env()

    this.mongoose = mongoose.connect(environment.db.uri, {useNewUrlParser: true})
    // this.mongoose = mongoose.connect(`mongodb://${environment.db.host}:${environment.db.port}/${environment.db.dbname}`, {useNewUrlParser: true})

    // this.sequelize = new Sequelize(environment.db.dbname, environment.db.user, environment.db.password, {
    //   host: environment.db.host,
    //   dialect: environment.db.dialect,
    //   logging: false,
    //   pool: {
    //     max: 5,
    //     min: 0,
    //     acquire: 30000,
    //     idle: 10000
    //   }
    // })

  }
}
