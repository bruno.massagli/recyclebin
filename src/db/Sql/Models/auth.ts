import { IndexDB } from '../../index'
import * as Sequelize from 'sequelize'

export class Company {
  public company(): any {
    let databases = new IndexDB()

    const Company = databases.sequelize.define('company', {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      name: {
        type: Sequelize.STRING
      },
      api_key: {
        type: Sequelize.STRING
      },
      document_number: {
        type: Sequelize.STRING
      },
      document_type: {
        type: Sequelize.STRING
      },
      street: {
        type: Sequelize.STRING
      },
      number: {
        type: Sequelize.INTEGER,
      },
      detail: {
        type: Sequelize.STRING
      },
      neighborhood: {
        type: Sequelize.STRING
      },
      city: {
        type: Sequelize.STRING
      },
      region: {
        type: Sequelize.STRING
      },
      country: {
        type: Sequelize.STRING
      },
      postcode: {
        type: Sequelize.STRING
      }
    }, {
        timestamps: true
      })
    return Company
  }
}

