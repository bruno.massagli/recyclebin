import {Companies} from '../../db/Mongo/Models/companies'
// import {Company} from '../../db/Sql/Models/auth'
import lodash from 'lodash'
export async function auth (req, res, next): Promise<any> {
    let company_model = new Companies()
    const API_KEY = req.get('api-key')
    /// Database setup needed
    if (!API_KEY) {
        res.status(401).send('EMPTY_API_KEY')
        throw new Error('EMPTY_API_KEY')
    }
    const company_obj = await company_model.companiesModel().findOne( {'api_key': API_KEY} ).exec()
    const company = lodash.replace(company_obj._id, /(?:ObjectId\(\")|(?:\))/, "")
    console.log(company)
    if(!company){
        res.status(403).send('FORBIDDEN')
        throw new Error('FORBIDDEN')
    }
    req = Object.assign(req, {company: company})

    next()
}
