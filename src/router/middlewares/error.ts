import {Errors} from '../../helpers/errors'

export async function error (err, req, res, next): Promise<any>  {
  let errors = new Errors()
  if (err) {
    const response = errors.errors[err.type] || errors.errors['DEFAULT']

    if (err.descriptor) {
      response.body = JSON.parse(JSON.stringify(response.body, err.descriptor))
    }

    res.status(err.status || 500)
    res.jsonp(response)
  }

  next()
}
