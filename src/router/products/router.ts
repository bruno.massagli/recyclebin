import * as express from 'express'
import {GetAllProductsService} from '../../services/products/get_all_products'
import {GetProductsByIdService} from '../../services/products/get_products_by_id'
import {GetProductsService} from '../../services/products/get_products'
import {PutProductsService} from '../../services/products/put_products'
import {PostProductsService} from '../../services/products/post_products'
import {auth} from '../middlewares/auth'
import {error} from '../middlewares/error'

export class ProductsRouter {
    public router: any
    constructor() {

        const router_products = express.Router()

        router_products.get('/', function () {
            return true
        })

        router_products.get('/products/:search', [auth, error], GetAllProductsService)
        router_products.get('/product/:id', [auth, error], GetProductsByIdService)
        router_products.get('/products', [auth, error], GetProductsService)
        router_products.put('/products', [auth, error], PutProductsService)
        router_products.post('/products', [auth, error], PostProductsService)
        this.router = router_products
    }
}

