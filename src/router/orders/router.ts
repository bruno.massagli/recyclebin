import * as express from 'express'
import {GetOrdersService} from '../../services/orders/get_orders'
import {GetCustomerByEmailService} from '../../services/orders/get_customer_by_email'
import {GetOrdersByIdService} from '../../services/orders/get_orders_by_id'
import {PostOrderService} from '../../services/orders/post_order';
import {PutOrdersCancelledService} from '../../services/orders/put_order_cancelled'
import {PutOrdersInvoicedService} from '../../services/orders/put_order_invoiced'
import {PutOrdersSentService} from '../../services/orders/put_order_sent'
import {PutOrdersDeliveredService} from '../../services/orders/put_order_delivered'
import {auth} from '../middlewares/auth'
import {error} from '../middlewares/error'

export class OrdersRouter {
    public router: any
    constructor() {
        const router_orders = express.Router()

        router_orders.get('/', function () {
            return true
        })

        router_orders.get('/customer/:email', [auth, error], GetCustomerByEmailService)
        router_orders.get('/orders/:id', [auth, error], GetOrdersByIdService)
        router_orders.get('/orders', [auth, error], GetOrdersService)
        router_orders.post('/orders', [auth, error], PostOrderService)
        router_orders.put('/orders/cancelled', [auth, error], PutOrdersCancelledService)
        router_orders.put('/orders/invoiced', [auth, error], PutOrdersInvoicedService)
        router_orders.put('/orders/sent', [auth, error], PutOrdersSentService)
        router_orders.put('/orders/delivered', [auth, error], PutOrdersDeliveredService)
        this.router = router_orders
    }
}
