import * as express from 'express'
import {GetCategoryByIdService} from '../../services/categories/get_category_by_id'
import {GetCategoriesService} from '../../services/categories/get_categories'
import {PutCategoryService} from '../../services/categories/put_category'
import {PostCategoryService} from '../../services/categories/post_category'
import {auth} from '../middlewares/auth'
import {error} from '../middlewares/error'

export class CategoriesRouter {
    public router: any
    constructor() {

        const router_categories = express.Router()
        router_categories.get('/', function () {
            return true
        })

        router_categories.get('/categories/:id', [auth, error], GetCategoryByIdService)
        router_categories.get('/categories', [auth, error], GetCategoriesService)
        router_categories.put('/categories', [auth, error], PutCategoryService)
        router_categories.post('/categories', [auth, error], PostCategoryService)
        this.router = router_categories

    }
}
