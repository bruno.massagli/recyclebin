import * as express from 'express'
import {getAllCompaniesService} from '../../services/auth/get_all_companies'
import {getCompanyByIdService} from '../../services/auth/get_company_by_id'
import {getAuthByIdService} from '../../services/auth/get_auth_by_customer_id'
import {putCompanyService} from '../../services/auth/put_company'
import {putAuthService} from '../../services/auth/put_auth'
import {postCompanyService} from '../../services/auth/post_company'
import {postAuthService} from '../../services/auth/post_auth'
import {auth} from '../middlewares/auth'
import {error} from '../middlewares/error'
import { loginService } from '../../services/auth/login';

export class AuthRouter {
    public router: any
    constructor() {

        const router_auth = express.Router()

        router_auth.get('/health', function (req, res, next) {
            res.send(true)
        })

        router_auth.get('/login', [auth, error], loginService)
        router_auth.get('/auth/:id', [auth, error], getAuthByIdService)
        router_auth.put('/auth', [auth, error], putAuthService)
        router_auth.post('/auth', [auth, error], postAuthService)

        router_auth.get('/company', [auth, error], getAllCompaniesService)
        router_auth.get('/company/:id', [auth, error], getCompanyByIdService)
        router_auth.put('/company', [auth, error], putCompanyService)
        router_auth.post('/company', [auth, error], postCompanyService)
        this.router = router_auth
    }
}

