
import {PutCompanyLib} from '../../core/auth/lib/put_company_lib'

export async function putCompanyService (req, res, next): Promise<any>  {


    const company = req.company
    let body = req.body     

    let putCompany = new PutCompanyLib()
    const company_res = await putCompany.putCompany(company, body, true)

    
    return res.jsonp(company_res)
}