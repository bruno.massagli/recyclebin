
import {PostCompanyLib} from '../../core/auth/lib/post_company_lib'

export async function postCompanyService (req, res, next): Promise<any>  {


    const company = req.company
    let body = req.body     

    let postCompany = new PostCompanyLib()
    const company_res = await postCompany.postCompany(company, body, true)

    
    return res.jsonp(company_res)
}