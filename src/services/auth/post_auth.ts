
import {PostAuthLib} from '../../core/auth/lib/post_auth_lib'

export async function postAuthService (req, res, next): Promise<any>  {


    const company = req.company
    let body = req.body     

    let postAuth = new PostAuthLib()
    const auth = await postAuth.postAuth(company, body, true)

    
    return res.jsonp(auth)
}