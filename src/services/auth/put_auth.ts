
import {PutAuthLib} from '../../core/auth/lib/put_auth_lib'

export async function putAuthService (req, res, next): Promise<any>  {


    const company = req.company
    let body = req.body     

    let putAuth = new PutAuthLib()
    const auth = await putAuth.putAuth(company, body, true)

    
    return res.jsonp(auth)
}