
import {login} from '../../core/auth/lib/login'

export async function loginService (req, res, next): Promise<any>  {


    const company = req.company
    let {email, password} = req.body     

    const auth = await login(company, email, password, true)

    
    return res.jsonp(auth)
}