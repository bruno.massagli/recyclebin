
import {GetCompaniesLib} from '../../core/auth/lib/get_companies_lib'

export async function getAllCompaniesService (req, res, next): Promise<any>  {
    const company = req.company
    const per_page = Number(req.query.per_page)
    const page = Number(req.query.page)
    let getCompanies = new GetCompaniesLib()
    const companies_res = await getCompanies.getCompanies(company, per_page, page, true)
    
    res.jsonp(companies_res)
}