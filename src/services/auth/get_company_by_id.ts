
import {GetCompanyLib} from '../../core/auth/lib/get_company_lib'

export async function getCompanyByIdService (req, res, next): Promise<any>  {
    const company = req.company
    const id = req.params.id
    let getCompanyById = new GetCompanyLib()
    const company_res = await getCompanyById.getCompanyById(company, id, true)

    res.jsonp(company_res)
}