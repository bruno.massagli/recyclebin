
import {GetAuthLib} from '../../core/auth/lib/get_auth_lib'

export async function getAuthByIdService (req, res, next): Promise<any>  {
    const company = req.company
    const id = req.params.id
    let getAuthById = new GetAuthLib()
    const auth = await getAuthById.GetAuthByCustomer(company, id, true)

    res.jsonp(auth)
}