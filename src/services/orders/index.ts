const getOrdersById = require('../../built/core/orders/lib/get_order_by_id_lib')
const getOrders = require('../../built/core/orders/lib/get_orders_lib')
const putOrdersStatus = require('../../built/core/orders/lib/put_order_status_lib')
const get_orders = require('./get_orders')
const get_orders_by_id = require('./get_orders_by_id')
const put_order_status_cancelled = require('./put_order_cancelled')
const put_order_status_delivered = require('./put_order_delivered')
const put_order_status_invoiced = require('./put_order_invoiced')
const put_order_status_sent = require('./put_order_sent')

module.exports = {
    get_orders: get_orders,
    get_orders_by_id: get_orders_by_id,
    put_order_status_cancelled: put_order_status_cancelled,
    put_order_status_delivered: put_order_status_delivered,
    put_order_status_invoiced: put_order_status_invoiced,
    put_order_status_sent: put_order_status_sent
}