
import {PostOrderLib} from '../../core/orders/lib/post_order_lib'

export async function PostOrderService (req, res, next): Promise<any>  {
    

    const company = req.company
    let body = req.body     

    const postOrders = new PostOrderLib()
    const orders = await postOrders.PostOrder(company, body, true)

    
    return res.jsonp(orders)
}