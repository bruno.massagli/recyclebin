
import {GetOrdersLib} from '../../core/orders/lib/get_orders_lib'

export async function GetOrdersService (req, res, next): Promise<any>  {
    const company = req.company
    const page = Number(req.query.page)
    const per_page = Number(req.query.per_page)
    const { status, start_date, end_date } = req.query
    const getOrders = new GetOrdersLib()
    const orders = await getOrders.getOrders(company, status, per_page, page, start_date, end_date, true)

    res.jsonp(orders)
}