
import {PutOrderStatusLib} from '../../core/orders/lib/put_order_status_lib'

export async function PutOrdersInvoicedService (req, res, next): Promise<any>  {


    const company = req.company

    let body = req.body     

    const putOrdersStatus = new PutOrderStatusLib()
    const orders = await putOrdersStatus.putOrdersStatus(company, 'invoiced', body, true)

    
    return res.jsonp(orders)
}