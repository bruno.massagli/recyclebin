
import {GetOrderByIdLib} from '../../core/orders/lib/get_order_by_id_lib'

export async function GetOrdersByIdService (req, res, next): Promise<any>  {
    const company = req.company
    const id = req.params.id
    const getOrdersById = new GetOrderByIdLib()
    const orders = await getOrdersById.getOrdersById(company, id, true)

    res.jsonp(orders)
}