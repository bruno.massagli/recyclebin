
import {GetCustomerLib} from '../../core/orders/lib/get_customers_by_email_lib'

export async function GetCustomerByEmailService (req, res, next): Promise<any>  {
    const company = req.company
    const email = req.params.email
    const getCustomersByEmail = new GetCustomerLib()
    const customer = await getCustomersByEmail.getCustomerByEmail(email, true)

    res.jsonp(customer)
}