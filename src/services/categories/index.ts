const getCategoryById = require('../../built/core/categories/lib/get_category_by_id_lib')
const getCategories = require('../../built/core/categories/lib/get_categories_lib')
const postCategory = require('../../built/core/categories/lib/post_category_lib')
const putCategory = require('../../built/core/categories/lib/put_category_lib')
const get_categories = require('./get_categories')
const get_category_by_id = require('./get_category_by_id')
const post_category = require('./post_category')
const put_category = require('./put_category')
module.exports = {
    get_categories: get_categories,
    get_category_by_id: get_category_by_id,
    post_category: post_category,
    put_category: put_category
}