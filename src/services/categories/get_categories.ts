
import {GetCategoriesLib} from '../../core/categories/lib/get_categories_lib'

export async function GetCategoriesService (req, res, next): Promise<any>  {
    const company = req.company
    const page = Number(req.query.page)
    const per_page = Number(req.query.per_page)
    const getCategories = new GetCategoriesLib()
    const categories = await getCategories.getCategories(company, per_page, page)
    
    
    res.jsonp(categories)
}