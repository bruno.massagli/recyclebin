
import {PostCategoryLib} from '../../core/categories/lib/post_category_lib'

export async function PostCategoryService (req, res, next): Promise<any>  {
    

    const company = req.company
    let body = req.body     
    body['rest'] = true

    const postCategory = new PostCategoryLib()
    const categories = await postCategory.postCategory(company, body)

    
    return res.jsonp(categories)
}