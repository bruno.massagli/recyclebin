
import {GetCategoryByIdLib} from '../../core/categories/lib/get_category_by_id_lib'

export async function GetCategoryByIdService (req, res, next): Promise<any>  {
    const company = req.company
    const id = req.params.id
    const getCategoryById = new GetCategoryByIdLib()
    const categories = await getCategoryById.getCategoryById(company, id)

    res.jsonp(categories)
}