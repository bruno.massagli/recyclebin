import {PutCategoryLib} from '../../core/categories/lib/put_category_lib'


export async function PutCategoryService (req, res, next): Promise<any>  {
    

    const company = req.company
    let body = req.body     
    body['rest'] = true

    const putCategory = new PutCategoryLib()
    const categories = await putCategory.putCategory(company, body)

    
    return res.jsonp(categories)
}