
import {GetProductByIdLib} from '../../core/products/lib/get_product_by_id_lib'

export async function GetProductsByIdService (req, res, next)  {
    const company = req.company
    const id = req.params.id
    let getProductsById = new GetProductByIdLib()
    const products = await getProductsById.getProductsById(company, id, true)

    res.jsonp(products)
}