
import {PutProductStocksLib} from '../../core/products/lib/put_product_stocks_lib'

export async function PutStocksProductsService (req, res, next)  {

    const company = req.company
    let body = req.body
    let putProductsStocks = new PutProductStocksLib()
    const products = await putProductsStocks.putProductsStocks(company, body, true)

    return res.jsonp(products)
}