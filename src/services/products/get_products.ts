
import {GetProductLib} from '../../core/products/lib/get_product_lib'

export async function GetProductsService (req, res, next)  {
    const company = req.company
    const page = Number(req.query.page)
    const per_page = Number(req.query.per_page)
    const { start_date, end_date } = req.query
    let getProducts = new GetProductLib()
    const products = await getProducts.getProducts(company, per_page, page, start_date, end_date, true)
    
    res.jsonp(products)
}