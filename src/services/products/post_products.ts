
import {PostProductLib} from '../../core/products/lib/post_product_lib'

export async function PostProductsService (req, res, next)  {


    const company = req.company
    let body = req.body     

    let postProducts = new PostProductLib()
    const products = await postProducts.postProducts(company, body, true)

    return res.jsonp(products)
}