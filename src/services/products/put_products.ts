
import {PutProductlib} from '../../core/products/lib/put_product_lib'

export async function PutProductsService (req, res, next)  {


    const company = req.company
    let body = req.body     

    let putProducts = new PutProductlib()
    const products = await putProducts.putProducts(company, body, true)

    return res.jsonp(products)
}