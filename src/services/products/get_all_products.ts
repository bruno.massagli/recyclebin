
import {GetAllProductsLib} from '../../core/products/lib/get_all_products_lib'

export async function GetAllProductsService (req, res, next)  {

    const page = Number(req.query.page)
    const per_page = Number(req.query.per_page)
    const search = req.params.search
    let getProducts = new GetAllProductsLib()
    const products = await getProducts.getAllProducts(search, per_page, page, true)

    
    res.jsonp(products)
}