
import {PutProductPricesLib} from '../../core/products/lib/put_product_prices_lib'

export async function PutPricesProductsService (req, res, next)  {


    const company = req.company
    let body = req.body     

    let putProductsPrices = new PutProductPricesLib()
    const products = await putProductsPrices.putProductsPrices(company, body, true)

    return res.jsonp(products)
}