const getProducts = require('../../built/core/products/lib/get_product_lib')
const getProductsById = require('../../built/core/products/lib/get_product_by_id_lib')
const postProducts = require('../../built/core/products/lib/post_product_lib')
const putProductsPrices = require('../../built/core/products/lib/put_product_prices_lib')
const putProducts = require('../../built/core/products/lib/put_product_lib')
const putProductsStocks = require('../../built/core/products/lib/put_product_stocks_lib')

const get_products = require('./get_products')
const get_products_by_id = require('./get_products_by_id')
const post_products = require('./post_products')
const put_products = require('./put_products')
const put_prices_products = require('./put_prices_products')
const put_stocks_products = require('./put_stocks_products')

module.exports = {
    get_products: get_products,
    get_products_by_id: get_products_by_id,
    post_products: post_products,
    put_products: put_products,
    put_prices_products: put_prices_products,
    put_stocks_products: put_stocks_products
}