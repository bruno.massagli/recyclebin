import * as moment_tz from 'moment-timezone'

/**
 * @description Configured singleton {MomentTimezone}
 * @module utils/moment
 * @version v.1.0
 * @returns {MomentTimezone} with fixed configured timezone
 */
module.exports = moment_tz.tz.setDefault('America/Sao_Paulo')
