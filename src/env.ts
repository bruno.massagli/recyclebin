export class Env {
    public db: any
    constructor() {
        this.db = {
            dbname: process.env.DB_NAME,
            uri: process.env.DB_URI,
            user: process.env.DB_USER,
            password: process.env.DB_PASS,
            port: process.env.DB_PORT,
            host: process.env.DB_HOST,
            dialect: process.env.DB_DIALECT
        }
    }
} 