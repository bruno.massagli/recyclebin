import { CustomerType, CustomerInput, CustomerUpdateInput } from '../orders/graphql/types/customers_type';
import { DocumentsType, DocumentsInput, DocumentsUpdateInput } from '../orders/graphql/types/documents_type';
import { AddressType, AddressInput, AddressUpdateInput } from '../orders/graphql/types/addresses_type';
import { CompanyType, AuthType, AuthInput, CompanyInput, AuthUpdateInput, CompanyUpdateInput } from '../auth/graphql/types/auth_types';
import { CategoriesType, CategoriesInput, CategoriesUpdateInput } from '../categories/graphql/types/categories_type';
import { StocksType, StocksInput, StocksUpdateInput } from '../products/graphql/types/stocks_type';
import { PricesType, PricesInput, PricesUpdateInput } from '../products/graphql/types/prices_type';
import { DimensionsType, DimensionsInput, DimensionsUpdateInput } from '../products/graphql/types/dimensions_type';
import { ImagesType, ImagesInput, ImagesUpdateInput } from '../products/graphql/types/images_type';
import { VideosType, VideosInput, VideosUpdateInput } from '../products/graphql/types/videos_type';
import { SpecificationsType, SpecificationsInput, SpecificationsUpdateInput } from '../products/graphql/types/specifications_type';
import { VariationsType, VariationsInput, VariationsUpdateInput } from '../products/graphql/types/vatriations_type';
import { ProductsType, ProductsInput, ProductsUpdateInput } from '../products/graphql/types/products_type';
import { ItemsType, ItemsInput, ItemsUpdateInput } from '../orders/graphql/types/items_type';
import { TracksType, TracksInput, TracksUpdateInput } from '../orders/graphql/types/tracks_type';
import { InvoicesType, InvoicesInput, InvoicesUpdateInput } from '../orders/graphql/types/invoices_type';
import { ShippingsType, ShippingsInput, ShippingsUpdateInput } from '../orders/graphql/types/shippings_type';
import { OrdersType, OrdersInput, OrdersUpdateInput, OrderStatusInput } from '../orders/graphql/types/orders_type';
import { GraphQLDate, GraphQLDateTime } from 'graphql-iso-date'
import { MyContext } from '../auth/mocks/MyContext'
import { auth_query, auth_mutation } from '../auth/graphql/resolver/auth_graphql';
import { products_query, products_mutation } from '../products/graphql/resolver/products_graphql';
import { orders_query, orders_mutation } from '../orders/graphql/resolver/orders_graphql';
import { categories_query, categories_mutation } from '../categories/graphql/resolver/categories_graphql';
import { OrdersCancelInput } from '../orders/graphql/types/orders_cancel_type';
import { OrdersDeliverInput } from '../orders/graphql/types/orders_deliver_type';
import { OrdersInvoiceInput } from '../orders/graphql/types/orders_invoice_type';
import { OrdersSentInput } from '../orders/graphql/types/orders_sent_type';

export const types_graphql = `
scalar MyContext
scalar Date  
scalar DateTime

${DocumentsType}
${DocumentsInput}
${DocumentsUpdateInput}
${AddressType}
${AddressInput}
${AddressUpdateInput}
${CustomerType}
${CustomerInput}
${CustomerUpdateInput}
${AuthType}
${AuthInput}
${AuthUpdateInput}
${CompanyType}
${CompanyInput}
${CompanyUpdateInput}
${CategoriesType}
${CategoriesInput}
${CategoriesUpdateInput}
${StocksType}
${StocksInput}
${StocksUpdateInput}
${PricesType}
${PricesInput}
${PricesUpdateInput}
${DimensionsType}
${DimensionsInput}
${DimensionsUpdateInput}
${ImagesType}
${ImagesInput}
${ImagesUpdateInput}
${VideosType}
${VideosInput}
${VideosUpdateInput}
${SpecificationsType}
${SpecificationsInput}
${SpecificationsUpdateInput}
${VariationsType}
${VariationsInput}
${VariationsUpdateInput}
${ProductsType}
${ProductsInput}
${ProductsUpdateInput}
${ItemsType}
${ItemsInput}
${ItemsUpdateInput}
${TracksType}
${TracksInput}
${TracksUpdateInput}
${InvoicesType}
${InvoicesInput}
${InvoicesUpdateInput}
${ShippingsType}
${ShippingsInput}
${ShippingsUpdateInput}
${OrdersType}
${OrdersInput}
${OrdersUpdateInput}
${OrderStatusInput}
type Query {
    ${auth_query}
    ${products_query}
    ${orders_query}
    ${categories_query}
}
  
  type Mutation {
    ${auth_mutation}
    ${products_mutation}
    ${orders_mutation}
    ${categories_mutation}
}
`