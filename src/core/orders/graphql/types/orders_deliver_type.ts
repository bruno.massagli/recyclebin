import { InputType, Field } from 'type-graphql';

@InputType()
export class OrdersDeliverData {

    @Field(type => String)
    order_number: string
    
    @Field(type => Date)
    delivered_date: Date
    
    
}

export const OrdersDeliverInput = `

input OrdersDeliverInput {
    order_number: String!
    delivered_date: Date!
}
`