import { ObjectType, Field } from 'type-graphql'
import {ItemsData, ItemsType, ItemsInput} from './items_type'
import { buildSchema } from 'graphql'

@ObjectType()
export class InvoicesData {

    @Field(type => String)
    _id: String

    @Field(type => String)
    key: String
    
    @Field(type => Date)
    issue_date: Date

    @Field(type => [ItemsData])
    items: ItemsData[]

}

export const InvoicesType = `


type InvoicesType {
    _id : ID!
    key: String
    issue_date: Date
    items: [ItemsType]
}
`

export const InvoicesUpdateInput = `


input InvoicesUpdateInput {
    _id : ID!
    key: String
    issue_date: Date
    items: [ItemsUpdateInput]
}
`

export const InvoicesInput = `


input InvoicesInput {
    _id : ID
    key: String
    issue_date: Date
    items: [ItemsInput!]!
}
`

// export const InvoicesSchema = buildSchema(InvoicesType)
// export const InvoicesInputSchema = buildSchema(InvoicesInput)
