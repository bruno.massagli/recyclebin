import { InputType, Field } from 'type-graphql';

@InputType()
export class OrdersInvoiceData {

    @Field(type => String)
    order_number: string
    
    @Field(type => String)
    key: string
    
    @Field(type => String)
    number: string
    
    @Field(type => Date)
    invoiced_date: Date
    
}

export const OrdersInvoiceInput = `

input OrdersInvoiceInput {
    order_number: String!
    key: String!
    number: String!
    invoiced_date: Date!
    items: [ItemsUpdateInput]

}
`