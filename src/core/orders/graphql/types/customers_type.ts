import { ObjectType, Field } from 'type-graphql'
import { DocumentsData, DocumentsType, DocumentsInput } from './documents_type'
import { AddressData, AddressType, AddressInput } from './addresses_type'
import { buildSchema } from 'graphql'

@ObjectType()
export class CustomerData {

    @Field(type => String)
    _id: String

    @Field(type => String)
    name: String

    @Field(type => String)
    email: String

    @Field(type => DocumentsData)
    document: DocumentsData

    @Field(type => [String])
    phones: String[]

    @Field(type => AddressData)
    address: AddressData

}

export const CustomerType = `

type CustomerType {
    _id : ID!
    name: String
    email: String
    document: DocumentsType
    phones: [String]
    address: AddressType
}
`

export const CustomerUpdateInput = `

input CustomerUpdateInput {
    _id : ID!
    name: String
    email: String
    document: DocumentsUpdateInput
    phones: [String]
    address: AddressUpdateInput
}
`

export const CustomerInput = `

input CustomerInput {
    _id : ID
    name: String!
    email: String!
    document: DocumentsInput!
    phones: [String!]!
    address: AddressInput!
}
`

// export const CustomerSchema = buildSchema(CustomerType)
// export const CustomerInputSchema = buildSchema(CustomerInput)
