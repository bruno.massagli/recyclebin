import { ObjectType, Field } from 'type-graphql'
import { buildSchema } from 'graphql'

@ObjectType()
export class ItemsData {

    @Field(type => String)
    _id: String

    @Field(type => String)
    sku: String

    @Field(type => String)
    name: String

    @Field(type => Number)
    quantity: number

    @Field(type => Number)
    original_price: number

    @Field(type => Number)
    special_price: number

}

export const ItemsType = `
type ItemsType {
    _id : ID!
    sku: String
    name: String
    quantity: Float
    original_price: Float
    special_price: Float
}
`

export const ItemsUpdateInput = `
input ItemsUpdateInput {
    _id : ID!
    sku: String
    name: String
    quantity: Float
    original_price: Float
    special_price: Float
}
`

export const ItemsInput = `
input ItemsInput {
    _id : ID
    sku: String!
    name: String!
    quantity: Float!
    original_price: Float!
    special_price: Float!
}
`

// export const ItemsSchema = buildSchema(ItemsType)
// export const ItemsInputSchema = buildSchema(ItemsInput)
