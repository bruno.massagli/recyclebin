import { ObjectType, Field } from 'type-graphql'
import {ItemsData, ItemsType, ItemsInput} from './items_type'
import { buildSchema } from 'graphql'

@ObjectType()
export class TracksData {

    @Field(type => String)
    _id: String

    @Field(type => String)
    tracking_link: String

    @Field(type => String)
    tracking_number: String

    @Field(type => [ItemsData])
    items: ItemsData[]

}

export const TracksType = `

type TracksType {
    _id : ID!
    tracking_link: String
    tracking_number: String
    items: [ItemsType]
}
`

export const TracksUpdateInput = `

input TracksUpdateInput {
    _id : ID!
    tracking_link: String
    tracking_number: String
    items: [ItemsUpdateInput]
}
`

export const TracksInput = `

input TracksInput {
    _id : ID
    tracking_link: String
    tracking_number: String
    items: [ItemsInput!]!
}
`

// export const TracksSchema = buildSchema(TracksType)
// export const TracksInputSchema = buildSchema(TracksInput)
