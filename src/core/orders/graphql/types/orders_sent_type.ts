import { InputType, Field } from 'type-graphql';

@InputType()
export class OrdersSentData {

    @Field(type => String)
    order_number: string

    @Field(type => String)
    carrier: string

    @Field(type => String)
    method: string

    @Field(type => String)
    tracking_number: string

    @Field(type => String)
    tracking_link: string

    @Field(type => Date)
    date_delivery: Date

    @Field(type => Date)
    estimated_delivery: Date
}

export const OrdersSentInput = `

input OrdersSentInput {
    order_number: String!
    carrier: String!
    method: String!
    tracking_number: String!
    tracking_link: String!
    date_delivery: Date!
    estimated_delivery: Date!
    items: [ItemsUpdateInput]

}
`