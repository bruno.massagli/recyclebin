import { ObjectType, Field } from 'type-graphql'
import {ShippingsData, ShippingsType, ShippingsInput} from './shippings_type'
import {CustomerData, CustomerType, CustomerInput} from './customers_type'
import { buildSchema } from 'graphql'

@ObjectType()
export class OrdersData {

    @Field(type => String)
    _id: String
    
    @Field(type => String)
    company: String

    @Field(type => String)
    order_number: String

    @Field(type => Date)
    order_date: Date

    @Field(type => Number)
    total_gross: Number

    @Field(type => String)
    status: String

    @Field(type => ShippingsData)
    shipping: ShippingsData

    @Field(type => CustomerData)
    customer: CustomerData

}

export const OrdersType = `

type OrdersType {
    _id : ID!
    company: String
    order_number: String
    order_date: Date
    total_gross: Float
    status: String
    shipping: ShippingsType
    customer: CustomerType
}
`

export const OrdersUpdateInput = `

input OrdersUpdateInput {
    _id : ID!
    company: String
    order_number: String
    order_date: Date
    total_gross: Float
    status: String
    shipping: ShippingsUpdateInput
    customer: CustomerUpdateInput
}
`

export const OrdersInput = `

input OrdersInput {
    _id : ID
    company: String!
    order_number: String!
    order_date: Date!
    total_gross: Float!
    status: String!
    shipping: ShippingsInput!
    customer: CustomerInput!
}
`

export const OrderStatusInput = `

input OrderStatusInput {
    order_number: String!
    reason_cancelled: String
    key: String
    number: String
    invoiced_date: Date
    carrier: String
    method: String
    tracking_number: String
    tracking_link: String
    date_delivery: Date
    estimated_delivery: Date
    delivered_date: Date
    items: [ItemsUpdateInput]

}
`

// export const OrdersSchema = buildSchema(OrdersType)
// export const OrdersInputSchema = buildSchema(OrdersInput)
