import { ObjectType, Field } from 'type-graphql'
import { AddressData, AddressType, AddressInput } from './addresses_type'
import { ItemsData, ItemsType, ItemsInput } from './items_type'
import { TracksData, TracksType, TracksInput } from './tracks_type'
import { InvoicesData, InvoicesType, InvoicesInput } from './invoices_type'
import { buildSchema } from 'graphql'

@ObjectType()
export class ShippingsData {

    @Field(type => String)
    _id: String

    @Field(type => String)
    carrier: String

    @Field(type => String)
    shipping_method: String

    @Field(type => Date)
    shipping_date: Date

    @Field(type => String)
    shipping_code: String

    @Field(type => Number)
    shipping_cost: number

    @Field(type => Date)
    shipping_estimated_delivery: Date

    @Field(type => Date)
    delivery_date: Date

    @Field(type => AddressData)
    shipping_address: AddressData

    @Field(type => [ItemsData])
    items: ItemsData[]

    @Field(type => [TracksData])
    tracks: TracksData[]

    @Field(type => [InvoicesData])
    invoices: InvoicesData[]

}

export const ShippingsType = `

type ShippingsType {
    _id : ID!
    carrier: String
    shipping_method: String
    shipping_date: Date
    shipping_code: String
    shipping_cost: Float
    shipping_estimated_delivery: Date
    delivery_date: Date
    shipping_address: AddressType
    reason_cancelled: String
    items: [ItemsType]
    tracks: [TracksType]
    invoices: [InvoicesType]
}
`

export const ShippingsUpdateInput = `

input ShippingsUpdateInput {
    _id : ID!
    carrier: String
    shipping_method: String
    shipping_date: Date
    shipping_code: String
    shipping_cost: Float
    shipping_estimated_delivery: Date
    delivery_date: Date
    shipping_address: AddressUpdateInput
    reason_cancelled: String
    items: [ItemsUpdateInput]
    tracks: [TracksUpdateInput]
    invoices: [InvoicesUpdateInput]
}
`

export const ShippingsInput = `

input ShippingsInput {
    _id : ID
    carrier: String!
    shipping_method: String!
    shipping_date: Date!
    shipping_code: String!
    shipping_cost: Float!
    shipping_estimated_delivery: Date!
    delivery_date: Date!
    shipping_address: AddressInput!
    reason_cancelled: String
    items: [ItemsInput!]!
    tracks: [TracksInput]!
    invoices: [InvoicesInput]!
}
`
// export const ShippingsSchema = buildSchema(ShippingsType)
// export const ShippingsInputSchema = buildSchema(ShippingsInput)
