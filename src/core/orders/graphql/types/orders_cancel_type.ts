import { InputType, Field } from 'type-graphql';

@InputType()
export class OrdersCancelData {

    @Field(type => String)
    order_number: string;

    @Field(type => String)
    reason_cancelled: string;
}

export const OrdersCancelInput = `

input OrdersCancelInput {
    order_number: String!
    reason_cancelled: String!
}
`