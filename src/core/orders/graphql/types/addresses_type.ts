import { ObjectType, Field } from 'type-graphql'
import { buildSchema } from 'graphql'

@ObjectType()
export class AddressData {

    @Field(type => String)
    street: String

    @Field(type => Number)
    number: number

    @Field(type => String)
    detail: String

    @Field(type => String)
    neighborhood: String

    @Field(type => String)
    city: String

    @Field(type => String)
    region: String

    @Field(type => String)
    country: String

    @Field(type => String)
    postcode: String

}

export const AddressType = `
type AddressType {    
    street: String
    number: Int
    detail: String
    neighborhood: String
    city: String
    region: String
    country: String
    postcode: String
}
`

export const AddressUpdateInput = `
input AddressUpdateInput {    
    street: String
    number: Int
    detail: String
    neighborhood: String
    city: String
    region: String
    country: String
    postcode: String
}
`

export const AddressInput = `
input AddressInput {    
    street: String!
    number: Int!
    detail: String!
    neighborhood: String!
    city: String!
    region: String!
    country: String!
    postcode: String!
}
`

// export const AddressSchema = buildSchema(AddressType)
// export const AddressInputSchema = buildSchema(AddressInput)

