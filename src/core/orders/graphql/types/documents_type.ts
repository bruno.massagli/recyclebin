import { ObjectType, Field } from 'type-graphql'
import { buildSchema } from 'graphql'

@ObjectType()
export class DocumentsData {

    @Field(type => String)
    number: String

    @Field(type => String)
    type: String

}

export const DocumentsType = `
type DocumentsType {
    number: String
    type: String
}
`

export const DocumentsUpdateInput = `
input DocumentsUpdateInput {
    number: String
    type: String
}
`

export const DocumentsInput = `
input DocumentsInput {
    number: String!
    type: String!
}
`

// export const DocumentsSchema = buildSchema(DocumentsType)
// export const DocumentsInputSchema = buildSchema(DocumentsInput)
