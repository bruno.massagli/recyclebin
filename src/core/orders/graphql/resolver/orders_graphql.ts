import { GetOrderByIdLib } from '../../lib/get_order_by_id_lib'
import { GetOrdersLib } from '../../lib/get_orders_lib'
import { GetCustomerLib } from '../../lib/get_customers_by_email_lib'
import { PostOrderLib } from '../../lib/post_order_lib'
import { PutOrderStatusLib } from '../../lib/put_order_status_lib'
import { Resolver, Query, Mutation, Arg } from 'type-graphql'
import { OrdersData, OrdersInput, OrdersType } from '../types/orders_type'
import { Company } from '../../../auth/mocks/Company'
import { buildSchema } from 'graphql'
import { CompanyType } from '../../../auth/graphql/types/auth_types'
import { CustomerType, CustomerInput } from '../types/customers_type'
import { GraphQLDate, GraphQLDateTime } from 'graphql-iso-date'
import { DocumentsType, DocumentsInput } from '../types/documents_type'
import { AddressType, AddressInput } from '../types/addresses_type'
import { ItemsType, ItemsInput } from '../types/items_type'
import { TracksType, TracksInput } from '../types/tracks_type'
import { InvoicesType, InvoicesInput } from '../types/invoices_type'
import { ShippingsType, ShippingsInput } from '../types/shippings_type'
import { ImagesType, ImagesInput } from '../../../products/graphql/types/images_type';

export const resolverOrders = {
  Query: {
    get_customer_by_email: async (parent, { email }, context, info) => {
      let customer = new GetCustomerLib()
      return customer.getCustomerByEmail(email)
    },  
    get_order_by_id: async (parent, { company, id }, context, info) => {
      let order = new GetOrderByIdLib()
      return order.getOrdersById(company, id)
    },
    get_orders: async (parent, { company, status, per_page, page, date_start, date_end }, context, info) => {
      let order = new GetOrdersLib()
      return await order.getOrders(company, status, per_page, page, date_start, date_end)
    }
  },
  Mutation: {
    create_orders: async (parent, { company, body }, context, info) => {
      let order = new PostOrderLib()
      return await order.PostOrder(company, body)
    },
    update_orders: async (parent, { company, status, body }, context, info) => {
      let order = new PutOrderStatusLib()
      return await order.putOrdersStatus(company, status, body)
    }
  }
}

export const orders_query = `
  get_customer_by_email(email: String): CustomerType

  get_order_by_id(company: String, id: String): OrdersType

  get_orders(company: String, status: String, per_page: Int, page: Int, date_start: Date, date_end: Date): [OrdersType]
`
export const orders_mutation = `
  create_orders(company: String, body: OrdersInput): OrdersType

  update_orders(company: String, status: String, body: OrderStatusInput): OrdersType
`
// export const graphqlOrders = `

// ${ImagesType}
// ${DocumentsType}
// ${AddressType}
// ${CustomerType}
// ${CompanyType}
// ${ItemsType}
// ${TracksType}
// ${InvoicesType}
// ${ShippingsType}
// ${OrdersType}

// ${ImagesInput}
// ${DocumentsInput}
// ${AddressInput}
// ${CustomerInput}
// ${ItemsInput}
// ${TracksInput}
// ${InvoicesInput}
// ${ShippingsInput}
// ${OrdersInput}
// scalar Date  
// scalar DateTime

// type Query {
//   get_customer_by_email(email: String): CustomerType

//   get_order_by_id(company: String, id: String): [OrdersType]

//   get_orders(company: String, status: String, per_page: Int, page: Int, date_start: Date, date_end: Date): OrdersType
// }

// type Mutation {
//   create_orders(company: String, body: OrdersInput): String

//   update_orders(company: String, status: String, body: String): String
// }
// `

// export const schemaOrders = buildSchema(graphqlOrders)