export interface OrderSend {
    order_number: string;
    carrier: string;
    method: string;
    tracking_number: string;
    tracking_link: string;
    date_delivery: Date;
    estimated_delivery: Date;
}
