export interface Item {
    _id : String;
    sku: string;
    name: string;
    quantity: number;
    original_price: number; 
    special_price: number;
}
