import {Item} from './Item';

export interface Invoice {
    _id : String;
    key: string;
    issue_date: Date;
    items: Item[];
}
