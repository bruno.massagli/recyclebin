import {Documents} from './Documents';
import {Address} from './Address';


export interface Customer {
    _id : String;
    name: string;
    email: string;
    document: Documents;
    phones: string[];
    address: Address;
}

