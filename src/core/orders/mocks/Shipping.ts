import {Address} from './Address';
import {Item} from './Item';
import {Track} from './Track';
import {Invoice} from './Invoice';


export interface Shipping {
    _id : String;
    carrier: string;
    shipping_method: string;
    shipping_date: Date;
    shipping_code: string;
    shipping_cost: number;
    shipping_estimated_delivery: Date;
    delivery_date: Date;
    shipping_address: Address;
    reason_cancelled: String;
    items: Item[];
    tracks: Track[];
    invoices: Invoice[];
}
