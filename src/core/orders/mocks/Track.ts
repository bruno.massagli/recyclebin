import {Item} from './Item';


export interface Track {
    _id : String;
    tracking_link: string;
    tracking_number: string;
    items: Item[];
}
