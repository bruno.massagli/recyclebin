import {AddressInputPost, AddressInputPut} from './address_inputs';
import {ItemsInputPost, ItemsInputPut} from './item_inputs';
import {TracksInputPost, TracksInputPut} from './track_inputs';
import {InvoicesInputPost, InvoicesInputPut} from './invoice_inputs';
import { IsISO8601, IsCurrency, IsDefined, IsNumber, IsOptional } from 'class-validator';

export class ShippingsInputPost {

    @IsDefined()
    carrier: string

    @IsDefined()
    shipping_method: string
    
    @IsISO8601()
    @IsDefined()
    shipping_date: Date

    @IsDefined()
    shipping_code: string
    
    @IsNumber()
    @IsDefined()
    shipping_cost: number
    
    @IsISO8601()
    @IsDefined()
    shipping_estimated_delivery: Date
    
    @IsISO8601()
    @IsDefined()
    delivery_date: Date

    @IsDefined()
    shipping_address: AddressInputPost

    @IsDefined()
    items: ItemsInputPost[]

    @IsDefined()
    tracks: TracksInputPost[]

    @IsDefined()
    invoices: InvoicesInputPost[]

}

export class ShippingsInputPut {
    @IsDefined()
    _id : String;

    carrier: string

    shipping_method: string
    
    @IsOptional()
    @IsISO8601()
    shipping_date: Date

    shipping_code: string
    
    @IsOptional()
    @IsNumber()
    shipping_cost: number
    
    @IsOptional()
    @IsISO8601()
    shipping_estimated_delivery: Date
    
    @IsOptional()
    @IsISO8601()
    delivery_date: Date

    shipping_address: AddressInputPut

    items: ItemsInputPut[]

    tracks: TracksInputPut[]

    invoices: InvoicesInputPut[]

}
