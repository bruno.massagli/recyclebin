import {ItemsInputPost, ItemsInputPut} from './item_inputs';
import { IsDefined } from 'class-validator';

export class TracksInputPost {

    @IsDefined()
    tracking_link: string

    @IsDefined()
    tracking_number: string

    @IsDefined()
    items: ItemsInputPost[]

}

export class TracksInputPut {
    @IsDefined()
    _id : String;

    tracking_link: string

    tracking_number: string

    items: ItemsInputPut[]

}
