import { MaxLength, IsNumber, IsDefined, IsOptional } from 'class-validator';

export class ItemsInputPost {

    @IsDefined()
    sku: string

    @MaxLength(60)
    @IsDefined()
    name: string

    @IsDefined()
    quantity: number

    @IsNumber()
    @IsDefined()
    original_price: number

    @IsNumber()
    @IsDefined()
    special_price: number

}

export class ItemsInputPut {
    @IsDefined()
    _id: string;

    sku: string

    @IsOptional()
    @MaxLength(60)
    name: string

    quantity: number

    @IsOptional()
    @IsNumber()
    original_price: number

    @IsOptional()
    @IsNumber()
    special_price: number

}
