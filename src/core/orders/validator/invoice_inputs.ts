import {ItemsInputPost, ItemsInputPut} from './item_inputs';
import { IsISO8601, IsDefined, IsOptional } from 'class-validator';

export class InvoicesInputPost {

    @IsDefined()
    key: string
    
    @IsISO8601()
    @IsDefined()
    issue_date: Date

    @IsDefined()
    items: ItemsInputPost[]

}

export class InvoicesInputPut {
    @IsDefined()
    _id: string;

    key: string
    
    @IsOptional()
    @IsISO8601()
    issue_date: Date

    items: ItemsInputPut[]

}
