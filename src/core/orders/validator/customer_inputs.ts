import {DocumentsInputPost, DocumentsInputPut} from './documents_inputs';
import {AddressInputPost, AddressInputPut} from './address_inputs';
import { IsEmail, MaxLength, IsPhoneNumber, IsDefined, IsOptional } from 'class-validator';

export class CustomerInputPost {
    
    @MaxLength(60)
    @IsDefined()
    name: string

    @IsEmail()
    @IsDefined()
    email: string

    @IsDefined()
    document: DocumentsInputPost

    @IsPhoneNumber('BR')
    @IsDefined()
    phones: string[]

    @IsDefined()
    address: AddressInputPost

}

export class CustomerInputPut {
    
    @IsDefined()
    _id: string;

    @IsOptional()
    @MaxLength(60)
    name: string

    @IsOptional()
    @IsEmail()
    email: string

    document: DocumentsInputPut

    @IsOptional()
    @IsPhoneNumber('BR')
    phones: string[]

    address: AddressInputPut

}
