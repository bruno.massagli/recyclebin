import { MaxLength, IsDefined, IsOptional } from 'class-validator';

export class AddressInputPost {

    @MaxLength(100)
    @IsDefined()
    street: string

    @IsDefined()
    number: number

    @MaxLength(30)
    @IsDefined()
    detail: string

    @MaxLength(30)
    @IsDefined()
    neighborhood: string

    @MaxLength(30)
    @IsDefined()
    city: string

    @MaxLength(20)
    @IsDefined()
    region: string

    @MaxLength(30)
    @IsDefined()
    country: string

    @MaxLength(10)
    @IsDefined()
    postcode: string

}

export class AddressInputPut {

    @IsOptional()
    @MaxLength(100)
    street: string

    number: number

    @IsOptional()
    @MaxLength(30)
    detail: string

    @IsOptional()
    @MaxLength(30)
    neighborhood: string

    @IsOptional()
    @MaxLength(30)
    city: string

    @IsOptional()
    @MaxLength(20)
    region: string

    @IsOptional()
    @MaxLength(30)
    country: string

    @IsOptional()
    @MaxLength(10)
    postcode: string

}
