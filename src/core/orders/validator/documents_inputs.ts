import { MaxLength, IsEnum, IsDefined, IsOptional } from 'class-validator';

export class DocumentsInputPost {
    
    @MaxLength(20)
    @IsDefined()
    number: string
    
    @IsEnum(['RG', 'CPF', 'CNPJ'])
    @IsDefined()
    type: string

}

export class DocumentsInputPut {
    
    @IsOptional()
    @MaxLength(20)
    number: string
    
    @IsOptional()
    @IsEnum(['RG', 'CPF', 'CNPJ'])
    type: string

}
