import {ShippingsInputPost, ShippingsInputPut} from './shipping_inputs';
import {CustomerInputPost, CustomerInputPut} from './customer_inputs';
import {ItemsInputPut} from './item_inputs';
import { IsISO8601, IsNumber, IsEnum, IsDefined, IsOptional } from 'class-validator';

export class OrdersInputPost {
    @IsDefined()
    company: String

    @IsDefined()
    order_number: String
    
    @IsISO8601()
    @IsDefined()
    order_date: Date
    
    @IsNumber()
    @IsDefined()
    total_gross: Number

    @IsEnum(['processed', 'invoiced', 'sent', 'delivered', 'cancelled'])
    @IsDefined()
    status: String

    @IsDefined()
    shipping: ShippingsInputPost

    @IsDefined()
    customer: CustomerInputPost

}

export class OrdersInputPut {
    @IsDefined()
    _id : String;

    company: String

    order_number: String
    
    @IsOptional()
    @IsISO8601()
    order_date: Date
    
    @IsOptional()
    @IsNumber()
    total_gross: Number

    @IsOptional()
    @IsEnum(['processed', 'invoiced', 'sent', 'delivered', 'cancelled'])
    status: String

    shipping: ShippingsInputPut

    customer: CustomerInputPut

}

export class OrdersStatusInputPut {
    @IsDefined()
    order_number: String
    
    reason_cancelled: String
    
    key: String
    
    number: String
    
    @IsOptional()
    @IsISO8601()
    invoiced_date: Date
    
    carrier: String
    
    method: String
    
    tracking_number: String
    
    tracking_link: String
    
    @IsOptional()
    @IsISO8601()
    date_delivery: Date
    
    @IsOptional()
    @IsISO8601()
    estimated_delivery: Date
    
    @IsOptional()
    @IsISO8601()
    delivered_date: Date

    items: ItemsInputPut[]


}


