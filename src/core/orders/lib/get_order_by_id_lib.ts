import { Orders } from '../../../db/Mongo/Models/orders'
// import * as http_response_200 from '../../http_responses/response_status_200'
import mongoose from 'mongoose'
import { ResponseStatus200 } from '../../http_responses/response_status_200'

export class GetOrderByIdLib {
    public async getOrdersById(company, id, rest?): Promise<object> {
        try {
            let orders_model = new Orders()
            var Order = orders_model.ordersModel()
            let order_query = await Order.findOne({ company: company, order_number: id }).exec()
            if(!order_query) {
                 if(rest) return {status: 404, message: 'Resource Not Located'}

                throw new Error('Resource Not Located')
            }
            return order_query
        } catch (err) {
            console.log(err)
            return err
        }
    }
}
