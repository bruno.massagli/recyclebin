import { Orders } from '../../../db/Mongo/Models/orders'
import { Shippings } from '../../../db/Mongo/Models//shippings'
import { Items } from '../../../db/Mongo/Models//items'
// import * as http_response_200 from '../../http_responses/response_status_200'
import mongoose from 'mongoose'
import { ResponseStatus200 } from '../../http_responses/response_status_200'
import { ConsolidateObject } from '../../../helpers/consolidate_object';
import { OrdersInputPost } from '../validator/order_inputs';
import { validate } from 'class-validator';

export class PostOrderLib {
    public async PostOrder(company, body, rest?): Promise<object> {
        try {
            let Props = new ConsolidateObject();
            let order_class = new OrdersInputPost();
            order_class = Props.setClassProps(order_class, body)
            let order_validate = await validate(order_class)
            if(order_validate.length) {
                if(rest) return order_validate
                throw new Error(JSON.stringify(order_validate))
            }   
            let orders_model = new Orders()
            let shipping_model = new Shippings()
            let items_model = new Items()
            var new_order = orders_model.ordersModel()
            var new_shipping = shipping_model.shippingsModel()
            var new_item = items_model.itemsModel()
            var verify_order_exists = await new_order.findOne({company: company, order_number: body.order_number}).exec() 
            if(verify_order_exists) {
                if(rest) return {status: 409, message: 'Order Already Exists'}

                throw new Error('Order Already Exists')
            }
            if(company !== body.company) {
                if(rest) return {status: 400, message: 'Company Doesnt Match'}

                throw new Error('Company Doesnt Match')
            }
            console.log(body.shipping)
            var final_items = body.shipping.items.map(async (elem, idx) => {
                var save_items = await new_item.findOne({ _id: elem._id }).exec()
                if (!save_items) {
                    save_items = new new_item({ _id: new mongoose.Types.ObjectId(), ...elem });
                    await save_items.save()
                }

                return save_items
            })
            body.shipping.items = await Promise.all(final_items)
            var save_shipping = new new_shipping({ _id: new mongoose.Types.ObjectId(), ...body.shipping });

            await save_shipping.save()
            body.shipping = save_shipping

            var save_orders = new new_order({ _id: new mongoose.Types.ObjectId(), ...body });

            await save_orders.save();
            return save_orders
        } catch (err) {
            console.log(err)
            return err
        }
    }
}
