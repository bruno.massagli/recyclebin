import { Customers } from '../../../db/Mongo/Models/customers'
// import * as http_response_200 from '../../http_responses/response_status_200'
import mongoose from 'mongoose'
import { ResponseStatus200 } from '../../http_responses/response_status_200'

export class GetCustomerLib {
    public async getCustomerByEmail(email, rest?): Promise<object> {
        try {
            let customers_model = new Customers()
            var Customer = customers_model.customersModel()
            let customer_query = await Customer.findOne({ email: email }).exec()
            if(!customer_query) {
                 if(rest) return {status: 404, message: 'Resource Not Located'}

                throw new Error('Resource Not Located')
            }
            return customer_query
        } catch (err) {
            console.log(err)
            return err
        }
    }
}
