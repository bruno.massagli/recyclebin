const orders_model = require('../../../db/Mongo/Models/orders');


module.exports = {
    getOrdersById: require('./get_order_by_id_lib'),
    getOrders: require('./get_orders_lib'),
    putOrdersStatus: require('./put_order_status_lib')
}