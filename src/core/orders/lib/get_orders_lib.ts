import { Orders } from '../../../db/Mongo/Models/orders'
// import * as http_response_200 from '../../http_responses/response_status_200'
import mongoose from 'mongoose'
import { ResponseStatus200 } from '../../http_responses/response_status_200'

export class GetOrdersLib {
    public async getOrders(company, status, per_page, page, date_start, date_end, rest?): Promise<object> {
        try {
            let orders_model = new Orders()
            var Order = orders_model.ordersModel()
            console.log(date_start, date_end)
            let orders_query = await Order.find({ company: company, createdAt: { "$gte": new Date(date_start).toISOString(), "$lt": new Date(date_end).toISOString() }, "status": status })
                .skip(per_page * page).limit(per_page).exec()
            return orders_query
        } catch (err) {
            console.log(err)
            return err
        }
    }
}

