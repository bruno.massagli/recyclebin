import { Orders } from '../../../db/Mongo/Models/orders'
import { Invoices } from '../../../db/Mongo/Models/invoices'
import { Tracks } from '../../../db/Mongo/Models/tracks'
// import * as http_response_200 from '../../http_responses/response_status_200'
import mongoose from 'mongoose'
import { ResponseStatus200 } from '../../http_responses/response_status_200'
import { ConsolidateObject } from '../../../helpers/consolidate_object';
import { OrdersStatusInputPut } from '../validator/order_inputs';
import { validate } from 'class-validator';

export class PutOrderStatusLib {
    public async putOrdersStatus(company, status, body, rest?): Promise<object> {
        try {

            let Props = new ConsolidateObject();
            let order_class = new OrdersStatusInputPut();
            order_class = Props.setClassProps(order_class, body)
            let order_validate = await validate(order_class)
            if(order_validate.length) {
                if(rest) return order_validate
                throw new Error(JSON.stringify(order_validate))
            }   
            let orders_model = new Orders()
            let invoices_model = new Invoices()
            let tracks_model = new Tracks()
            var Order = orders_model.ordersModel()
            var Invoice = invoices_model.invoicesModel()
            var Track = tracks_model.tracksModel()
            let save_orders = await Order.findOne({ company: company, order_number: body.order_number }).exec()
            if(!save_orders) {
                 if(rest) return {status: 404, message: 'Resource Not Located'}

                throw new Error('Resource Not Located')
            }
            switch (status) {
                case 'processed':
                    save_orders.status = 'processed'
                    break;
                case 'invoiced':
                    if (save_orders.status !== 'processed') {
                        break;
                    }
                    let inv_obj = body
                    inv_obj.invoiced_date = new Date(inv_obj.invoiced_date).toISOString()
                    save_orders.status = 'invoiced'
                    var save_invoices = await Invoice.findOne({ key: inv_obj.key }).exec()
                    if (!save_invoices) {
                        save_invoices = new Invoice({ _id: new mongoose.Types.ObjectId(), ...inv_obj });
                        await save_invoices.save()
                        save_orders.shipping.invoices.push(save_invoices)
                    }
                    break;
                case 'sent':
                    if (save_orders.status !== 'invoiced') {
                        break;
                    }
                    let sent_obj = body
                    sent_obj.date_delivery = new Date(sent_obj.date_delivery).toISOString()
                    sent_obj.estimated_delivery = new Date(sent_obj.estimated_delivery).toISOString()
                    save_orders.status = 'sent'
                    var save_tracks = await Track.findOne({ tracking_number: sent_obj.tracking_number }).exec()
                    if (!save_tracks) {
                        save_tracks = new Track({ _id: new mongoose.Types.ObjectId(), ...sent_obj });
                        await save_tracks.save()
                        save_orders.shipping.tracks.push(save_tracks)
                    }
                    break;
                case 'delivered':
                    if (save_orders.status !== 'sent') {
                        break;
                    }
                    save_orders.status = 'delivered'
                    save_orders.delivery_date = body.delivery_date
                    break;
                case 'cancelled':
                    if (save_orders.status == 'sent' || save_orders.status == 'delivered') {
                        break;
                    }
                    save_orders.status = 'cancelled'
                    save_orders.reason_cancelled = body.reason_cancelled
                    break;
            }
            await save_orders.save();
            return save_orders
        } catch (err) {
            console.log(err)
            return err
        }
    }
}
