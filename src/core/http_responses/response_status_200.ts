export class ResponseStatus200 {
    constructor(data) {
        return {
            data: data,
            status: 200,
            description: 'requisição realizada com sucesso'
        }
    }
}

module.exports = (data) => {
    return {
        data: data,
        status: 200,
        description: 'requisição realizada com sucesso'
    }
}