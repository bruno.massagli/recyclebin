import { VariationsInputPost, VariationsInputPut } from './variation_inputs';
import { CategoriesInputPost, CategoriesInputPut } from '../../categories/validator/category_inputs';
import { MaxLength, IsDefined, IsOptional } from 'class-validator';

export class ProductsInputPost {
    @MaxLength(100)
    @IsDefined()
    name: String
        
    @IsDefined()
    product_id: String
    
    @IsDefined()
    company: String
    
    @MaxLength(40)
    @IsDefined()
    brand: String
    
    @IsDefined()
    description: String
    
    @IsDefined()
    categories: CategoriesInputPost[]
    
    @IsDefined()
    warranty: String
    
    @IsDefined()
    variations: VariationsInputPost[]

}

export class ProductsInputPut {
    
    @IsDefined()
    _id : String;

    @IsOptional()
    @MaxLength(100)
    name: String
        
    product_id: String
    
    company: String
    
    @IsOptional()
    @MaxLength(40)
    brand: String
    
    description: String
    
    categories: CategoriesInputPut[]
    
    warranty: String
    
    variations: VariationsInputPut[]

}
