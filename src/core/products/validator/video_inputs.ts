import { IsUrl, MaxLength, IsDefined, IsOptional } from 'class-validator';

export class VideosInputPost {

    @MaxLength(50)
    @IsDefined()
    name: String

    @IsUrl()
    @IsDefined()
    url: String

    @IsDefined()
    id: String

}

export class VideosInputPut {

    @IsOptional()
    @MaxLength(50)
    name: String

    @IsOptional()
    @IsUrl()
    url: String

    id: String

}
