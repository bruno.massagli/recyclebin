import {StocksInputPost, StocksInputPut} from './stock_inputs';
import {ImagesInputPost, ImagesInputPut} from './images_inputs';
import {VideosInputPost, VideosInputPut} from './video_inputs';
import {PricesInputPost, PricesInputPut} from './price_inputs';
import {DimensionsInputPost, DimensionsInputPut} from './dimension_inputs';
import {SpecificationsInputPost, SpecificationsInputPut} from './specification_inputs';
import { IsDefined } from 'class-validator';


export class VariationsInputPost {
    _id : String;

    @IsDefined()
    sku: number

    @IsDefined()
    stock: StocksInputPost

    @IsDefined()
    ean: String

    @IsDefined()
    nbm: String

    @IsDefined()
    prices: PricesInputPost

    @IsDefined()
    dimensions: DimensionsInputPost

    @IsDefined()
    images: ImagesInputPost[]

    @IsDefined()
    videos: VideosInputPost[]

    @IsDefined()
    specifications: SpecificationsInputPost[]

}

export class VariationsInputPut {
    _id : String;

    sku: number

    stock: StocksInputPut

    ean: String

    nbm: String

    prices: PricesInputPut

    dimensions: DimensionsInputPut

    images: ImagesInputPut[]

    videos: VideosInputPut[]

    specifications: SpecificationsInputPut[]

}
