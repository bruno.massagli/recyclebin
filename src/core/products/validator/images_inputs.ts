import { IsUrl, IsDefined, IsOptional } from 'class-validator';

export class ImagesInputPost {

    @IsDefined()
    name: String
    
    @IsUrl()
    @IsDefined()
    url: String
    
    @IsDefined()
    id: String

}

export class ImagesInputPut {

    name: String
    
    @IsOptional()
    @IsUrl()
    url: String
    
    id: String

}
