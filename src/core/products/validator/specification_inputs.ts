import { IsDefined } from 'class-validator';

export class SpecificationsInputPost {

    @IsDefined()
    key: String
    
    @IsDefined()
    value: String

}

export class SpecificationsInputPut {

    key: String
    
    value: String

}
