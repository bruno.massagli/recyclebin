import { IsDefined, IsNumber, IsOptional } from 'class-validator';

export class StocksInputPost {
    
    @IsNumber()
    @IsDefined()
    quantity: Number
    
    @IsNumber()
    @IsDefined()
    reserved: Number
    
    @IsDefined()
    unit: String

}

export class StocksInputPut {
    
    @IsOptional()
    @IsNumber()
    quantity: Number
    
    @IsOptional()
    @IsNumber()
    reserved: Number
    
    @IsOptional()
    unit: String

}
