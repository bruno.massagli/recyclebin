import { IsNumber, IsDefined, IsOptional } from 'class-validator';

export class PricesInputPost {
    
    @IsNumber()
    @IsDefined()
    list_price: Number
    
    @IsNumber()
    @IsDefined()
    sale_price: Number

}

export class PricesInputPut {
    
    @IsOptional()
    @IsNumber()
    list_price: Number
    
    @IsOptional()
    @IsNumber()
    sale_price: Number

}
