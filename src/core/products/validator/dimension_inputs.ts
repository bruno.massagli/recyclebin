import { IsDefined, IsOptional, IsNumber } from 'class-validator';

export class DimensionsInputPost {

    @IsDefined()
    @IsNumber()
    weight: Number

    @IsDefined()
    @IsNumber()
    length: Number

    @IsDefined()
    @IsNumber()
    width: Number

    @IsDefined()
    @IsNumber()
    height: Number

    @IsDefined()
    @IsNumber()
    cubic_wheight: Number

}
export class DimensionsInputPut {


    @IsOptional()
    @IsNumber()
    weight: Number

    @IsOptional()
    @IsNumber()
    length: Number

    @IsOptional()
    @IsNumber()
    width: Number

    @IsOptional()
    @IsNumber()
    height: Number

    @IsOptional()
    @IsNumber()
    cubic_wheight: Number

}
