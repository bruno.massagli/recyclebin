import { Products } from '../../../db/Mongo/Models/products'
// import * as http_response_200 from '../../http_responses/response_status_200'
import mongoose from 'mongoose'
import { ResponseStatus200 } from '../../http_responses/response_status_200'

export class GetProductByIdLib {
    public async getProductsById(company, id, rest?): Promise<object> {
        try {
            let products_model = new Products()
            var Product = products_model.productsModel()

            let products_query = await Product.findOne({ company: company, _id: id }).exec()
            if(!products_query) {
                 if(rest) return {status: 404, message: 'Resource Not Located'}

                throw new Error('Resource Not Located')
            }
            return products_query
        } catch (err) {
            console.log(err)
            return err
        }
    }
}
