import { Products } from '../../../db/Mongo/Models/products'
// import * as http_response_200 from '../../http_responses/response_status_200'
import mongoose from 'mongoose'
import { ResponseStatus200 } from '../../http_responses/response_status_200'

export class GetProductLib {
    public async getProducts(company, per_page, page, date_start, date_end, rest?): Promise<object> {
        try {
            let products_model = new Products()
            var Product = products_model.productsModel()

            let products_query = await Product.find({ company: company, createdAt: { "$gte": new Date(date_start).toISOString(), "$lt": new Date(date_end).toISOString() } })
                .skip(per_page * page).limit(per_page).exec()
            return products_query
        } catch (err) {
            console.log(err)
            return err
        }

    }
}
