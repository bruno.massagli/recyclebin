import { ConsolidateObject } from '../../../helpers/consolidate_object'
// import * as http_response_200 from '../../http_responses/response_status_200'
import { Variations } from '../../../db/Mongo/Models/variations'
import mongoose from 'mongoose'
import { Products } from '../../../db/Mongo/Models/products'
import { ResponseStatus200 } from '../../http_responses/response_status_200'
import { validate } from 'class-validator';
import { ProductsInputPut } from '../validator/product_inputs';

export class PutProductlib {
    public async putProducts(company, body, rest?): Promise<object> {
        try {
            let Props = new ConsolidateObject();
            let product_class = new ProductsInputPut();
            product_class = Props.setClassProps(product_class, body)
            let product_validate = await validate(product_class)
            if(product_validate.length) {
                if (rest) return product_validate

                throw new Error(JSON.stringify(product_validate))
            }              
            let products_model = new Products()
            var variation_model = new Variations()

            var Product = products_model.productsModel()

            var new_variation = variation_model.variationsModel()
            let if_exists = await Product.findOne({ company: company, _id: body._id }).exec()
            if(!if_exists) {
                 if(rest) return {status: 404, message: 'Resource Not Located'}

                throw new Error('Resource Not Located')
            }
            var verify_product_exists = await Product.findOne({company: company, id_product: body.id_product}).exec() 
            if(verify_product_exists) {
                if(rest) return {message: 'Product Already Exists'}

                throw new Error('Product Already Exists')
            }
            if(company !== body.company) {
                if(rest) return {status: 400, message: 'Company Doesnt Match'}

                throw new Error('Company Doesnt Match')
            }
            var final_variations = body.variations.map(async (elem, idx) => {
                var save_variations = await new_variation.findOne({ _id: elem._id, sku: elem.sku }).exec()
                console.log(save_variations)
                if (!save_variations) {
                    save_variations = new new_variation({ _id: new mongoose.Types.ObjectId(), ...elem });
                    await save_variations.save()
                } else {
                    save_variations = await new_variation.findOneAndUpdate({ _id: elem._id, sku: elem.sku }, {...elem}, { new: true }).exec()
                }
                
                return save_variations
            })
            body.variations = await Promise.all(final_variations)
            let save_products = await Product.findOneAndUpdate({ company: company, _id: body._id }, {...body}, { new: true }).exec()
            // save_products = Props.setClassProps(save_products, body)
            // await save_products.save();
            return save_products
        } catch (err) {
            console.log(err)
            return err
        }
    }
}
