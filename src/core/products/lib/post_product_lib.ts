import {ConsolidateObject} from '../../../helpers/consolidate_object'
import { Products } from '../../../db/Mongo/Models/products'
import { Variations } from '../../../db/Mongo/Models/variations'
// import * as http_response_200 from '../../http_responses/response_status_200'
import mongoose from 'mongoose'
import { ResponseStatus200 } from '../../http_responses/response_status_200'
import { ProductsInputPost } from '../validator/product_inputs';
import { validate } from 'class-validator';

export class PostProductLib {
    public async postProducts(company, body, rest?): Promise<object> {
        try {

            let Props = new ConsolidateObject();
            let product_class = new ProductsInputPost();
            product_class = Props.setClassProps(product_class, body)
            let product_validate = await validate(product_class)
            if(product_validate.length) {
                if (rest) return product_validate
                
                throw new Error(JSON.stringify(product_validate))
            }   
            let products_model = new Products()
            var variation_model = new Variations()


            var new_product = products_model.productsModel()
            var new_variation = variation_model.variationsModel()
            var verify_product_exists = await new_product.findOne({company: company, id_product: body.id_product}).exec() 
            if(verify_product_exists) {
                if(rest) return {status: 409, message: 'Product Already Exists'}

                throw new Error('Product Already Exists')
            }
            if(company !== body.company) {
                if(rest) return {status: 400, message: 'Company Doesnt Match'}

                throw new Error('Company Doesnt Match')
            }
            var final_variations = body.variations.map(async (elem, idx) => {
                var save_variations = await new_variation.findOne({ _id: elem._id, sku: elem.sku }).exec()
                if (!save_variations) {
                    save_variations = new new_variation({ _id: new mongoose.Types.ObjectId(), ...elem });
                    await save_variations.save()
                }
                return save_variations
            })
            body.variations = await Promise.all(final_variations)
            var save_products = new new_product({ _id: new mongoose.Types.ObjectId(), ...body });

            await save_products.save();
            return save_products
        } catch (err) {
            console.log(err)
            return err
        }
    }
}
