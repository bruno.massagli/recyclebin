import * as products_model from '../../../db/Mongo/Models/products'


module.exports = {
    getProductById: require('./get_product_by_id_lib'),
    getProducts: require('./get_product_lib'),
    postProduct: require('./post_product_lib'),
    putProduct: require('./put_product_lib'),
    putProductStocks: require('./put_product_stocks_lib'),
    putProductPrices: require('./put_product_prices_lib')
}