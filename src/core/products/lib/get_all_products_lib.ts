import { Products } from '../../../db/Mongo/Models/products'
import { Categories } from '../../../db/Mongo/Models/categories'
// import * as http_response_200 from '../../http_responses/response_status_200'
import mongoose from 'mongoose'
import { ResponseStatus200 } from '../../http_responses/response_status_200'

export class GetAllProductsLib {
    public async getAllProducts(search, per_page, page, rest?): Promise<object> {
        try {
            let products_model = new Products()
            let categories_model = new Categories()
            var Product = products_model.productsModel()
            var Category = categories_model.categoriesModel()

            let products_query = await Product.find( { $or: [ { "categories.name": search }, { name: search },{ "categories.name": {$regex: search , $options: 'g'} }, { name: {$regex: search , $options: 'g'} } ] } )
                .skip(per_page * page).limit(per_page).exec()
            return products_query
        } catch (err) {
            console.log(err)
            return err
        }

    }
}
