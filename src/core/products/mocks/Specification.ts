export interface Specification {
    key: String;
    value: String;
}
