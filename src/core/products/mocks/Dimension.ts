export interface Dimension {
    weight: Number;
    length: Number;
    width: Number;
    height: Number;
    cubic_wheight: Number;
}

