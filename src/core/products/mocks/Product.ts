import { Variation } from './Variation';
import { Category } from '../../categories/mocks/Category';
export interface Product {
  _id : String;
  company: string;
  name: string;
  id_product: any;
  brand: string;
  description: string;
  categories: Category[];
  warranty: string;
  variations: Variation[];
}

