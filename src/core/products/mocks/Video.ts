export interface Video {

    name: String;
    url: String;
}
