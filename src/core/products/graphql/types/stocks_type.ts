import { ObjectType, Field } from 'type-graphql'
import { buildSchema } from 'graphql'

@ObjectType()
export class StocksData {

    @Field(type => Number)
    quantity: Number
    @Field(type => Number)
    reserved: Number
    @Field(type => String)
    unit: String

}

export const StocksType = `
type StocksType {
    quantity: Float
    reserved: Float
    unit: String

}
`

export const StocksUpdateInput = `
input StocksUpdateInput {
    quantity: Float
    reserved: Float
    unit: String

}
`

export const StocksInput = `
input StocksInput {
    quantity: Float!
    reserved: Float!
    unit: String!

}
`

// export const StocksSchema = buildSchema(StocksType)
// export const StocksInputSchema = buildSchema(StocksInput)
