import { ObjectType, Field } from 'type-graphql'
import { buildSchema } from 'graphql'

@ObjectType()
export class SpecificationsData {

    @Field(type => String)
    key: String
    @Field(type => String)
    value: String

}

export const SpecificationsType = `
type SpecificationsType {
    key: String
    value: String
}
`

export const SpecificationsUpdateInput = `
input SpecificationsUpdateInput {
    key: String
    value: String
}
`

export const SpecificationsInput = `
input SpecificationsInput {
    key: String!
    value: String!
}
`

// export const SpecificationsSchema = buildSchema(SpecificationsType)
// export const SpecificationsInputSchema = buildSchema(SpecificationsInput)