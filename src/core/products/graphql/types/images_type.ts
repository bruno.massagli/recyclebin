import { ObjectType, Field } from 'type-graphql'
import { buildSchema } from 'graphql'

@ObjectType()
export class ImagesData {

    @Field(type => String)
    name: String
    @Field(type => String)
    url: String
    @Field(type => String)
    id: String

}

export const ImagesType = `
type ImagesType {
    name: String
    url: String
    id: String
}
`

export const ImagesUpdateInput = `
input ImagesUpdateInput {
    name: String
    url: String
    id: String
}
`

export const ImagesInput = `
input ImagesInput {
    name: String!
    url: String!
    id: String!
}
`

// export const ImagesSchema = buildSchema(ImagesType)
// export const ImagesInputSchema = buildSchema(ImagesInput)