import { ObjectType, Field } from 'type-graphql'
import { buildSchema } from 'graphql'

@ObjectType()
export class VideosData {

    @Field(type => String)
    name: String
    @Field(type => String)
    url: String
    @Field(type => String)
    id: String

}

export const VideosType = `
type VideosType {
    name: String
    url: String
    id: String

}
`

export const VideosUpdateInput = `
input VideosUpdateInput {
    name: String
    url: String
    id: String

}
`

export const VideosInput = `
input VideosInput {
    name: String!
    url: String!
    id: String!

}
`

// export const VideosSchema = buildSchema(VideosType)
// export const VideosInputSchema = buildSchema(VideosInput)
