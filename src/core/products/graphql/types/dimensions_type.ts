import { ObjectType, Field } from 'type-graphql'
import { buildSchema } from 'graphql'

@ObjectType()
export class DimensionsData {

    @Field(type => Number)
    weight: Number
    @Field(type => Number)
    length: Number
    @Field(type => Number)
    width: Number
    @Field(type => Number)
    height: Number
    @Field(type => Number)
    cubic_wheight: Number

}

export const DimensionsType = `
type DimensionsType {
    weight: Float
    length: Float
    width: Float
    height: Float
    cubic_wheight: Float
}
`

export const DimensionsUpdateInput = `
input DimensionsUpdateInput {
    weight: Float
    length: Float
    width: Float
    height: Float
    cubic_wheight: Float
}
`

export const DimensionsInput = `
input DimensionsInput {
    weight: Float!
    length: Float!
    width: Float!
    height: Float!
    cubic_wheight: Float!
}
`
// export const DimensionsSchema = buildSchema(DimensionsType)
// export const DimensionsInputSchema = buildSchema(DimensionsInput)