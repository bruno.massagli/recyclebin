import { ObjectType, Field } from 'type-graphql'
import { StocksData, StocksType, StocksInput } from './stocks_type'
import { PricesData, PricesType, PricesInput } from './prices_type'
import { DimensionsData, DimensionsType, DimensionsInput } from './dimensions_type'
import { ImagesData, ImagesType, ImagesInput } from './images_type'
import { VideosData, VideosType, VideosInput } from './videos_type'
import { SpecificationsData, SpecificationsType, SpecificationsInput } from './specifications_type'
import { buildSchema } from 'graphql'

@ObjectType()
export class VariationsData {

    @Field(type => String)
    _id : String

    @Field(type => Number)
    sku: number

    @Field(type => StocksData)
    stock: StocksData

    @Field(type => String)
    ean: String

    @Field(type => String)
    nbm: String

    @Field(type => PricesData)
    prices: PricesData

    @Field(type => DimensionsData)
    dimensions: DimensionsData

    @Field(type => [ImagesData])
    images: ImagesData[]

    @Field(type => [VideosData])
    videos: VideosData[]

    @Field(type => [SpecificationsData])
    specifications: SpecificationsData[]

}

export const VariationsType = `

type VariationsType {
    _id : ID!
    sku: String
    id_product: String
    stock: StocksType
    ean: String
    nbm: String
    prices: PricesType
    dimensions: DimensionsType
    images: [ImagesType]
    videos: [VideosType]
    specifications: [SpecificationsType]
}
`

export const VariationsUpdateInput = `

input VariationsUpdateInput {
    _id : ID
    sku: String
    id_product: String
    stock: StocksUpdateInput
    ean: String
    nbm: String
    prices: PricesUpdateInput
    dimensions: DimensionsUpdateInput
    images: [ImagesUpdateInput]
    videos: [VideosUpdateInput]
    specifications: [SpecificationsUpdateInput]
}
`

export const VariationsInput = `

input VariationsInput {
    _id : ID
    sku: String!
    id_product: String!
    stock: StocksInput!
    ean: String!
    nbm: String!
    prices: PricesInput!
    dimensions: DimensionsInput!
    images: [ImagesInput!]!
    videos: [VideosInput!]!
    specifications: [SpecificationsInput!]!
}
`

// export const VariationsSchema = buildSchema(VariationsType)
// export const VariationsInputSchema = buildSchema(VariationsInput)
