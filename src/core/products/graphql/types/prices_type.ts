import { ObjectType, Field } from 'type-graphql'
import { buildSchema } from 'graphql'

@ObjectType()
export class PricesData {

    @Field(type => Number)
    list_price: Number
    @Field(type => Number)
    sale_price: Number

}

export const PricesType = `
type PricesType {
    list_price: Float
    sale_price: Float
}
`

export const PricesUpdateInput = `
input PricesUpdateInput {
    list_price: Float
    sale_price: Float
}
`

export const PricesInput = `
input PricesInput {
    list_price: Float!
    sale_price: Float!
}
`

// export const PricesSchema = buildSchema(PricesType)
// export const PricesInputSchema = buildSchema(PricesInput)