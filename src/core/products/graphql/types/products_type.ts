import { ObjectType, Field } from 'type-graphql'
import { CategoriesData, CategoriesType, CategoriesInput } from '../../../categories/graphql/types/categories_type'
import { VariationsData, VariationsType, VariationsInput } from './vatriations_type'
import { buildSchema } from 'graphql'

@ObjectType()
export class ProductsData {

    @Field(type => String)
    _id: String
    
    @Field(type => String)
    name: String
    
    @Field(type => String)
    company: String
    
    @Field(type => String)
    id_product: String
    
    @Field(type => String)
    brand: String
    
    @Field(type => String)
    description: String
    
    @Field(type => [CategoriesData])
    categories: CategoriesData[]
    
    @Field(type => String)
    warranty: String
    
    @Field(type => [VariationsData])
    variations: VariationsData[]

}

export const ProductsType = `


type ProductsType {
    _id : ID!
    name: String
    company: String
    id_product: String
    brand: String
    description: String
    categories: [CategoriesType]
    warranty: String
    variations: [VariationsType]
}
`

export const ProductsUpdateInput = `

input ProductsUpdateInput {
    _id : ID!
    name: String
    company: String
    id_product: String
    brand: String
    description: String
    categories: [CategoriesUpdateInput]
    warranty: String
    variations: [VariationsUpdateInput]
}
`

export const ProductsInput = `


input ProductsInput {
    _id : ID
    name: String!
    company: String!
    id_product: String!
    brand: String!
    description: String!
    categories: [CategoriesInput!]!
    warranty: String!
    variations: [VariationsInput!]!
}
`

// export const ProductsSchema = buildSchema(ProductsType)
// export const ProductsInputSchema = buildSchema(ProductsInput)