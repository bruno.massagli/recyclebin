import { GetAllProductsLib } from '../../lib/get_all_products_lib'
import { GetProductByIdLib } from '../../lib/get_product_by_id_lib'
import { GetProductLib } from '../../lib/get_product_lib'
import { PostProductLib } from '../../lib/post_product_lib'
import { PutProductlib } from '../../lib/put_product_lib'
import { PutProductPricesLib } from '../../lib/put_product_prices_lib'
import { PutProductStocksLib } from '../../lib/put_product_stocks_lib'
import { Resolver, Query, Mutation, Arg } from 'type-graphql'
import { ProductsData, ProductsType, ProductsInput } from '../types/products_type'
import { Company } from '../../../auth/mocks/Company'
import { buildSchema } from 'graphql'
import { makeExecutableSchema } from 'graphql-tools'
import { CompanyType } from '../../../auth/graphql/types/auth_types'
import { GraphQLDate, GraphQLDateTime } from 'graphql-iso-date'
import { CategoriesType, CategoriesInput } from '../../../categories/graphql/types/categories_type'
import { StocksType, StocksInput } from '../types/stocks_type'
import { PricesType, PricesInput } from '../types/prices_type'
import { DimensionsType, DimensionsInput } from '../types/dimensions_type'
import { ImagesType, ImagesInput } from '../types/images_type'
import { VideosType, VideosInput } from '../types/videos_type'
import { SpecificationsType, SpecificationsInput } from '../types/specifications_type'
import { VariationsType, VariationsInput } from '../types/vatriations_type'
import { CustomerType } from '../../../orders/graphql/types/customers_type'
import { DocumentsType } from '../../../orders/graphql/types/documents_type'
import { AddressType } from '../../../orders/graphql/types/addresses_type'

export const resolverProducts = {
  Query: {
     get_all_products: async(parent, { search, per_page, page}, context, info) => {
      let prod = new GetAllProductsLib()
      return await prod.getAllProducts(search, per_page, page)
    },
    get_product_by_id: async (parent, { company, id }, context, info) => {
      let prod = new GetProductByIdLib()
      return await prod.getProductsById(company, id)
    },
    get_products: async (parent, { company, per_page, page, date_start, date_end }, context, info) => {
      let prod = new GetProductLib()
      return await prod.getProducts(company, per_page, page, date_start, date_end)
    }
  },
  Mutation: {
    create_products: async (parent, { company, body }, context, info) => {
      let prod = new PostProductLib()

      return await prod.postProducts(company, body)
    },
    update_products: async (parent, { company, body }, context, info) => {
      let prod = new PutProductlib()
      return await prod.putProducts(company, body)
    },
    update_prices: async (parent, { company, body }, context, info) => {
      let prod = new PutProductPricesLib()
      return await prod.putProductsPrices(company, body)
    },
    update_stocks: async (parent, { company, body }, context, info) => {
      let prod = new PutProductStocksLib()
      return await prod.putProductsStocks(company, body)
    }
  }
}

export const products_query = `
  get_all_products(search: String, per_page: Int, page: Int, date_start: Date, date_end: Date ): [ProductsType]
  
  get_product_by_id(company: String, id: String): ProductsType
  
  get_products(company: String, per_page: Int, page: Int, date_start: Date, date_end: Date) : [ProductsType]
`
export const products_mutation = `
  create_products(company: String, body: ProductsInput): ProductsType
  
  update_products(company: String, body: ProductsUpdateInput): ProductsType
  
  update_prices(company: String, body: PricesUpdateInput): ProductsType
  
  update_stocks(company: String, body: StocksUpdateInput): ProductsType
`
// export const graphqlProducts = `
// ${CustomerType}
// ${DocumentsType}
// ${AddressType}
// ${CompanyType}

// ${CategoriesType}
// ${StocksType}
// ${PricesType}
// ${DimensionsType}
// ${ImagesType}
// ${VideosType}
// ${SpecificationsType}

// ${CategoriesInput}
// ${StocksInput}
// ${PricesInput}
// ${DimensionsInput}
// ${ImagesInput}
// ${VideosInput}
// ${SpecificationsInput}

// ${VariationsType}
// ${VariationsInput}
// ${ProductsType}
// ${ProductsInput}

// scalar Date  
// scalar DateTime

// type Query {
//   get_all_products(search: String, per_page: Int, page: Int, date_start: Date, date_end: Date ): [ProductsType]
  
//   get_product_by_id(company: String, id: String): ProductsType
  
//   get_products(company: String, per_page: Int, page: Int, date_start: Date, date_end: Date) : [ProductsType]
// }

// type Mutation {
//   create_products(company: String, body: ProductsInput): String
  
//   update_products(company: String, body: String): String
  
//   update_prices(company: String, body: String): String
  
//   update_stocks(company: String, body: String): String
// }
// `

// export const schemaProducts = buildSchema(graphqlProducts)