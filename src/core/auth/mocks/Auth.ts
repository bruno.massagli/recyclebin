import { Customer } from '../../orders/mocks/Customer';

export interface Auth {
    _id : String;
    email: String;
    password: String;
    access_level: String;
    customer: Customer;
}

