import { Customer } from '../../orders/mocks/Customer';
import { Documents } from '../../orders/mocks/Documents';
import { Address } from '../../orders/mocks/Address';
import { Images } from '../../products/mocks/Images';

export interface Company {
    _id : String;
    name: String;
    email: String;
    api_key: String;
    document: Documents;
    phones: String[];
    address: Address;
    avatar: Images;
    customers: Customer[];
}

