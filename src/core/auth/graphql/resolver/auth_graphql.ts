import { GetAuthLib } from '../../lib/get_auth_lib'
import { PostAuthLib } from '../../lib/post_auth_lib'
import { PutAuthLib } from '../../lib/put_auth_lib'
import { GetCompaniesLib } from '../../lib/get_companies_lib'
import { GetCompanyLib } from '../../lib/get_company_lib'
import { PostCompanyLib } from '../../lib/post_company_lib'
import { PutCompanyLib } from '../../lib/put_company_lib'
import { me } from '../../lib/me'
import { login } from '../../lib/login'
import { Resolver, Query, Arg, Mutation, Ctx } from 'type-graphql'
import { AuthData, CompanyData } from '../types/auth_types'
import { MyContext } from '../../mocks/MyContext'
import { Company } from '../../mocks/Company'
import { Auth } from '../../mocks/Auth'
import { buildSchema } from 'graphql'
import gpl from 'graphql-tag'
import { CompanyType, AuthType, AuthInput, CompanyInput } from '../types/auth_types'
import { CustomerInput, CustomerType } from '../../../orders/graphql/types/customers_type'
import { DocumentsType, DocumentsInput } from '../../../orders/graphql/types/documents_type'
import { AddressType, AddressInput } from '../../../orders/graphql/types/addresses_type'
import { ImagesType, ImagesInput } from '../../../products/graphql/types/images_type'

export const resolverAuth = {
  Query: {
    me: async (parent, { ctx }, context, info) => {
      return await me(ctx)
    },
    get_auth: async (parent, { company, id_customer}, context, info) => {
      let auth = new GetAuthLib()
      return await auth.GetAuthByCustomer(company, id_customer)
    },
    get_companies: async (parent, { company, per_page, page }, context, info) => {
      let companies = new GetCompaniesLib()
      return await companies.getCompanies(company, per_page, page)
    },
    get_company_by_id: async (parent, { company, id }, context, info) => {
      let companies = new GetCompanyLib()
      return await companies.getCompanyById(company, id)
    }
  },
  Mutation: {
    login: async (parent, { company, email, password }, context, info) => {
      return await login(company, email, password)
    },
    create_auth: async (parent, { company, body }, context, info) => {
      let auth = new PostAuthLib()
      return await auth.postAuth(company, body)
    },
    update_auth: async (parent, { company, body }, context, info) => {
      let auth = new PutAuthLib()
      return await auth.putAuth(company, body)
    },
    create_company: async (parent, { company, body }, context, info) => {
      let companies = new PostCompanyLib()
      return await companies.postCompany(company, body)
    },
    update_company: async (parent, { company, body }, context, info) => {
      let companies = new PutCompanyLib()
      return await companies.putCompany(company, body)
    }
  }
}

export const auth_query = `
  me(ctx: MyContext): AuthType

  get_auth(company: String, id_customer: String): AuthType

  get_companies(company: String, per_page: Int, page: Int): [CompanyType]

  get_company_by_id(company: String, id: String): CompanyType
`

export const auth_mutation = `
  login(company: String, email: String, password: String): String

  create_auth(company: String, body: AuthInput): AuthType

  update_auth(company: String, body: AuthUpdateInput): AuthType

  create_company(company: String, body: CompanyInput): CompanyType

  update_company(company: String, body: CompanyUpdateInput): CompanyType
`
// export const graphqlAuth = `

// scalar MyContext
// ${CompanyType}
// ${AuthType}
// ${CustomerType}
// ${DocumentsType}
// ${AddressType}
// ${ImagesType}

// ${AuthInput}
// ${CompanyInput}
// ${DocumentsInput}
// ${AddressInput}
// ${ImagesInput}
// ${CustomerInput}


// type Query {
//   me(ctx: MyContext): [AuthType]

//   get_auth(company: String, id_customer: String): AuthType

//   get_companies(company: String, per_page: Int, page: Int): CompanyType

//   get_company_by_id(company: String, id: String): CompanyType
// }

// type Mutation {
//   login(company: String, email: String, password: String): String

//   create_auth(company: String, body: AuthInput): AuthType

//   update_auth(company: String, body: String): AuthType

//   create_company(company: String, body: CompanyInput): CompanyType

//   update_company(company: String, body: String): CompanyType
// }
// `

// export const schemaAuth = buildSchema(graphqlAuth)