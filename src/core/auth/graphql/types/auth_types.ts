import { ObjectType, Field } from 'type-graphql'
import {DocumentsData, DocumentsType, DocumentsInput} from '../../../orders/graphql/types/documents_type'
import {CustomerData, CustomerType, CustomerInput} from '../../../orders/graphql/types/customers_type'
import {AddressData, AddressType, AddressInput} from '../../../orders/graphql/types/addresses_type'
import {ImagesData, ImagesType, ImagesInput} from '../../../products/graphql/types/images_type'
import { buildSchema } from 'graphql'

@ObjectType()
export class AuthData {

    @Field(type => String)
    _id: String

    @Field(type => String)
    email: String

    @Field(type => String)
    password: String

    @Field(type => CustomerData)
    customer: CustomerData

}

export const AuthType = `

type AuthType {
    _id: ID!
    email: String
    password: String
    access_level: String
    customer: CustomerType
}
`

export const AuthUpdateInput = `

input AuthUpdateInput {
    _id: ID!
    email: String
    password: String
    access_level: String
    customer: CustomerUpdateInput
}
`

export const AuthInput = `

input AuthInput {
    email: String!
    password: String!
    access_level: String!
    customer: CustomerInput!
}
`

// export const AuthSchema = buildSchema(AuthType)
// export const AuthInputSchema = buildSchema(AuthInput)



@ObjectType()
export class CompanyData {

    @Field(type => String)
    _id : String

    @Field(type => String)
    name: String
    
    @Field(type => String)
    email: String
    
    @Field(type => String)
    api_key: String
    
    @Field(type => DocumentsData)
    document: DocumentsData
    
    @Field(type => [String])
    phones: String[]
    
    @Field(type => AddressData)
    address: AddressData
    
    @Field(type => ImagesData)
    avatar: ImagesData
    
    @Field(type => [CustomerData])
    customers: CustomerData[]

}

export const CompanyType = `

type CompanyType {
    _id : ID!
    name: String
    email: String
    api_key: String
    document: DocumentsType
    phones: [String]
    address: AddressType
    avatar: ImagesType
    customers: [CustomerType]
}
`

export const CompanyUpdateInput = `

input CompanyUpdateInput {
    _id : ID!
    name: String
    email: String
    api_key: String
    document: DocumentsUpdateInput
    phones: [String]
    address: AddressUpdateInput
    avatar: ImagesUpdateInput
    customers: [CustomerUpdateInput]
}
`

export const CompanyInput = `

input CompanyInput {
    _id : ID
    name: String!
    email: String!
    api_key: String!
    document: DocumentsInput!
    phones: [String!]!
    address: AddressInput!
    avatar: ImagesInput!
    customers: [CustomerInput!]!
}
`

// export const CompanySchema = buildSchema(CompanyType)

// export const CompanyInputSchema = buildSchema(CompanyInput)
