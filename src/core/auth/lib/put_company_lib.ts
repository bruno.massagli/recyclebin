import { Companies } from '../../../db/Mongo/Models/companies'
import { ConsolidateObject } from '../../../helpers/consolidate_object'
import mongoose from 'mongoose'
import { ResponseStatus200 } from '../../http_responses/response_status_200'
import { validate } from 'class-validator';
import { CompanyInputPut } from '../validator/company_inputs';

export class PutCompanyLib {
    public async putCompany(company, body, rest?): Promise<object> {
        try {
            
            let Props = new ConsolidateObject();
            let company_class = new CompanyInputPut();
            company_class =Props.setClassProps(company_class, body)
            let company_validate = await validate(company_class)
            if(company_validate.length) {
                if(rest) return company_validate
                throw new Error(JSON.stringify(company_validate))
            }
            let companies_model = new Companies()
            var Company = companies_model.companiesModel()
            var verify_company_exists = await Company.findOne({email: body.email}).exec() 
            if(verify_company_exists) {
                if(rest) return {status: 409, message: 'Company Already Exists'}

                throw new Error('Company Already Exists')
            }
            let if_exists = await Company.findOne({ _id: body._id }).exec()
            if(!if_exists) {
                 if(rest) return {status: 404, message: 'Resource Not Located'}

                throw new Error('Resource Not Located')
            }
            let save_company = await Company.findOneAndUpdate({ _id: body._id }, {...body}, { new: true }).exec()
            // save_company = Props.setClassProps(save_company, JSON.parse(body))
            
            // await save_company.save();
            return save_company
        } catch (err) {
            console.log(err)
            return err
        }
    }
}
