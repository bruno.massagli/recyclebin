import { Auth } from '../../../db/Mongo/Models/auth'
import { ConsolidateObject } from '../../../helpers/consolidate_object'
import bcrypt from 'bcryptjs'
import mongoose from 'mongoose'
import { ResponseStatus200 } from '../../http_responses/response_status_200'
import { Customers } from '../../../db/Mongo/Models/customers'
import { validate } from 'class-validator';
import { AuthInputPut } from '../validator/auth_inputs';

export class PutAuthLib {
    public async putAuth(company, body, rest?): Promise<object> {
        try {
          
            let Props = new ConsolidateObject();
            let auth_class = new AuthInputPut();
            auth_class = Props.setClassProps(auth_class, body)
            let auth_validate = await validate(auth_class)
            if(auth_validate.length) {
                if(rest) return auth_validate

                throw new Error(JSON.stringify(auth_validate))
            }

            var customer_model = new Customers()
            let auth_model = new Auth()
            var Authentication = auth_model.authModel()
            var new_customer = customer_model.customersModel()

            var verify_auth_exists = await Authentication.findOne({email: body.email}).exec() 
            if(verify_auth_exists) {
                if(rest) return {status: 400, message: 'email already taken'}

                throw new Error('email already taken')
            }

            var save_customer = await new_customer.findOne({ _id: body.customer._id }).exec()
            if(!save_customer) {
                 if(rest) return {status: 404, message: 'Resource Not Located'}

                throw new Error('Resource Not Located')
            }
            body.customer = save_customer
            save_customer.save()

            // save_auth = Props.setClassProps(save_auth, body)

            body.password = await bcrypt.hash(body.password, 12)
            let if_exists = await Authentication.findOne({ _id: body._id }).exec()
            if(!if_exists) {
                 if(rest) return {status: 404, message: 'Resource Not Located'}

                throw new Error('Resource Not Located')
            }
            let save_auth = await Authentication.findOneAndUpdate({ _id: body._id }, {...body}, { new: true }).exec()

            // await save_auth.save()
            return save_auth
        } catch (err) {
            console.log(err)
            return err
        }
    }
}
