import { Companies } from '../../../db/Mongo/Models/companies'
// import * as http_response_200 from '../../http_responses/response_status_200'
import mongoose from 'mongoose'
import { ResponseStatus200 } from '../../http_responses/response_status_200'

export class GetCompanyLib {
    public async getCompanyById(company, id, rest?): Promise<object> {
        try {

            let companies_model = new Companies()
            var Company = companies_model.companiesModel()

            let companies_query = await Company.findOne({ _id: id }).exec()
            if(!companies_query) {
                 if(rest) return {status: 404, message: 'Resource Not Located'}

                throw new Error('Resource Not Located')
            }
            return companies_query
        } catch (err) {
            console.log(err)
            return err
        }
    }
}
