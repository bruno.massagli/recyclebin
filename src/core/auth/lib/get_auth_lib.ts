import { Auth } from '../../../db/Mongo/Models/auth'
import { Customers } from '../../../db/Mongo/Models/customers'
import { ConsolidateObject } from '../../../helpers/consolidate_object'

// import * as http_response_200 from '../../http_responses/response_status_200'
import mongoose from 'mongoose'
import { ResponseStatus200 } from '../../http_responses/response_status_200'

export class GetAuthLib {
    public async GetAuthByCustomer(company, id_customer, rest?): Promise<object> {
        try {

            let auth_model = new Auth()
            let customers_model = new Customers()
            var Authentication = auth_model.authModel();
            var customer = customers_model.customersModel()
            let auth_query = await Authentication.findOne({ "customer._id": id_customer }).exec()
            if(!auth_query) {
                 if(rest) return {status: 404, message: 'Resource Not Located'}

                throw new Error('Resource Not Located')
            }
            return auth_query
        } catch (err) {
            console.log(err)
            return err
        }
    }
}
