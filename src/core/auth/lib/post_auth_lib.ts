import {Auth} from '../../../db/Mongo/Models/auth'
import {Customers} from '../../../db/Mongo/Models/customers'
import bcrypt from 'bcryptjs'
import mongoose from 'mongoose'
import { validate } from 'class-validator';
import {ResponseStatus200} from '../../http_responses/response_status_200'
import {AuthInputPost} from '../validator/auth_inputs'
import { ConsolidateObject } from '../../../helpers/consolidate_object';
export class PostAuthLib {
    public async postAuth (company, body, rest?): Promise<object> {
        try {
           
            let Props = new ConsolidateObject();
            let auth_class = new AuthInputPost();
            auth_class = Props.setClassProps(auth_class, body)
            let auth_validate = await validate(auth_class)
            if(auth_validate.length) {
                if(rest) return auth_validate

                throw new Error(JSON.stringify(auth_validate))
            }
            let auth_model = new Auth()
            var customer_model = new Customers()
            
            var new_auth = auth_model.authModel();
            var new_customer = customer_model.customersModel();
            var verify_auth_exists = await new_auth.findOne({email: body.email}).exec() 
            if(verify_auth_exists) {
                if(rest) return {status: 400, message: 'email already taken'}

                throw new Error('email already taken')
            }

            var save_customer = await new_customer.findOne({email: body.email}).exec()
            console.log(save_customer)
            if(!save_customer || !save_customer.length) {
                save_customer = new new_customer({_id: new mongoose.Types.ObjectId(), ...body.customer});
                save_customer.save()
            }
            body.customer = save_customer
            body.password = await bcrypt.hash(body.password, 12)
            var save_auth = new new_auth({_id: new mongoose.Types.ObjectId(), ...body})
            console.log(save_auth)
            await save_auth.save();
            return save_auth
        } catch (err) {
            console.log(err)
            return err
        }
    }
}
