import { Auth } from '../../../db/Mongo/Models/auth'
import mongoose from 'mongoose'
export async function me (ctx): Promise<any> {
    const auth = new Auth()
    var Authentication = auth.authModel()

    const id_user = ctx.req.session!.id_user
    if (!id_user) return null
    return await Authentication.findOne({ _id: id_user }).exec()
}