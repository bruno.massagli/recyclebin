import { compare } from 'bcryptjs'
import { sign } from 'jsonwebtoken'
import { Auth as AuthModel} from '../../../db/Mongo/Models/auth'
import { Companies as CompanyModel} from '../../../db/Mongo/Models/companies'
import { Company } from '../mocks/Company'
import { Auth } from '../mocks/Auth'

function generateToken(company: Company, auth: Auth): string {
  return sign(
    {
      companies: company.name,
      user_id: auth._id,
      username: auth.customer.name,
      email: auth.email
    }, 'secret', { expiresIn: '7 days' })
}

export async function login (company: String, email: string, password: string, rest?: boolean): Promise<String> {
    const auth = new AuthModel()
    const company_model = new CompanyModel()
    const companyData = await company_model.companiesModel().findOne({ _id: company }).exec()
    const user = await auth.authModel().findOne({ email: email }).exec()
    if (!user) {
      if(rest) return 'Resource Not Located'

      throw Error(`User with email ${email} does not exist!`)
    }
    const valid: boolean = await compare(password, user.password)
    if (!valid) {
      if(rest) return 'incorrect password!'

      throw Error('incorrect password!')
    }
    return generateToken(companyData, user)
}
