import { Companies } from '../../../db/Mongo/Models/companies'
// import * as http_response_200 from '../../http_responses/response_status_200'
import mongoose from 'mongoose'
import { ResponseStatus200 } from '../../http_responses/response_status_200'
import { validate } from 'class-validator';
import { ConsolidateObject } from '../../../helpers/consolidate_object';
import { CompanyInputPost } from '../validator/company_inputs';

export class PostCompanyLib {
    public async postCompany(company, body, rest?): Promise<object> {
        try {
         
            let Props = new ConsolidateObject();
            let company_class = new CompanyInputPost();
            company_class = Props.setClassProps(company_class, body)
            let company_validate = await validate(company_class)
            if(company_validate.length) {
                if(rest) return company_validate

                throw new Error(JSON.stringify(company_validate))
            }
            let companies_model = new Companies()
            var new_company = companies_model.companiesModel()
            var verify_company_exists = await new_company.findOne({email: body.email}).exec() 
            if(verify_company_exists) {
                if(rest) return {status: 409, message: 'Company Already Exists'}

                throw new Error('Company Already Exists')
            }
            var save_company = new new_company({ _id: new mongoose.Types.ObjectId(), ...body });

            await save_company.save();
            return save_company
        } catch (err) {
            console.log(err)
            return err
        }
    }
}
