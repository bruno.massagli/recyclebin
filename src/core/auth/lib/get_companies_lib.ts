import { Companies } from '../../../db/Mongo/Models/companies'
// import * as http_response_200 from '../../http_responses/response_status_200'
import mongoose from 'mongoose'
import { ResponseStatus200 } from '../../http_responses/response_status_200'

export class GetCompaniesLib {
    public async getCompanies(company, per_page, page, rest?): Promise<object> {
        try {

            let companies_model = new Companies()
            var Company = companies_model.companiesModel()

            let companies_query = await Company.find({})
                .skip(per_page * page).limit(per_page).exec()
            return companies_query
        } catch (err) {
            console.log(err)
            return err
        }

    }
}
