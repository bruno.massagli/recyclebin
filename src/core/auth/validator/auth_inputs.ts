import { CustomerInputPost, CustomerInputPut } from '../../orders/validator/customer_inputs';
import { IsEmail, MaxLength, MinLength, IsEnum, IsDefined, IsOptional } from 'class-validator';

export class AuthInputPost {

    @IsEmail()
    @IsDefined()
    email: String;

    @MaxLength(20)
    @MinLength(6)
    @IsDefined()
    password: String;

    @IsEnum(['master', 'member'])
    @IsDefined()
    access_level: String;

    @IsDefined()
    customer: CustomerInputPost;

}
export class AuthInputPut {
    @IsDefined()
    _id : String;

    @IsOptional()
    @IsEmail()
    email: String;

    @IsOptional()
    @MaxLength(20)
    @MinLength(6)
    password: String;

    @IsOptional()
    @IsEnum(['master', 'member'])
    access_level: String;

    customer: CustomerInputPut;

}


