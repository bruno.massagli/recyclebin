import { CustomerInputPost, CustomerInputPut } from '../../orders/validator/customer_inputs';
import { DocumentsInputPost, DocumentsInputPut } from '../../orders/validator/documents_inputs';
import { AddressInputPost, AddressInputPut } from '../../orders/validator/address_inputs';
import { ImagesInputPost, ImagesInputPut } from '../../products/validator/images_inputs';
import { MaxLength, IsEmail, IsPhoneNumber, IsUrl, IsDefined, IsOptional } from 'class-validator';

export class CompanyInputPost {

    @MaxLength(60)
    @IsDefined()
    name: String;
    
    @IsEmail()
    @IsDefined()
    email: String;
    
    @IsDefined()
    api_key: String;
    
    @IsDefined()
    document: DocumentsInputPost;
    
    @IsPhoneNumber('BR')
    @IsDefined()
    phones: String[];
    
    @IsDefined()
    address: AddressInputPost;
    
    @IsDefined()
    avatar: ImagesInputPost;
    
    @IsDefined()
    customers: CustomerInputPost[];

}
export class CompanyInputPut {

    @IsDefined()
    _id : String;

    @IsOptional()
    @MaxLength(60)
    name: String;
    
    @IsOptional()
    @IsEmail()
    email: String;
    
    api_key: String;
    
    document: DocumentsInputPut;
    
    @IsOptional()
    @IsPhoneNumber('BR')
    phones: String[];
    
    address: AddressInputPut;
    
    avatar: ImagesInputPut;
    
    customers: CustomerInputPut[];

}
