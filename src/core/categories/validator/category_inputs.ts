import { MaxLength, IsDefined, IsOptional } from 'class-validator';

export class CategoriesInputPost {
    @MaxLength(30)
    @IsDefined()
    name: String
    
    @IsDefined()
    company: String

    parent_id : String
}

export class CategoriesInputPut {
    @IsDefined()
    _id : String;

    @IsOptional()
    @MaxLength(30)
    name: String
    
    company: String

    parent_id : String
}
