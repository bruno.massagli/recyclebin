import { Categories } from '../../../db/Mongo/Models/categories'
// import * as http_response_200 from '../../http_responses/response_status_200'
import mongoose from 'mongoose'
import { ResponseStatus200 } from '../../http_responses/response_status_200'

export class GetCategoriesLib {
    public async getCategories(company, per_page, page, rest?): Promise<object> {
        try {
            let categories_model = new Categories()
            var Category = categories_model.categoriesModel()

            let categories_query = await Category.find({ company: company })
                .skip(per_page * page).limit(per_page).exec()
            return categories_query
        } catch (err) {
            console.log(err)
            return err
        }
    }
}