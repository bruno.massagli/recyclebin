import { ConsolidateObject } from '../../../helpers/consolidate_object'
import { Categories } from '../../../db/Mongo/Models/categories'
// import * as http_response_200 from '../../http_responses/response_status_200'
import mongoose from 'mongoose'
import { ResponseStatus200 } from '../../http_responses/response_status_200'
import { CategoriesInputPut } from '../validator/category_inputs';
import { validate } from 'class-validator';

export class PutCategoryLib {
    public async putCategory(company, body, rest?): Promise<object> {
        try {
            let Props = new ConsolidateObject();
            let category_class = new CategoriesInputPut();
            category_class = Props.setClassProps(category_class, body)
            console.log(category_class)
            let category_validate = await validate(category_class)
            if(category_validate.length) {
                if(rest) return category_validate
                throw new Error(JSON.stringify(category_validate))
            }            
            let categories_model = new Categories()
            var Category = categories_model.categoriesModel()
            let if_exists = await Category.findOne({ company: company, _id: body._id }).exec()
            if(!if_exists) {
                 if(rest) return {status: 404, message: 'Resource Not Located'}

                throw new Error('Resource Not Located')
            }
            if(company !== body.company) {
                if(rest) return {status: 400, message: 'Company Doesnt Match'}

                throw new Error('Company Doesnt Match')
            }
            let save_categories = await Category.findOneAndUpdate({ company: company, _id: body._id }, {...body}, { new: true }).exec()
            // save_categories = Props.setClassProps(save_categories, JSON.parse(body))
            // await save_categories.save();
            return save_categories
        } catch (err) {
            console.log(err)
            return err
        }
    }
}