import {ConsolidateObject} from '../../../helpers/consolidate_object'
import { Categories } from '../../../db/Mongo/Models/categories'
// import * as http_response_200 from '../../http_responses/response_status_200'
import mongoose from 'mongoose'
import { ResponseStatus200 } from '../../http_responses/response_status_200'
import { CategoriesInputPost } from '../validator/category_inputs';
import { validate } from 'class-validator';

export class PostCategoryLib {
    public async postCategory(company, body, rest?): Promise<object> {
        try {

            let Props = new ConsolidateObject();
            let category_class = new CategoriesInputPost();
            category_class = Props.setClassProps(category_class, body)
            console.log(category_class)

            let category_validate = await validate(category_class)
            if(category_validate.length) {
                if(rest) return category_validate

                throw new Error(JSON.stringify(category_validate))
            }
            if(company !== body.company) {
                if(rest) return {status: 400, message: 'Company Doesnt Match'}

                throw new Error('Company Doesnt Match')
            }
            let categories_model = new Categories()

            var new_category = categories_model.categoriesModel()

            var save_categories = new new_category({ company: company, _id: new mongoose.Types.ObjectId(), ...body });
            await save_categories.save();
            return save_categories
        } catch (err) {
            console.log(err)
            return err
        }
    }
}
