import { Categories } from '../../../db/Mongo/Models/categories'
// import * as http_response_200 from '../../http_responses/response_status_200'
import mongoose from 'mongoose'
import { ResponseStatus200 } from '../../http_responses/response_status_200'

export class GetCategoryByIdLib {
    public async getCategoryById(company, id, rest?): Promise<object> {
        try {
            let categories_model = new Categories()
            var Category = categories_model.categoriesModel()

            let category_query = await Category.findOne({_id: id, company: company}).exec()
            if(!category_query) {
                 if(rest) return {status: 404, message: 'Resource Not Located'}

                throw new Error('Resource Not Located')
            }
            return category_query
        } catch (err) {
            console.log(err)
            return err
        }
    }
}
