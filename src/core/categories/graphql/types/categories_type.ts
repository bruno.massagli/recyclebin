import { ObjectType, Field } from 'type-graphql'
import { buildSchema } from 'graphql'

@ObjectType()
export class CategoriesData {

    @Field(type => String)
    _id: String

    @Field(type => String)
    name: String

    @Field(type => String)
    company: String

    @Field(type => String, { nullable: true })
    parent_id: String
}

export const CategoriesType = `
type CategoriesType {
    _id : ID!
    name: String
    company: String
    parent_id: String
}
`

export const CategoriesUpdateInput = `
input CategoriesUpdateInput {
    _id : ID!
    name: String
    company: String
    parent_id: String
}
`

export const CategoriesInput = `
input CategoriesInput {
    _id : ID
    name: String!
    company: String!
    parent_id: String
}
`

// export const CategoriesSchema = buildSchema(CategoriesType)
// export const CategoriesInputSchema = buildSchema(CategoriesInput)
