import { GetCategoryByIdLib } from '../../lib/get_category_by_id_lib'
import { GetCategoriesLib } from '../../lib/get_categories_lib'
import { PostCategoryLib } from '../../lib/post_category_lib'
import { PutCategoryLib } from '../../lib/put_category_lib'
import { Resolver, Query, Mutation, Arg } from 'type-graphql';
import { CategoriesData, CategoriesInput, CategoriesType } from '../types/categories_type'
import { Company } from '../../../auth/mocks/Company'
import { CompanyType } from '../../../auth/graphql/types/auth_types'
import { buildSchema } from 'graphql'
import { CustomerType } from '../../../orders/graphql/types/customers_type';
import { DocumentsType } from '../../../orders/graphql/types/documents_type';
import { AddressType } from '../../../orders/graphql/types/addresses_type';
import { ImagesType } from '../../../products/graphql/types/images_type';

export const resolverCategories = {
  Query: {
    get_category_by_id: async (parent, { company, id }, context, info) => {
      let cat = new GetCategoryByIdLib()
      return await cat.getCategoryById( company, id)
    },
    get_categories: async (parent, { company, per_page, page}, context, info) => {
      let cat = new GetCategoriesLib()

      return await cat.getCategories( company, per_page, page)
    }
  },
  Mutation: {
    create_category: async (parent, { company, body }, context, info) => {
      let cat = new PostCategoryLib()

      return await cat.postCategory(company, body)
    },
    update_category: async (parent, { company, body }, context, info) => {
      let cat = new PutCategoryLib()

      return await cat.putCategory(company, body)
    }
  }
};

export const categories_query = `
  get_category_by_id(company: String, id: String): CategoriesType

  get_categories(company: String, per_page: Int, page: Int): [CategoriesType]
`
export const categories_mutation = `
  create_category(company: String, body: CategoriesInput): CategoriesType

  update_category(company: String, body: CategoriesUpdateInput): CategoriesType
`
// export const graphqlCategories = `
// ${CategoriesType}
// ${CategoriesInput}
// ${CustomerType}
// ${DocumentsType}
// ${AddressType}
// ${ImagesType}
// ${CompanyType}
// type Query {
//   get_category_by_id(company: String, id: String): [CategoriesType]

//   get_categories(company: String, per_page: Int, page: Int): CategoriesType
// }

// type Mutation {
//   create_category(company: String, body: CategoriesInput): String

//   update_category(company: String, body: String): String
// }
// `

// export const schemaCategories = buildSchema(graphqlCategories)