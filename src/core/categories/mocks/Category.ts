export interface Category {
    _id : String;
    company: String;
    id_category: Number,
    name: String;
    parent_id : String;
}
