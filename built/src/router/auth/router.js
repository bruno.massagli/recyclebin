"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const express = tslib_1.__importStar(require("express"));
const get_all_companies_1 = require("../../services/auth/get_all_companies");
const get_company_by_id_1 = require("../../services/auth/get_company_by_id");
const get_auth_by_customer_id_1 = require("../../services/auth/get_auth_by_customer_id");
const put_company_1 = require("../../services/auth/put_company");
const put_auth_1 = require("../../services/auth/put_auth");
const post_company_1 = require("../../services/auth/post_company");
const post_auth_1 = require("../../services/auth/post_auth");
const auth_1 = require("../middlewares/auth");
const error_1 = require("../middlewares/error");
const login_1 = require("../../services/auth/login");
class AuthRouter {
    constructor() {
        const router_auth = express.Router();
        router_auth.get('/health', function (req, res, next) {
            res.send(true);
        });
        router_auth.get('/login', [auth_1.auth, error_1.error], login_1.loginService);
        router_auth.get('/auth/:id', [auth_1.auth, error_1.error], get_auth_by_customer_id_1.getAuthByIdService);
        router_auth.put('/auth', [auth_1.auth, error_1.error], put_auth_1.putAuthService);
        router_auth.post('/auth', [auth_1.auth, error_1.error], post_auth_1.postAuthService);
        router_auth.get('/company', [auth_1.auth, error_1.error], get_all_companies_1.getAllCompaniesService);
        router_auth.get('/company/:id', [auth_1.auth, error_1.error], get_company_by_id_1.getCompanyByIdService);
        router_auth.put('/company', [auth_1.auth, error_1.error], put_company_1.putCompanyService);
        router_auth.post('/company', [auth_1.auth, error_1.error], post_company_1.postCompanyService);
        this.router = router_auth;
    }
}
exports.AuthRouter = AuthRouter;
//# sourceMappingURL=router.js.map