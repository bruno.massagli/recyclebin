"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const companies_1 = require("../../db/Mongo/Models/companies");
const lodash_1 = tslib_1.__importDefault(require("lodash"));
function auth(req, res, next) {
    return tslib_1.__awaiter(this, void 0, void 0, function* () {
        let company_model = new companies_1.Companies();
        const API_KEY = req.get('api-key');
        if (!API_KEY) {
            res.status(401).send('EMPTY_API_KEY');
            throw new Error('EMPTY_API_KEY');
        }
        const company_obj = yield company_model.companiesModel().findOne({ 'api_key': API_KEY }).exec();
        const company = lodash_1.default.replace(company_obj._id, /(?:ObjectId\(\")|(?:\))/, "");
        console.log(company);
        if (!company) {
            res.status(403).send('FORBIDDEN');
            throw new Error('FORBIDDEN');
        }
        req = Object.assign(req, { company: company });
        next();
    });
}
exports.auth = auth;
//# sourceMappingURL=auth.js.map