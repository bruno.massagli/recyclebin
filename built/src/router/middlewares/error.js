"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const errors_1 = require("../../helpers/errors");
function error(err, req, res, next) {
    return tslib_1.__awaiter(this, void 0, void 0, function* () {
        let errors = new errors_1.Errors();
        if (err) {
            const response = errors.errors[err.type] || errors.errors['DEFAULT'];
            if (err.descriptor) {
                response.body = JSON.parse(JSON.stringify(response.body, err.descriptor));
            }
            res.status(err.status || 500);
            res.jsonp(response);
        }
        next();
    });
}
exports.error = error;
//# sourceMappingURL=error.js.map