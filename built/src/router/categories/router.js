"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const express = tslib_1.__importStar(require("express"));
const get_category_by_id_1 = require("../../services/categories/get_category_by_id");
const get_categories_1 = require("../../services/categories/get_categories");
const put_category_1 = require("../../services/categories/put_category");
const post_category_1 = require("../../services/categories/post_category");
const auth_1 = require("../middlewares/auth");
const error_1 = require("../middlewares/error");
class CategoriesRouter {
    constructor() {
        const router_categories = express.Router();
        router_categories.get('/', function () {
            return true;
        });
        router_categories.get('/categories/:id', [auth_1.auth, error_1.error], get_category_by_id_1.GetCategoryByIdService);
        router_categories.get('/categories', [auth_1.auth, error_1.error], get_categories_1.GetCategoriesService);
        router_categories.put('/categories', [auth_1.auth, error_1.error], put_category_1.PutCategoryService);
        router_categories.post('/categories', [auth_1.auth, error_1.error], post_category_1.PostCategoryService);
        this.router = router_categories;
    }
}
exports.CategoriesRouter = CategoriesRouter;
//# sourceMappingURL=router.js.map