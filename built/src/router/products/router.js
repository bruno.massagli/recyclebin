"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const express = tslib_1.__importStar(require("express"));
const get_all_products_1 = require("../../services/products/get_all_products");
const get_products_by_id_1 = require("../../services/products/get_products_by_id");
const get_products_1 = require("../../services/products/get_products");
const put_products_1 = require("../../services/products/put_products");
const post_products_1 = require("../../services/products/post_products");
const auth_1 = require("../middlewares/auth");
const error_1 = require("../middlewares/error");
class ProductsRouter {
    constructor() {
        const router_products = express.Router();
        router_products.get('/', function () {
            return true;
        });
        router_products.get('/products/:search', [auth_1.auth, error_1.error], get_all_products_1.GetAllProductsService);
        router_products.get('/product/:id', [auth_1.auth, error_1.error], get_products_by_id_1.GetProductsByIdService);
        router_products.get('/products', [auth_1.auth, error_1.error], get_products_1.GetProductsService);
        router_products.put('/products', [auth_1.auth, error_1.error], put_products_1.PutProductsService);
        router_products.post('/products', [auth_1.auth, error_1.error], post_products_1.PostProductsService);
        this.router = router_products;
    }
}
exports.ProductsRouter = ProductsRouter;
//# sourceMappingURL=router.js.map