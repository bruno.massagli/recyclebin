"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const express = tslib_1.__importStar(require("express"));
const get_orders_1 = require("../../services/orders/get_orders");
const get_customer_by_email_1 = require("../../services/orders/get_customer_by_email");
const get_orders_by_id_1 = require("../../services/orders/get_orders_by_id");
const post_order_1 = require("../../services/orders/post_order");
const put_order_cancelled_1 = require("../../services/orders/put_order_cancelled");
const put_order_invoiced_1 = require("../../services/orders/put_order_invoiced");
const put_order_sent_1 = require("../../services/orders/put_order_sent");
const put_order_delivered_1 = require("../../services/orders/put_order_delivered");
const auth_1 = require("../middlewares/auth");
const error_1 = require("../middlewares/error");
class OrdersRouter {
    constructor() {
        const router_orders = express.Router();
        router_orders.get('/', function () {
            return true;
        });
        router_orders.get('/customer/:email', [auth_1.auth, error_1.error], get_customer_by_email_1.GetCustomerByEmailService);
        router_orders.get('/orders/:id', [auth_1.auth, error_1.error], get_orders_by_id_1.GetOrdersByIdService);
        router_orders.get('/orders', [auth_1.auth, error_1.error], get_orders_1.GetOrdersService);
        router_orders.post('/orders', [auth_1.auth, error_1.error], post_order_1.PostOrderService);
        router_orders.put('/orders/cancelled', [auth_1.auth, error_1.error], put_order_cancelled_1.PutOrdersCancelledService);
        router_orders.put('/orders/invoiced', [auth_1.auth, error_1.error], put_order_invoiced_1.PutOrdersInvoicedService);
        router_orders.put('/orders/sent', [auth_1.auth, error_1.error], put_order_sent_1.PutOrdersSentService);
        router_orders.put('/orders/delivered', [auth_1.auth, error_1.error], put_order_delivered_1.PutOrdersDeliveredService);
        this.router = router_orders;
    }
}
exports.OrdersRouter = OrdersRouter;
//# sourceMappingURL=router.js.map