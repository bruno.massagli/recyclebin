"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function normalizeNumber(number) {
    if (number) {
        number = number.toString().trim();
        let cleanNumber = number.replace('-', '');
        cleanNumber = cleanNumber.replace('(', '');
        cleanNumber = cleanNumber.replace(')', '');
        cleanNumber = cleanNumber.replace(/\s/g, '');
        let phone = cleanNumber.length;
        if (cleanNumber.length >= 10 && cleanNumber.length <= 11) {
            phone = `${cleanNumber.substring(0, 2)} ${cleanNumber.substring(2)}`;
        }
        if (cleanNumber.length >= 8 && cleanNumber.length <= 9) {
            phone = `00 ${cleanNumber}`;
        }
        return phone;
    }
}
exports.normalizeNumber = normalizeNumber;
//# sourceMappingURL=normalize-phone-number.js.map