"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Errors {
    constructor() {
        this.errors = {
            "DEFAULT": {
                "status": 500,
                "body": {
                    "code": 1,
                    "message": "Erro ao processar requisição"
                }
            },
            "REQUIRED_DRIVE": {
                "status": 500,
                "body": {
                    "code": 2,
                    "message": "É obrigatório passar o parametro drive na querystring"
                }
            },
            "INVALID_DRIVE": {
                "status": 500,
                "body": {
                    "code": 3,
                    "message": "Foi passado um drive inexistente na querystring"
                }
            },
            "NOT_IMPLEMENTED_DRIVE": {
                "status": 500,
                "body": {
                    "code": 4,
                    "message": "Foi passado um drive não implementado na querystring"
                }
            },
            "EMPTY_API_KEY": {
                "status": 401,
                "body": {
                    "code": 5,
                    "message": "É obrigatório passar o api-key no header"
                }
            },
            "COMPANY_NOT_FOUND": {
                "status": 500,
                "body": {
                    "code": 6,
                    "message": "O token fornecido é inexistente"
                }
            },
            "COMPANY_NAME_NOT_EMPTY": {
                "status": 500,
                "body": {
                    "code": 7,
                    "message": "O campo name é obrigatório"
                }
            },
            "COMPANY_NOT_EXIST": {
                "status": 500,
                "body": {
                    "code": 8,
                    "message": "Não foi encontrado nenhuma empresa com essa api-key"
                }
            },
            "NOT_ADMIN": {
                "status": 500,
                "body": {
                    "code": 9,
                    "message": "Você não possuí permissão para acessar essa ação"
                }
            },
            "ONE_DRIVE_ONLY": {
                "status": 500,
                "body": {
                    "code": 10,
                    "message": "Para essa requisição só pode ser passado apenas um drive"
                }
            },
            "FORBIDDEN": {
                "status": 403,
                "body": {
                    "code": 10,
                    "message": "Para essa requisição só pode ser passado apenas um drive"
                }
            }
        };
    }
}
exports.Errors = Errors;
//# sourceMappingURL=errors.js.map