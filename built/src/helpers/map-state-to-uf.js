"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function MapState(state) {
    let data = state;
    switch (state.toUpperCase()) {
        case 'ACRE':
            data = 'AC';
            break;
        case 'ALAGOAS':
            data = 'AL';
            break;
        case 'AMAZONAS':
            data = 'AM';
            break;
        case 'AMAPÁ':
            data = 'AP';
            break;
        case 'BAHIA':
            data = 'BA';
            break;
        case 'CEARÁ':
            data = 'CE';
            break;
        case 'DISTRITO FEDERAL':
            data = 'DF';
            break;
        case 'ESPÍRITO SANTO':
            data = 'ES';
            break;
        case 'GOIÁS':
            data = 'GO';
            break;
        case 'MARANHÃO':
            data = 'MA';
            break;
        case 'MINAS GERAIS':
            data = 'MG';
            break;
        case 'MATO GROSSO DO SUL':
            data = 'MS';
            break;
        case 'MATO GROSSO':
            data = 'MT';
            break;
        case 'PARÁ':
            data = 'PA';
            break;
        case 'PARAÍBA':
            data = 'PB';
            break;
        case 'PERNAMBUCO':
            data = 'PE';
            break;
        case 'PIAUÍ':
            data = 'PI';
            break;
        case 'PARANÁ':
            data = 'PR';
            break;
        case 'RIO DE JANEIRO':
            data = 'RJ';
            break;
        case 'RIO GRANDE DO NORTE':
            data = 'RN';
            break;
        case 'RONDÔNIA':
            data = 'RO';
            break;
        case 'RORAIMA':
            data = 'RR';
            break;
        case 'RIO GRANDE DO SUL':
            data = 'RS';
            break;
        case 'SANTA CATARINA':
            data = 'SC';
            break;
        case 'SERGIPE':
            data = 'SE';
            break;
        case 'SÃO PAULO':
            data = 'SP';
            break;
        case 'TOCANTÍNS':
            data = 'TO';
            break;
    }
    return data;
}
exports.MapState = MapState;
//# sourceMappingURL=map-state-to-uf.js.map