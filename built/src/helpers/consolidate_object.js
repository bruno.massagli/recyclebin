"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class ConsolidateObject {
    setClassProps(class_to_set, class_to_assign) {
        Reflect.ownKeys(class_to_assign).map((name, idx) => {
            class_to_set[name] = class_to_assign[name];
        });
        return class_to_set;
    }
}
exports.ConsolidateObject = ConsolidateObject;
//# sourceMappingURL=consolidate_object.js.map