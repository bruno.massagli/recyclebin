"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const moment_tz = tslib_1.__importStar(require("moment-timezone"));
module.exports = moment_tz.tz.setDefault('America/Sao_Paulo');
//# sourceMappingURL=moment.js.map