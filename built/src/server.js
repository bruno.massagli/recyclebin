"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const body_parser_1 = tslib_1.__importDefault(require("body-parser"));
const cookie_parser_1 = tslib_1.__importDefault(require("cookie-parser"));
const express_1 = tslib_1.__importDefault(require("express"));
const morgan_1 = tslib_1.__importDefault(require("morgan"));
const path_1 = tslib_1.__importDefault(require("path"));
const errorhandler_1 = tslib_1.__importDefault(require("errorhandler"));
const index_1 = require("./router/index");
class Server {
    static bootstrap() {
        return new Server();
    }
    constructor() {
        this.app = express_1.default();
        this.config();
        this.routes();
        this.api();
    }
    api() {
    }
    config() {
        this.app.use(express_1.default.static(path_1.default.join(__dirname, "public")));
        this.app.use(morgan_1.default("dev"));
        this.app.use(body_parser_1.default.json());
        this.app.use(body_parser_1.default.urlencoded({
            extended: true
        }));
        this.app.use(body_parser_1.default.text({ type: 'application/graphql' }));
        this.app.use(cookie_parser_1.default("SECRET_GOES_HERE"));
        this.app.use(function (err, req, res, next) {
            err.status = 404;
            next(err);
        });
        this.app.use(errorhandler_1.default());
    }
    routes() {
        let router;
        router = express_1.default.Router();
        index_1.IndexRoute.create(router);
        this.app.use(router);
    }
}
exports.Server = Server;
//# sourceMappingURL=server.js.map