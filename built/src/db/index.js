"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const env_1 = require("../env");
const mongoose_1 = tslib_1.__importDefault(require("mongoose"));
class IndexDB {
    constructor() {
        let environment = new env_1.Env();
        this.mongoose = mongoose_1.default.connect(environment.db.uri, { useNewUrlParser: true });
    }
}
exports.IndexDB = IndexDB;
//# sourceMappingURL=index.js.map