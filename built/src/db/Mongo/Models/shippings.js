"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const mongoose_1 = tslib_1.__importDefault(require("mongoose"));
const items_1 = require("./items");
const tracks_1 = require("./tracks");
const invoices_1 = require("./invoices");
const adresses_1 = require("./adresses");
class Shippings {
    shippings() {
        let items = new items_1.Items();
        let tracks = new tracks_1.Tracks();
        let invoices = new invoices_1.Invoices();
        let address = new adresses_1.Address();
        const shippings = new mongoose_1.default.Schema({
            _id: mongoose_1.default.Schema.Types.ObjectId,
            carrier: String,
            shipping_method: String,
            shipping_date: Date,
            shipping_code: String,
            shipping_cost: Number,
            shipping_estimated_delivery: Date,
            delivery_date: Date,
            shipping_address: address.address(),
            items: [items.items()],
            tracks: [tracks.tracks()],
            invoices: [invoices.invoices()]
        }, { collection: 'shippings', timestamps: true });
        return shippings;
    }
    shippingsModel() {
        return mongoose_1.default.models.shippings || mongoose_1.default.model('shippings', this.shippings(), 'shippings');
    }
}
exports.Shippings = Shippings;
//# sourceMappingURL=shippings.js.map