"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const mongoose_1 = tslib_1.__importDefault(require("mongoose"));
class Specifications {
    specifications() {
        const specifications = new mongoose_1.default.Schema({
            key: String,
            value: String,
            createdAt: Date,
            updatedAt: Date
        }, { collection: 'specifications', timestamps: true });
        return specifications;
    }
    specificationsModel() {
        return mongoose_1.default.models.specifications || mongoose_1.default.model('specifications', this.specifications(), 'specifications');
    }
}
exports.Specifications = Specifications;
//# sourceMappingURL=specifications.js.map