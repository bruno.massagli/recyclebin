"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const mongoose_1 = tslib_1.__importDefault(require("mongoose"));
const items_1 = require("./items");
class Invoices {
    invoices() {
        let items_invoice = new items_1.Items();
        const invoices = new mongoose_1.default.Schema({
            _id: mongoose_1.default.Schema.Types.ObjectId,
            key: String,
            issue_date: Date,
            items: [items_invoice.items()],
            createdAt: Date,
            updatedAt: Date
        }, { collection: 'invoices', timestamps: true });
        return invoices;
    }
    invoicesModel() {
        return mongoose_1.default.models.invoices || mongoose_1.default.model('invoices', this.invoices(), 'invoices');
    }
}
exports.Invoices = Invoices;
//# sourceMappingURL=invoices.js.map