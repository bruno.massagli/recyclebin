"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const mongoose_1 = tslib_1.__importDefault(require("mongoose"));
class Stocks {
    stocks() {
        const stocks = new mongoose_1.default.Schema({
            quantity: Number,
            reserved: Number,
            unit: String,
            createdAt: Date,
            updatedAt: Date
        }, { collection: 'stocks', timestamps: true });
        return stocks;
    }
    stocksModel() {
        return mongoose_1.default.models.stocks || mongoose_1.default.model('stocks', this.stocks(), 'stocks');
    }
}
exports.Stocks = Stocks;
//# sourceMappingURL=stocks.js.map