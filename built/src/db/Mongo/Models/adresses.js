"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const mongoose_1 = tslib_1.__importDefault(require("mongoose"));
class Address {
    address() {
        const address = new mongoose_1.default.Schema({
            street: String,
            number: Number,
            detail: String,
            neighborhood: String,
            city: String,
            region: String,
            country: String,
            postcode: String,
            createdAt: Date,
            updatedAt: Date
        }, { collection: 'address', timestamps: true });
        return address;
    }
    addressModel() {
        return mongoose_1.default.models.address || mongoose_1.default.model('address', this.address(), 'address');
    }
}
exports.Address = Address;
//# sourceMappingURL=adresses.js.map