"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const mongoose_1 = tslib_1.__importDefault(require("mongoose"));
class Document {
    document() {
        const document = new mongoose_1.default.Schema({
            number: String,
            type: String
        }, { collection: 'document', timestamps: true });
        return document;
    }
    documentModel() {
        return mongoose_1.default.models.document || mongoose_1.default.model('document', this.document(), 'document');
    }
}
exports.Document = Document;
//# sourceMappingURL=document.js.map