"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const mongoose_1 = tslib_1.__importDefault(require("mongoose"));
const variations_1 = require("./variations");
const categories_1 = require("./categories");
class Products {
    products() {
        let variations = new variations_1.Variations();
        let categories = new categories_1.Categories();
        const products = new mongoose_1.default.Schema({
            _id: mongoose_1.default.Schema.Types.ObjectId,
            name: String,
            company: String,
            id_product: String,
            brand: String,
            description: String,
            categories: [categories.categories()],
            warranty: String,
            variations: [variations.variations()],
            createdAt: Date,
            updatedAt: Date
        }, { collection: 'products', timestamps: true });
        return products;
    }
    productsModel() {
        return mongoose_1.default.models.products || mongoose_1.default.model('products', this.products(), 'products');
    }
}
exports.Products = Products;
//# sourceMappingURL=products.js.map