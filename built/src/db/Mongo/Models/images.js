"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const mongoose_1 = tslib_1.__importDefault(require("mongoose"));
class Images {
    images() {
        const images = new mongoose_1.default.Schema({
            id: String,
            name: String,
            url: String,
            createdAt: Date,
            updatedAt: Date
        }, { collection: 'images', timestamps: true });
        return images;
    }
    imagesModel() {
        return mongoose_1.default.models.images || mongoose_1.default.model('images', this.images(), 'images');
    }
}
exports.Images = Images;
//# sourceMappingURL=images.js.map