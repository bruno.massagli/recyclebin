"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const mongoose_1 = tslib_1.__importDefault(require("mongoose"));
const shippings_1 = require("./shippings");
const customers_1 = require("./customers");
class Orders {
    orders() {
        let shippings = new shippings_1.Shippings();
        let customers = new customers_1.Customers();
        const orders = new mongoose_1.default.Schema({
            _id: mongoose_1.default.Schema.Types.ObjectId,
            company: String,
            order_number: String,
            order_date: Date,
            total_gross: Number,
            status: String,
            shipping: shippings.shippings(),
            customer: customers.customers(),
            createdAt: Date,
            updatedAt: Date
        }, { collection: 'orders', timestamps: true });
        return orders;
    }
    ordersModel() {
        return mongoose_1.default.models.orders || mongoose_1.default.model('orders', this.orders(), 'orders');
    }
}
exports.Orders = Orders;
//# sourceMappingURL=orders.js.map