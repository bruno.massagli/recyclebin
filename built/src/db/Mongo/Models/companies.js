"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const mongoose = tslib_1.__importStar(require("mongoose"));
const adresses_1 = require("./adresses");
const images_1 = require("./images");
const document_1 = require("./document");
const customers_1 = require("./customers");
class Companies {
    companies() {
        let customers = new customers_1.Customers();
        let address = new adresses_1.Address();
        let images = new images_1.Images();
        let document = new document_1.Document();
        const companies = new mongoose.Schema({
            _id: mongoose.Schema.Types.ObjectId,
            name: String,
            email: String,
            api_key: String,
            document: document.document(),
            phones: [String],
            address: address.address(),
            avatar: images.images(),
            customers: [customers.customers()],
            createdAt: Date,
            updatedAt: Date
        }, { collection: 'companies', timestamps: true });
        return companies;
    }
    companiesModel() {
        return mongoose.models.companies || mongoose.model('companies', this.companies(), 'companies');
    }
}
exports.Companies = Companies;
//# sourceMappingURL=companies.js.map