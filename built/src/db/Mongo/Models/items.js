"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const mongoose_1 = tslib_1.__importDefault(require("mongoose"));
class Items {
    items() {
        const items = new mongoose_1.default.Schema({
            _id: mongoose_1.default.Schema.Types.ObjectId,
            sku: String,
            name: String,
            quantity: Number,
            original_price: Number,
            special_price: Number,
            createdAt: Date,
            updatedAt: Date
        }, { collection: 'items', timestamps: true });
        return items;
    }
    itemsModel() {
        return mongoose_1.default.models.items || mongoose_1.default.model('items', this.items(), 'items');
    }
}
exports.Items = Items;
//# sourceMappingURL=items.js.map