"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const mongoose = tslib_1.__importStar(require("mongoose"));
const customers_1 = require("./customers");
class Auth {
    auth() {
        let customers = new customers_1.Customers();
        const auth = new mongoose.Schema({
            _id: mongoose.Schema.Types.ObjectId,
            email: String,
            password: String,
            access_level: String,
            customer: customers.customers(),
            createdAt: Date,
            updatedAt: Date
        }, { collection: 'auth', timestamps: true });
        return auth;
    }
    authModel() {
        return mongoose.models.auth || mongoose.model('auth', this.auth(), 'auth');
    }
}
exports.Auth = Auth;
//# sourceMappingURL=auth.js.map