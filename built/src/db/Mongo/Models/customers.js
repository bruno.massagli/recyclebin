"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const mongoose = tslib_1.__importStar(require("mongoose"));
const adresses_1 = require("./adresses");
const images_1 = require("./images");
const document_1 = require("./document");
class Customers {
    customers() {
        let address = new adresses_1.Address();
        let images = new images_1.Images();
        let document = new document_1.Document();
        const customers = new mongoose.Schema({
            _id: mongoose.Schema.Types.ObjectId,
            id_customer: Number,
            name: String,
            email: String,
            document: document.document(),
            phones: [String],
            address: address.address(),
            avatar: images.images(),
            createdAt: Date,
            updatedAt: Date
        }, { collection: 'customers', timestamps: true });
        return customers;
    }
    customersModel() {
        return mongoose.models.customers || mongoose.model('customers', this.customers(), 'customers');
    }
}
exports.Customers = Customers;
//# sourceMappingURL=customers.js.map