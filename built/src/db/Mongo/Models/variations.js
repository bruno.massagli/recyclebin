"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const mongoose_1 = tslib_1.__importDefault(require("mongoose"));
const images_1 = require("./images");
const videos_1 = require("./videos");
const specifications_1 = require("./specifications");
const stocks_1 = require("./stocks");
const prices_1 = require("./prices");
const dimensions_1 = require("./dimensions");
class Variations {
    variations() {
        let images = new images_1.Images();
        let videos = new videos_1.Videos();
        let specifications = new specifications_1.Specifications();
        let stocks = new stocks_1.Stocks();
        let prices = new prices_1.Prices();
        let dimensions = new dimensions_1.Dimensions();
        const variations = new mongoose_1.default.Schema({
            _id: mongoose_1.default.Schema.Types.ObjectId,
            id_product: String,
            sku: String,
            stock: stocks.stocks(),
            ean: String,
            nbm: String,
            prices: prices.prices(),
            dimensions: dimensions.dimensions(),
            images: [images.images()],
            videos: [videos.videos()],
            specifications: [specifications.specifications()],
            createdAt: Date,
            updatedAt: Date
        }, { collection: 'variations', timestamps: true });
        return variations;
    }
    variationsModel() {
        return mongoose_1.default.models.variations || mongoose_1.default.model('variations', this.variations(), 'variations');
    }
}
exports.Variations = Variations;
//# sourceMappingURL=variations.js.map