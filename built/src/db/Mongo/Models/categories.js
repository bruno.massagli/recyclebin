"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const mongoose_1 = tslib_1.__importDefault(require("mongoose"));
class Categories {
    categories() {
        const categories = new mongoose_1.default.Schema({
            _id: mongoose_1.default.Schema.Types.ObjectId,
            company: String,
            id_category: Number,
            name: String,
            parent_id: String,
            createdAt: Date,
            updatedAt: Date
        }, { collection: 'categories', timestamps: true });
        return categories;
    }
    categoriesModel() {
        return mongoose_1.default.models.categories || mongoose_1.default.model('categories', this.categories(), 'categories');
    }
}
exports.Categories = Categories;
//# sourceMappingURL=categories.js.map