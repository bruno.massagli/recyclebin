"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const mongoose_1 = tslib_1.__importDefault(require("mongoose"));
class Prices {
    prices() {
        const prices = new mongoose_1.default.Schema({
            list_price: Number,
            sale_price: Number,
            createdAt: Date,
            updatedAt: Date
        }, { collection: 'prices', timestamps: true });
        return prices;
    }
    pricesModel() {
        return mongoose_1.default.models.prices || mongoose_1.default.model('prices', this.prices(), 'prices');
    }
}
exports.Prices = Prices;
//# sourceMappingURL=prices.js.map