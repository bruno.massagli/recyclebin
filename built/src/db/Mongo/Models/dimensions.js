"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const mongoose_1 = tslib_1.__importDefault(require("mongoose"));
class Dimensions {
    dimensions() {
        const dimensions = new mongoose_1.default.Schema({
            weight: Number,
            length: Number,
            width: Number,
            height: Number,
            cubic_wheight: Number,
            createdAt: Date,
            updatedAt: Date
        }, { collection: 'dimensions', timestamps: true });
        return dimensions;
    }
    dimensionsModel() {
        return mongoose_1.default.models.dimensions || mongoose_1.default.model('dimensions', this.dimensions(), 'dimensions');
    }
}
exports.Dimensions = Dimensions;
//# sourceMappingURL=dimensions.js.map