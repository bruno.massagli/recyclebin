"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const mongoose_1 = tslib_1.__importDefault(require("mongoose"));
class Videos {
    videos() {
        const videos = new mongoose_1.default.Schema({
            id: String,
            name: String,
            url: String,
            createdAt: Date,
            updatedAt: Date
        }, { collection: 'videos', timestamps: true });
        return videos;
    }
    videosModel() {
        return mongoose_1.default.models.videos || mongoose_1.default.model('videos', this.videos(), 'videos');
    }
}
exports.Videos = Videos;
//# sourceMappingURL=videos.js.map