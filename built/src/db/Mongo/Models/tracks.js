"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const mongoose_1 = tslib_1.__importDefault(require("mongoose"));
const items_1 = require("./items");
class Tracks {
    tracks() {
        let items_tracks = new items_1.Items();
        const tracks = new mongoose_1.default.Schema({
            _id: mongoose_1.default.Schema.Types.ObjectId,
            key: String,
            tracking_link: String,
            tracking_number: String,
            items: [items_tracks.items()],
            createdAt: Date,
            updatedAt: Date
        }, { collection: 'tracks', timestamps: true });
        return tracks;
    }
    tracksModel() {
        return mongoose_1.default.models.tracks || mongoose_1.default.model('tracks', this.tracks(), 'tracks');
    }
}
exports.Tracks = Tracks;
//# sourceMappingURL=tracks.js.map