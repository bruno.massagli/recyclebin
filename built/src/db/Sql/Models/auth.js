"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const index_1 = require("../../index");
const Sequelize = tslib_1.__importStar(require("sequelize"));
class Company {
    company() {
        let databases = new index_1.IndexDB();
        const Company = databases.sequelize.define('company', {
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            name: {
                type: Sequelize.STRING
            },
            api_key: {
                type: Sequelize.STRING
            },
            document_number: {
                type: Sequelize.STRING
            },
            document_type: {
                type: Sequelize.STRING
            },
            street: {
                type: Sequelize.STRING
            },
            number: {
                type: Sequelize.INTEGER,
            },
            detail: {
                type: Sequelize.STRING
            },
            neighborhood: {
                type: Sequelize.STRING
            },
            city: {
                type: Sequelize.STRING
            },
            region: {
                type: Sequelize.STRING
            },
            country: {
                type: Sequelize.STRING
            },
            postcode: {
                type: Sequelize.STRING
            }
        }, {
            timestamps: true
        });
        return Company;
    }
}
exports.Company = Company;
//# sourceMappingURL=auth.js.map