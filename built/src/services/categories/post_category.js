"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const post_category_lib_1 = require("../../core/categories/lib/post_category_lib");
function PostCategoryService(req, res, next) {
    return tslib_1.__awaiter(this, void 0, void 0, function* () {
        const company = req.company;
        let body = req.body;
        body['rest'] = true;
        const postCategory = new post_category_lib_1.PostCategoryLib();
        const categories = yield postCategory.postCategory(company, body);
        return res.jsonp(categories);
    });
}
exports.PostCategoryService = PostCategoryService;
//# sourceMappingURL=post_category.js.map