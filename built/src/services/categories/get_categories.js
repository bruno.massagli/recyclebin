"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const get_categories_lib_1 = require("../../core/categories/lib/get_categories_lib");
function GetCategoriesService(req, res, next) {
    return tslib_1.__awaiter(this, void 0, void 0, function* () {
        const company = req.company;
        const page = Number(req.query.page);
        const per_page = Number(req.query.per_page);
        const getCategories = new get_categories_lib_1.GetCategoriesLib();
        const categories = yield getCategories.getCategories(company, per_page, page);
        res.jsonp(categories);
    });
}
exports.GetCategoriesService = GetCategoriesService;
//# sourceMappingURL=get_categories.js.map