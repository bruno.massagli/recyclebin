"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const put_category_lib_1 = require("../../core/categories/lib/put_category_lib");
function PutCategoryService(req, res, next) {
    return tslib_1.__awaiter(this, void 0, void 0, function* () {
        const company = req.company;
        let body = req.body;
        body['rest'] = true;
        const putCategory = new put_category_lib_1.PutCategoryLib();
        const categories = yield putCategory.putCategory(company, body);
        return res.jsonp(categories);
    });
}
exports.PutCategoryService = PutCategoryService;
//# sourceMappingURL=put_category.js.map