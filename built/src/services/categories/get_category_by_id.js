"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const get_category_by_id_lib_1 = require("../../core/categories/lib/get_category_by_id_lib");
function GetCategoryByIdService(req, res, next) {
    return tslib_1.__awaiter(this, void 0, void 0, function* () {
        const company = req.company;
        const id = req.params.id;
        const getCategoryById = new get_category_by_id_lib_1.GetCategoryByIdLib();
        const categories = yield getCategoryById.getCategoryById(company, id);
        res.jsonp(categories);
    });
}
exports.GetCategoryByIdService = GetCategoryByIdService;
//# sourceMappingURL=get_category_by_id.js.map