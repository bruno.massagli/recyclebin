"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const put_order_status_lib_1 = require("../../core/orders/lib/put_order_status_lib");
function PutOrdersCancelledService(req, res, next) {
    return tslib_1.__awaiter(this, void 0, void 0, function* () {
        const company = req.company;
        let body = req.body;
        const putOrdersStatus = new put_order_status_lib_1.PutOrderStatusLib();
        const orders = yield putOrdersStatus.putOrdersStatus(company, 'cancelled', body, true);
        return res.jsonp(orders);
    });
}
exports.PutOrdersCancelledService = PutOrdersCancelledService;
//# sourceMappingURL=put_order_cancelled.js.map