"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const put_order_status_lib_1 = require("../../core/orders/lib/put_order_status_lib");
function PutOrdersInvoicedService(req, res, next) {
    return tslib_1.__awaiter(this, void 0, void 0, function* () {
        const company = req.company;
        let body = req.body;
        const putOrdersStatus = new put_order_status_lib_1.PutOrderStatusLib();
        const orders = yield putOrdersStatus.putOrdersStatus(company, 'invoiced', body, true);
        return res.jsonp(orders);
    });
}
exports.PutOrdersInvoicedService = PutOrdersInvoicedService;
//# sourceMappingURL=put_order_invoiced.js.map