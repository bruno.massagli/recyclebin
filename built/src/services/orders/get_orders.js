"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const get_orders_lib_1 = require("../../core/orders/lib/get_orders_lib");
function GetOrdersService(req, res, next) {
    return tslib_1.__awaiter(this, void 0, void 0, function* () {
        const company = req.company;
        const page = Number(req.query.page);
        const per_page = Number(req.query.per_page);
        const { status, start_date, end_date } = req.query;
        const getOrders = new get_orders_lib_1.GetOrdersLib();
        const orders = yield getOrders.getOrders(company, status, per_page, page, start_date, end_date, true);
        res.jsonp(orders);
    });
}
exports.GetOrdersService = GetOrdersService;
//# sourceMappingURL=get_orders.js.map