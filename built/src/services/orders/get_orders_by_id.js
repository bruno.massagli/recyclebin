"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const get_order_by_id_lib_1 = require("../../core/orders/lib/get_order_by_id_lib");
function GetOrdersByIdService(req, res, next) {
    return tslib_1.__awaiter(this, void 0, void 0, function* () {
        const company = req.company;
        const id = req.params.id;
        const getOrdersById = new get_order_by_id_lib_1.GetOrderByIdLib();
        const orders = yield getOrdersById.getOrdersById(company, id, true);
        res.jsonp(orders);
    });
}
exports.GetOrdersByIdService = GetOrdersByIdService;
//# sourceMappingURL=get_orders_by_id.js.map