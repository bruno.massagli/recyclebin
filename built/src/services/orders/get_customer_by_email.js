"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const get_customers_by_email_lib_1 = require("../../core/orders/lib/get_customers_by_email_lib");
function GetCustomerByEmailService(req, res, next) {
    return tslib_1.__awaiter(this, void 0, void 0, function* () {
        const company = req.company;
        const email = req.params.email;
        const getCustomersByEmail = new get_customers_by_email_lib_1.GetCustomerLib();
        const customer = yield getCustomersByEmail.getCustomerByEmail(email, true);
        res.jsonp(customer);
    });
}
exports.GetCustomerByEmailService = GetCustomerByEmailService;
//# sourceMappingURL=get_customer_by_email.js.map