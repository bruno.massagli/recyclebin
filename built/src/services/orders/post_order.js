"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const post_order_lib_1 = require("../../core/orders/lib/post_order_lib");
function PostOrderService(req, res, next) {
    return tslib_1.__awaiter(this, void 0, void 0, function* () {
        const company = req.company;
        let body = req.body;
        const postOrders = new post_order_lib_1.PostOrderLib();
        const orders = yield postOrders.PostOrder(company, body, true);
        return res.jsonp(orders);
    });
}
exports.PostOrderService = PostOrderService;
//# sourceMappingURL=post_order.js.map