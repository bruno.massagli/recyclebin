"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const post_product_lib_1 = require("../../core/products/lib/post_product_lib");
function PostProductsService(req, res, next) {
    return tslib_1.__awaiter(this, void 0, void 0, function* () {
        const company = req.company;
        let body = req.body;
        let postProducts = new post_product_lib_1.PostProductLib();
        const products = yield postProducts.postProducts(company, body, true);
        return res.jsonp(products);
    });
}
exports.PostProductsService = PostProductsService;
//# sourceMappingURL=post_products.js.map