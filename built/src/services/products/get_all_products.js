"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const get_all_products_lib_1 = require("../../core/products/lib/get_all_products_lib");
function GetAllProductsService(req, res, next) {
    return tslib_1.__awaiter(this, void 0, void 0, function* () {
        const page = Number(req.query.page);
        const per_page = Number(req.query.per_page);
        const search = req.params.search;
        let getProducts = new get_all_products_lib_1.GetAllProductsLib();
        const products = yield getProducts.getAllProducts(search, per_page, page, true);
        res.jsonp(products);
    });
}
exports.GetAllProductsService = GetAllProductsService;
//# sourceMappingURL=get_all_products.js.map