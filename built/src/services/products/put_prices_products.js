"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const put_product_prices_lib_1 = require("../../core/products/lib/put_product_prices_lib");
function PutPricesProductsService(req, res, next) {
    return tslib_1.__awaiter(this, void 0, void 0, function* () {
        const company = req.company;
        let body = req.body;
        let putProductsPrices = new put_product_prices_lib_1.PutProductPricesLib();
        const products = yield putProductsPrices.putProductsPrices(company, body, true);
        return res.jsonp(products);
    });
}
exports.PutPricesProductsService = PutPricesProductsService;
//# sourceMappingURL=put_prices_products.js.map