"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const get_product_by_id_lib_1 = require("../../core/products/lib/get_product_by_id_lib");
function GetProductsByIdService(req, res, next) {
    return tslib_1.__awaiter(this, void 0, void 0, function* () {
        const company = req.company;
        const id = req.params.id;
        let getProductsById = new get_product_by_id_lib_1.GetProductByIdLib();
        const products = yield getProductsById.getProductsById(company, id, true);
        res.jsonp(products);
    });
}
exports.GetProductsByIdService = GetProductsByIdService;
//# sourceMappingURL=get_products_by_id.js.map