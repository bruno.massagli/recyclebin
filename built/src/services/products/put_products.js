"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const put_product_lib_1 = require("../../core/products/lib/put_product_lib");
function PutProductsService(req, res, next) {
    return tslib_1.__awaiter(this, void 0, void 0, function* () {
        const company = req.company;
        let body = req.body;
        let putProducts = new put_product_lib_1.PutProductlib();
        const products = yield putProducts.putProducts(company, body, true);
        return res.jsonp(products);
    });
}
exports.PutProductsService = PutProductsService;
//# sourceMappingURL=put_products.js.map