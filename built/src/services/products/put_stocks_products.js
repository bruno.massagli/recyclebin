"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const put_product_stocks_lib_1 = require("../../core/products/lib/put_product_stocks_lib");
function PutStocksProductsService(req, res, next) {
    return tslib_1.__awaiter(this, void 0, void 0, function* () {
        const company = req.company;
        let body = req.body;
        let putProductsStocks = new put_product_stocks_lib_1.PutProductStocksLib();
        const products = yield putProductsStocks.putProductsStocks(company, body, true);
        return res.jsonp(products);
    });
}
exports.PutStocksProductsService = PutStocksProductsService;
//# sourceMappingURL=put_stocks_products.js.map