"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const get_product_lib_1 = require("../../core/products/lib/get_product_lib");
function GetProductsService(req, res, next) {
    return tslib_1.__awaiter(this, void 0, void 0, function* () {
        const company = req.company;
        const page = Number(req.query.page);
        const per_page = Number(req.query.per_page);
        const { start_date, end_date } = req.query;
        let getProducts = new get_product_lib_1.GetProductLib();
        const products = yield getProducts.getProducts(company, per_page, page, start_date, end_date, true);
        res.jsonp(products);
    });
}
exports.GetProductsService = GetProductsService;
//# sourceMappingURL=get_products.js.map