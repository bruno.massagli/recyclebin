"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const put_auth_lib_1 = require("../../core/auth/lib/put_auth_lib");
function putAuthService(req, res, next) {
    return tslib_1.__awaiter(this, void 0, void 0, function* () {
        const company = req.company;
        let body = req.body;
        let putAuth = new put_auth_lib_1.PutAuthLib();
        const auth = yield putAuth.putAuth(company, body, true);
        return res.jsonp(auth);
    });
}
exports.putAuthService = putAuthService;
//# sourceMappingURL=put_auth.js.map