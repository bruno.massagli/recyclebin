"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const get_auth_lib_1 = require("../../core/auth/lib/get_auth_lib");
function getAuthByIdService(req, res, next) {
    return tslib_1.__awaiter(this, void 0, void 0, function* () {
        const company = req.company;
        const id = req.params.id;
        let getAuthById = new get_auth_lib_1.GetAuthLib();
        const auth = yield getAuthById.GetAuthByCustomer(company, id, true);
        res.jsonp(auth);
    });
}
exports.getAuthByIdService = getAuthByIdService;
//# sourceMappingURL=get_auth_by_customer_id.js.map