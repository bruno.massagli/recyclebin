"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const post_auth_lib_1 = require("../../core/auth/lib/post_auth_lib");
function postAuthService(req, res, next) {
    return tslib_1.__awaiter(this, void 0, void 0, function* () {
        const company = req.company;
        let body = req.body;
        let postAuth = new post_auth_lib_1.PostAuthLib();
        const auth = yield postAuth.postAuth(company, body, true);
        return res.jsonp(auth);
    });
}
exports.postAuthService = postAuthService;
//# sourceMappingURL=post_auth.js.map