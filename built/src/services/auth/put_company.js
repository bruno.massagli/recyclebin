"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const put_company_lib_1 = require("../../core/auth/lib/put_company_lib");
function putCompanyService(req, res, next) {
    return tslib_1.__awaiter(this, void 0, void 0, function* () {
        const company = req.company;
        let body = req.body;
        let putCompany = new put_company_lib_1.PutCompanyLib();
        const company_res = yield putCompany.putCompany(company, body, true);
        return res.jsonp(company_res);
    });
}
exports.putCompanyService = putCompanyService;
//# sourceMappingURL=put_company.js.map