"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const post_company_lib_1 = require("../../core/auth/lib/post_company_lib");
function postCompanyService(req, res, next) {
    return tslib_1.__awaiter(this, void 0, void 0, function* () {
        const company = req.company;
        let body = req.body;
        let postCompany = new post_company_lib_1.PostCompanyLib();
        const company_res = yield postCompany.postCompany(company, body, true);
        return res.jsonp(company_res);
    });
}
exports.postCompanyService = postCompanyService;
//# sourceMappingURL=post_company.js.map