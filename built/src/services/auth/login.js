"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const login_1 = require("../../core/auth/lib/login");
function loginService(req, res, next) {
    return tslib_1.__awaiter(this, void 0, void 0, function* () {
        const company = req.company;
        let { email, password } = req.body;
        const auth = yield login_1.login(company, email, password, true);
        return res.jsonp(auth);
    });
}
exports.loginService = loginService;
//# sourceMappingURL=login.js.map