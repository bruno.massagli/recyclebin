"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const get_companies_lib_1 = require("../../core/auth/lib/get_companies_lib");
function getAllCompaniesService(req, res, next) {
    return tslib_1.__awaiter(this, void 0, void 0, function* () {
        const company = req.company;
        const per_page = Number(req.query.per_page);
        const page = Number(req.query.page);
        let getCompanies = new get_companies_lib_1.GetCompaniesLib();
        const companies_res = yield getCompanies.getCompanies(company, per_page, page, true);
        res.jsonp(companies_res);
    });
}
exports.getAllCompaniesService = getAllCompaniesService;
//# sourceMappingURL=get_all_companies.js.map