"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const get_company_lib_1 = require("../../core/auth/lib/get_company_lib");
function getCompanyByIdService(req, res, next) {
    return tslib_1.__awaiter(this, void 0, void 0, function* () {
        const company = req.company;
        const id = req.params.id;
        let getCompanyById = new get_company_lib_1.GetCompanyLib();
        const company_res = yield getCompanyById.getCompanyById(company, id, true);
        res.jsonp(company_res);
    });
}
exports.getCompanyByIdService = getCompanyByIdService;
//# sourceMappingURL=get_company_by_id.js.map