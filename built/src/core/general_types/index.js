"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const customers_type_1 = require("../orders/graphql/types/customers_type");
const documents_type_1 = require("../orders/graphql/types/documents_type");
const addresses_type_1 = require("../orders/graphql/types/addresses_type");
const auth_types_1 = require("../auth/graphql/types/auth_types");
const categories_type_1 = require("../categories/graphql/types/categories_type");
const stocks_type_1 = require("../products/graphql/types/stocks_type");
const prices_type_1 = require("../products/graphql/types/prices_type");
const dimensions_type_1 = require("../products/graphql/types/dimensions_type");
const images_type_1 = require("../products/graphql/types/images_type");
const videos_type_1 = require("../products/graphql/types/videos_type");
const specifications_type_1 = require("../products/graphql/types/specifications_type");
const vatriations_type_1 = require("../products/graphql/types/vatriations_type");
const products_type_1 = require("../products/graphql/types/products_type");
const items_type_1 = require("../orders/graphql/types/items_type");
const tracks_type_1 = require("../orders/graphql/types/tracks_type");
const invoices_type_1 = require("../orders/graphql/types/invoices_type");
const shippings_type_1 = require("../orders/graphql/types/shippings_type");
const orders_type_1 = require("../orders/graphql/types/orders_type");
const auth_graphql_1 = require("../auth/graphql/resolver/auth_graphql");
const products_graphql_1 = require("../products/graphql/resolver/products_graphql");
const orders_graphql_1 = require("../orders/graphql/resolver/orders_graphql");
const categories_graphql_1 = require("../categories/graphql/resolver/categories_graphql");
exports.types_graphql = `
scalar MyContext
scalar Date  
scalar DateTime

${documents_type_1.DocumentsType}
${documents_type_1.DocumentsInput}
${documents_type_1.DocumentsUpdateInput}
${addresses_type_1.AddressType}
${addresses_type_1.AddressInput}
${addresses_type_1.AddressUpdateInput}
${customers_type_1.CustomerType}
${customers_type_1.CustomerInput}
${customers_type_1.CustomerUpdateInput}
${auth_types_1.AuthType}
${auth_types_1.AuthInput}
${auth_types_1.AuthUpdateInput}
${auth_types_1.CompanyType}
${auth_types_1.CompanyInput}
${auth_types_1.CompanyUpdateInput}
${categories_type_1.CategoriesType}
${categories_type_1.CategoriesInput}
${categories_type_1.CategoriesUpdateInput}
${stocks_type_1.StocksType}
${stocks_type_1.StocksInput}
${stocks_type_1.StocksUpdateInput}
${prices_type_1.PricesType}
${prices_type_1.PricesInput}
${prices_type_1.PricesUpdateInput}
${dimensions_type_1.DimensionsType}
${dimensions_type_1.DimensionsInput}
${dimensions_type_1.DimensionsUpdateInput}
${images_type_1.ImagesType}
${images_type_1.ImagesInput}
${images_type_1.ImagesUpdateInput}
${videos_type_1.VideosType}
${videos_type_1.VideosInput}
${videos_type_1.VideosUpdateInput}
${specifications_type_1.SpecificationsType}
${specifications_type_1.SpecificationsInput}
${specifications_type_1.SpecificationsUpdateInput}
${vatriations_type_1.VariationsType}
${vatriations_type_1.VariationsInput}
${vatriations_type_1.VariationsUpdateInput}
${products_type_1.ProductsType}
${products_type_1.ProductsInput}
${products_type_1.ProductsUpdateInput}
${items_type_1.ItemsType}
${items_type_1.ItemsInput}
${items_type_1.ItemsUpdateInput}
${tracks_type_1.TracksType}
${tracks_type_1.TracksInput}
${tracks_type_1.TracksUpdateInput}
${invoices_type_1.InvoicesType}
${invoices_type_1.InvoicesInput}
${invoices_type_1.InvoicesUpdateInput}
${shippings_type_1.ShippingsType}
${shippings_type_1.ShippingsInput}
${shippings_type_1.ShippingsUpdateInput}
${orders_type_1.OrdersType}
${orders_type_1.OrdersInput}
${orders_type_1.OrdersUpdateInput}
${orders_type_1.OrderStatusInput}
type Query {
    ${auth_graphql_1.auth_query}
    ${products_graphql_1.products_query}
    ${orders_graphql_1.orders_query}
    ${categories_graphql_1.categories_query}
}
  
  type Mutation {
    ${auth_graphql_1.auth_mutation}
    ${products_graphql_1.products_mutation}
    ${orders_graphql_1.orders_mutation}
    ${categories_graphql_1.categories_mutation}
}
`;
//# sourceMappingURL=index.js.map