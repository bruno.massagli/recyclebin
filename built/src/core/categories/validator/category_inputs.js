"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const class_validator_1 = require("class-validator");
class CategoriesInputPost {
}
tslib_1.__decorate([
    class_validator_1.MaxLength(30),
    class_validator_1.IsDefined()
], CategoriesInputPost.prototype, "name", void 0);
tslib_1.__decorate([
    class_validator_1.IsDefined()
], CategoriesInputPost.prototype, "company", void 0);
exports.CategoriesInputPost = CategoriesInputPost;
class CategoriesInputPut {
}
tslib_1.__decorate([
    class_validator_1.IsDefined()
], CategoriesInputPut.prototype, "_id", void 0);
tslib_1.__decorate([
    class_validator_1.IsOptional(),
    class_validator_1.MaxLength(30)
], CategoriesInputPut.prototype, "name", void 0);
exports.CategoriesInputPut = CategoriesInputPut;
//# sourceMappingURL=category_inputs.js.map