export declare class CategoriesInputPost {
    name: String;
    company: String;
    parent_id: String;
}
export declare class CategoriesInputPut {
    _id: String;
    name: String;
    company: String;
    parent_id: String;
}
