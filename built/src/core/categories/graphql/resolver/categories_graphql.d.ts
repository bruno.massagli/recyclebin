export declare const resolverCategories: {
    Query: {
        get_category_by_id: (parent: any, { company, id }: {
            company: any;
            id: any;
        }, context: any, info: any) => Promise<object>;
        get_categories: (parent: any, { company, per_page, page }: {
            company: any;
            per_page: any;
            page: any;
        }, context: any, info: any) => Promise<object>;
    };
    Mutation: {
        create_category: (parent: any, { company, body }: {
            company: any;
            body: any;
        }, context: any, info: any) => Promise<object>;
        update_category: (parent: any, { company, body }: {
            company: any;
            body: any;
        }, context: any, info: any) => Promise<object>;
    };
};
export declare const categories_query = "\n  get_category_by_id(company: String, id: String): CategoriesType\n\n  get_categories(company: String, per_page: Int, page: Int): [CategoriesType]\n";
export declare const categories_mutation = "\n  create_category(company: String, body: CategoriesInput): CategoriesType\n\n  update_category(company: String, body: CategoriesUpdateInput): CategoriesType\n";
