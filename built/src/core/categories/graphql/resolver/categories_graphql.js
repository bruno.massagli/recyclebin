"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const get_category_by_id_lib_1 = require("../../lib/get_category_by_id_lib");
const get_categories_lib_1 = require("../../lib/get_categories_lib");
const post_category_lib_1 = require("../../lib/post_category_lib");
const put_category_lib_1 = require("../../lib/put_category_lib");
exports.resolverCategories = {
    Query: {
        get_category_by_id: (parent, { company, id }, context, info) => tslib_1.__awaiter(this, void 0, void 0, function* () {
            let cat = new get_category_by_id_lib_1.GetCategoryByIdLib();
            return yield cat.getCategoryById(company, id);
        }),
        get_categories: (parent, { company, per_page, page }, context, info) => tslib_1.__awaiter(this, void 0, void 0, function* () {
            let cat = new get_categories_lib_1.GetCategoriesLib();
            return yield cat.getCategories(company, per_page, page);
        })
    },
    Mutation: {
        create_category: (parent, { company, body }, context, info) => tslib_1.__awaiter(this, void 0, void 0, function* () {
            let cat = new post_category_lib_1.PostCategoryLib();
            return yield cat.postCategory(company, body);
        }),
        update_category: (parent, { company, body }, context, info) => tslib_1.__awaiter(this, void 0, void 0, function* () {
            let cat = new put_category_lib_1.PutCategoryLib();
            return yield cat.putCategory(company, body);
        })
    }
};
exports.categories_query = `
  get_category_by_id(company: String, id: String): CategoriesType

  get_categories(company: String, per_page: Int, page: Int): [CategoriesType]
`;
exports.categories_mutation = `
  create_category(company: String, body: CategoriesInput): CategoriesType

  update_category(company: String, body: CategoriesUpdateInput): CategoriesType
`;
//# sourceMappingURL=categories_graphql.js.map