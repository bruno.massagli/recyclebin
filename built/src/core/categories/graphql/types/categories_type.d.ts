export declare class CategoriesData {
    _id: String;
    name: String;
    company: String;
    parent_id: String;
}
export declare const CategoriesType = "\ntype CategoriesType {\n    _id : ID!\n    name: String\n    company: String\n    parent_id: String\n}\n";
export declare const CategoriesUpdateInput = "\ninput CategoriesUpdateInput {\n    _id : ID!\n    name: String\n    company: String\n    parent_id: String\n}\n";
export declare const CategoriesInput = "\ninput CategoriesInput {\n    _id : ID\n    name: String!\n    company: String!\n    parent_id: String\n}\n";
