"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const type_graphql_1 = require("type-graphql");
let CategoriesData = class CategoriesData {
};
tslib_1.__decorate([
    type_graphql_1.Field(type => String)
], CategoriesData.prototype, "_id", void 0);
tslib_1.__decorate([
    type_graphql_1.Field(type => String)
], CategoriesData.prototype, "name", void 0);
tslib_1.__decorate([
    type_graphql_1.Field(type => String)
], CategoriesData.prototype, "company", void 0);
tslib_1.__decorate([
    type_graphql_1.Field(type => String, { nullable: true })
], CategoriesData.prototype, "parent_id", void 0);
CategoriesData = tslib_1.__decorate([
    type_graphql_1.ObjectType()
], CategoriesData);
exports.CategoriesData = CategoriesData;
exports.CategoriesType = `
type CategoriesType {
    _id : ID!
    name: String
    company: String
    parent_id: String
}
`;
exports.CategoriesUpdateInput = `
input CategoriesUpdateInput {
    _id : ID!
    name: String
    company: String
    parent_id: String
}
`;
exports.CategoriesInput = `
input CategoriesInput {
    _id : ID
    name: String!
    company: String!
    parent_id: String
}
`;
//# sourceMappingURL=categories_type.js.map