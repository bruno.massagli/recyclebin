"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const consolidate_object_1 = require("../../../helpers/consolidate_object");
const categories_1 = require("../../../db/Mongo/Models/categories");
const category_inputs_1 = require("../validator/category_inputs");
const class_validator_1 = require("class-validator");
class PutCategoryLib {
    putCategory(company, body, rest) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            try {
                let Props = new consolidate_object_1.ConsolidateObject();
                let category_class = new category_inputs_1.CategoriesInputPut();
                category_class = Props.setClassProps(category_class, body);
                console.log(category_class);
                let category_validate = yield class_validator_1.validate(category_class);
                if (category_validate.length) {
                    if (rest)
                        return category_validate;
                    throw new Error(JSON.stringify(category_validate));
                }
                let categories_model = new categories_1.Categories();
                var Category = categories_model.categoriesModel();
                let if_exists = yield Category.findOne({ company: company, _id: body._id }).exec();
                if (!if_exists) {
                    if (rest)
                        return { status: 404, message: 'Resource Not Located' };
                    throw new Error('Resource Not Located');
                }
                if (company !== body.company) {
                    if (rest)
                        return { status: 400, message: 'Company Doesnt Match' };
                    throw new Error('Company Doesnt Match');
                }
                let save_categories = yield Category.findOneAndUpdate({ company: company, _id: body._id }, Object.assign({}, body), { new: true }).exec();
                return save_categories;
            }
            catch (err) {
                console.log(err);
                return err;
            }
        });
    }
}
exports.PutCategoryLib = PutCategoryLib;
//# sourceMappingURL=put_category_lib.js.map