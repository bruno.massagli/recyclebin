"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const categories_1 = require("../../../db/Mongo/Models/categories");
class GetCategoriesLib {
    getCategories(company, per_page, page, rest) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            try {
                let categories_model = new categories_1.Categories();
                var Category = categories_model.categoriesModel();
                let categories_query = yield Category.find({ company: company })
                    .skip(per_page * page).limit(per_page).exec();
                return categories_query;
            }
            catch (err) {
                console.log(err);
                return err;
            }
        });
    }
}
exports.GetCategoriesLib = GetCategoriesLib;
//# sourceMappingURL=get_categories_lib.js.map