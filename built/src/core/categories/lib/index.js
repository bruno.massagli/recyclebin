const categories_model = require('../../../db/Mongo/Models/categories');
module.exports = {
    getCategoryById: require('./get_category_by_id_lib'),
    getCategories: require('./get_categories_lib'),
    postCategory: require('./post_category_lib'),
    putCategory: require('./put_category_lib')
};
//# sourceMappingURL=index.js.map