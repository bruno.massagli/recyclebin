export declare class GetCategoryByIdLib {
    getCategoryById(company: any, id: any, rest?: any): Promise<object>;
}
