"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const consolidate_object_1 = require("../../../helpers/consolidate_object");
const categories_1 = require("../../../db/Mongo/Models/categories");
const mongoose_1 = tslib_1.__importDefault(require("mongoose"));
const category_inputs_1 = require("../validator/category_inputs");
const class_validator_1 = require("class-validator");
class PostCategoryLib {
    postCategory(company, body, rest) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            try {
                let Props = new consolidate_object_1.ConsolidateObject();
                let category_class = new category_inputs_1.CategoriesInputPost();
                category_class = Props.setClassProps(category_class, body);
                console.log(category_class);
                let category_validate = yield class_validator_1.validate(category_class);
                if (category_validate.length) {
                    if (rest)
                        return category_validate;
                    throw new Error(JSON.stringify(category_validate));
                }
                if (company !== body.company) {
                    if (rest)
                        return { status: 400, message: 'Company Doesnt Match' };
                    throw new Error('Company Doesnt Match');
                }
                let categories_model = new categories_1.Categories();
                var new_category = categories_model.categoriesModel();
                var save_categories = new new_category(Object.assign({ company: company, _id: new mongoose_1.default.Types.ObjectId() }, body));
                yield save_categories.save();
                return save_categories;
            }
            catch (err) {
                console.log(err);
                return err;
            }
        });
    }
}
exports.PostCategoryLib = PostCategoryLib;
//# sourceMappingURL=post_category_lib.js.map