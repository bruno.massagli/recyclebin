export declare class GetCategoriesLib {
    getCategories(company: any, per_page: any, page: any, rest?: any): Promise<object>;
}
