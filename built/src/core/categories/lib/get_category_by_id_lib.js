"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const categories_1 = require("../../../db/Mongo/Models/categories");
class GetCategoryByIdLib {
    getCategoryById(company, id, rest) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            try {
                let categories_model = new categories_1.Categories();
                var Category = categories_model.categoriesModel();
                let category_query = yield Category.findOne({ _id: id, company: company }).exec();
                if (!category_query) {
                    if (rest)
                        return { status: 404, message: 'Resource Not Located' };
                    throw new Error('Resource Not Located');
                }
                return category_query;
            }
            catch (err) {
                console.log(err);
                return err;
            }
        });
    }
}
exports.GetCategoryByIdLib = GetCategoryByIdLib;
//# sourceMappingURL=get_category_by_id_lib.js.map