"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const type_graphql_1 = require("type-graphql");
const documents_type_1 = require("../../../orders/graphql/types/documents_type");
const customers_type_1 = require("../../../orders/graphql/types/customers_type");
const addresses_type_1 = require("../../../orders/graphql/types/addresses_type");
const images_type_1 = require("../../../products/graphql/types/images_type");
let AuthData = class AuthData {
};
tslib_1.__decorate([
    type_graphql_1.Field(type => String)
], AuthData.prototype, "_id", void 0);
tslib_1.__decorate([
    type_graphql_1.Field(type => String)
], AuthData.prototype, "email", void 0);
tslib_1.__decorate([
    type_graphql_1.Field(type => String)
], AuthData.prototype, "password", void 0);
tslib_1.__decorate([
    type_graphql_1.Field(type => customers_type_1.CustomerData)
], AuthData.prototype, "customer", void 0);
AuthData = tslib_1.__decorate([
    type_graphql_1.ObjectType()
], AuthData);
exports.AuthData = AuthData;
exports.AuthType = `

type AuthType {
    _id: ID!
    email: String
    password: String
    access_level: String
    customer: CustomerType
}
`;
exports.AuthUpdateInput = `

input AuthUpdateInput {
    _id: ID!
    email: String
    password: String
    access_level: String
    customer: CustomerUpdateInput
}
`;
exports.AuthInput = `

input AuthInput {
    email: String!
    password: String!
    access_level: String!
    customer: CustomerInput!
}
`;
let CompanyData = class CompanyData {
};
tslib_1.__decorate([
    type_graphql_1.Field(type => String)
], CompanyData.prototype, "_id", void 0);
tslib_1.__decorate([
    type_graphql_1.Field(type => String)
], CompanyData.prototype, "name", void 0);
tslib_1.__decorate([
    type_graphql_1.Field(type => String)
], CompanyData.prototype, "email", void 0);
tslib_1.__decorate([
    type_graphql_1.Field(type => String)
], CompanyData.prototype, "api_key", void 0);
tslib_1.__decorate([
    type_graphql_1.Field(type => documents_type_1.DocumentsData)
], CompanyData.prototype, "document", void 0);
tslib_1.__decorate([
    type_graphql_1.Field(type => [String])
], CompanyData.prototype, "phones", void 0);
tslib_1.__decorate([
    type_graphql_1.Field(type => addresses_type_1.AddressData)
], CompanyData.prototype, "address", void 0);
tslib_1.__decorate([
    type_graphql_1.Field(type => images_type_1.ImagesData)
], CompanyData.prototype, "avatar", void 0);
tslib_1.__decorate([
    type_graphql_1.Field(type => [customers_type_1.CustomerData])
], CompanyData.prototype, "customers", void 0);
CompanyData = tslib_1.__decorate([
    type_graphql_1.ObjectType()
], CompanyData);
exports.CompanyData = CompanyData;
exports.CompanyType = `

type CompanyType {
    _id : ID!
    name: String
    email: String
    api_key: String
    document: DocumentsType
    phones: [String]
    address: AddressType
    avatar: ImagesType
    customers: [CustomerType]
}
`;
exports.CompanyUpdateInput = `

input CompanyUpdateInput {
    _id : ID!
    name: String
    email: String
    api_key: String
    document: DocumentsUpdateInput
    phones: [String]
    address: AddressUpdateInput
    avatar: ImagesUpdateInput
    customers: [CustomerUpdateInput]
}
`;
exports.CompanyInput = `

input CompanyInput {
    _id : ID
    name: String!
    email: String!
    api_key: String!
    document: DocumentsInput!
    phones: [String!]!
    address: AddressInput!
    avatar: ImagesInput!
    customers: [CustomerInput!]!
}
`;
//# sourceMappingURL=auth_types.js.map