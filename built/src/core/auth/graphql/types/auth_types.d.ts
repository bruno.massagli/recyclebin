import { DocumentsData } from '../../../orders/graphql/types/documents_type';
import { CustomerData } from '../../../orders/graphql/types/customers_type';
import { AddressData } from '../../../orders/graphql/types/addresses_type';
import { ImagesData } from '../../../products/graphql/types/images_type';
export declare class AuthData {
    _id: String;
    email: String;
    password: String;
    customer: CustomerData;
}
export declare const AuthType = "\n\ntype AuthType {\n    _id: ID!\n    email: String\n    password: String\n    access_level: String\n    customer: CustomerType\n}\n";
export declare const AuthUpdateInput = "\n\ninput AuthUpdateInput {\n    _id: ID!\n    email: String\n    password: String\n    access_level: String\n    customer: CustomerUpdateInput\n}\n";
export declare const AuthInput = "\n\ninput AuthInput {\n    email: String!\n    password: String!\n    access_level: String!\n    customer: CustomerInput!\n}\n";
export declare class CompanyData {
    _id: String;
    name: String;
    email: String;
    api_key: String;
    document: DocumentsData;
    phones: String[];
    address: AddressData;
    avatar: ImagesData;
    customers: CustomerData[];
}
export declare const CompanyType = "\n\ntype CompanyType {\n    _id : ID!\n    name: String\n    email: String\n    api_key: String\n    document: DocumentsType\n    phones: [String]\n    address: AddressType\n    avatar: ImagesType\n    customers: [CustomerType]\n}\n";
export declare const CompanyUpdateInput = "\n\ninput CompanyUpdateInput {\n    _id : ID!\n    name: String\n    email: String\n    api_key: String\n    document: DocumentsUpdateInput\n    phones: [String]\n    address: AddressUpdateInput\n    avatar: ImagesUpdateInput\n    customers: [CustomerUpdateInput]\n}\n";
export declare const CompanyInput = "\n\ninput CompanyInput {\n    _id : ID\n    name: String!\n    email: String!\n    api_key: String!\n    document: DocumentsInput!\n    phones: [String!]!\n    address: AddressInput!\n    avatar: ImagesInput!\n    customers: [CustomerInput!]!\n}\n";
