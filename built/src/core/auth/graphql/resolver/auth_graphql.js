"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const get_auth_lib_1 = require("../../lib/get_auth_lib");
const post_auth_lib_1 = require("../../lib/post_auth_lib");
const put_auth_lib_1 = require("../../lib/put_auth_lib");
const get_companies_lib_1 = require("../../lib/get_companies_lib");
const get_company_lib_1 = require("../../lib/get_company_lib");
const post_company_lib_1 = require("../../lib/post_company_lib");
const put_company_lib_1 = require("../../lib/put_company_lib");
const me_1 = require("../../lib/me");
const login_1 = require("../../lib/login");
exports.resolverAuth = {
    Query: {
        me: (parent, { ctx }, context, info) => tslib_1.__awaiter(this, void 0, void 0, function* () {
            return yield me_1.me(ctx);
        }),
        get_auth: (parent, { company, id_customer }, context, info) => tslib_1.__awaiter(this, void 0, void 0, function* () {
            let auth = new get_auth_lib_1.GetAuthLib();
            return yield auth.GetAuthByCustomer(company, id_customer);
        }),
        get_companies: (parent, { company, per_page, page }, context, info) => tslib_1.__awaiter(this, void 0, void 0, function* () {
            let companies = new get_companies_lib_1.GetCompaniesLib();
            return yield companies.getCompanies(company, per_page, page);
        }),
        get_company_by_id: (parent, { company, id }, context, info) => tslib_1.__awaiter(this, void 0, void 0, function* () {
            let companies = new get_company_lib_1.GetCompanyLib();
            return yield companies.getCompanyById(company, id);
        })
    },
    Mutation: {
        login: (parent, { company, email, password }, context, info) => tslib_1.__awaiter(this, void 0, void 0, function* () {
            return yield login_1.login(company, email, password);
        }),
        create_auth: (parent, { company, body }, context, info) => tslib_1.__awaiter(this, void 0, void 0, function* () {
            let auth = new post_auth_lib_1.PostAuthLib();
            return yield auth.postAuth(company, body);
        }),
        update_auth: (parent, { company, body }, context, info) => tslib_1.__awaiter(this, void 0, void 0, function* () {
            let auth = new put_auth_lib_1.PutAuthLib();
            return yield auth.putAuth(company, body);
        }),
        create_company: (parent, { company, body }, context, info) => tslib_1.__awaiter(this, void 0, void 0, function* () {
            let companies = new post_company_lib_1.PostCompanyLib();
            return yield companies.postCompany(company, body);
        }),
        update_company: (parent, { company, body }, context, info) => tslib_1.__awaiter(this, void 0, void 0, function* () {
            let companies = new put_company_lib_1.PutCompanyLib();
            return yield companies.putCompany(company, body);
        })
    }
};
exports.auth_query = `
  me(ctx: MyContext): AuthType

  get_auth(company: String, id_customer: String): AuthType

  get_companies(company: String, per_page: Int, page: Int): [CompanyType]

  get_company_by_id(company: String, id: String): CompanyType
`;
exports.auth_mutation = `
  login(company: String, email: String, password: String): String

  create_auth(company: String, body: AuthInput): AuthType

  update_auth(company: String, body: AuthUpdateInput): AuthType

  create_company(company: String, body: CompanyInput): CompanyType

  update_company(company: String, body: CompanyUpdateInput): CompanyType
`;
//# sourceMappingURL=auth_graphql.js.map