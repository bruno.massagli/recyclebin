export declare const resolverAuth: {
    Query: {
        me: (parent: any, { ctx }: {
            ctx: any;
        }, context: any, info: any) => Promise<any>;
        get_auth: (parent: any, { company, id_customer }: {
            company: any;
            id_customer: any;
        }, context: any, info: any) => Promise<object>;
        get_companies: (parent: any, { company, per_page, page }: {
            company: any;
            per_page: any;
            page: any;
        }, context: any, info: any) => Promise<object>;
        get_company_by_id: (parent: any, { company, id }: {
            company: any;
            id: any;
        }, context: any, info: any) => Promise<object>;
    };
    Mutation: {
        login: (parent: any, { company, email, password }: {
            company: any;
            email: any;
            password: any;
        }, context: any, info: any) => Promise<String>;
        create_auth: (parent: any, { company, body }: {
            company: any;
            body: any;
        }, context: any, info: any) => Promise<object>;
        update_auth: (parent: any, { company, body }: {
            company: any;
            body: any;
        }, context: any, info: any) => Promise<object>;
        create_company: (parent: any, { company, body }: {
            company: any;
            body: any;
        }, context: any, info: any) => Promise<object>;
        update_company: (parent: any, { company, body }: {
            company: any;
            body: any;
        }, context: any, info: any) => Promise<object>;
    };
};
export declare const auth_query = "\n  me(ctx: MyContext): AuthType\n\n  get_auth(company: String, id_customer: String): AuthType\n\n  get_companies(company: String, per_page: Int, page: Int): [CompanyType]\n\n  get_company_by_id(company: String, id: String): CompanyType\n";
export declare const auth_mutation = "\n  login(company: String, email: String, password: String): String\n\n  create_auth(company: String, body: AuthInput): AuthType\n\n  update_auth(company: String, body: AuthUpdateInput): AuthType\n\n  create_company(company: String, body: CompanyInput): CompanyType\n\n  update_company(company: String, body: CompanyUpdateInput): CompanyType\n";
