"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const companies_1 = require("../../../db/Mongo/Models/companies");
const mongoose_1 = tslib_1.__importDefault(require("mongoose"));
const class_validator_1 = require("class-validator");
const consolidate_object_1 = require("../../../helpers/consolidate_object");
const company_inputs_1 = require("../validator/company_inputs");
class PostCompanyLib {
    postCompany(company, body, rest) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            try {
                let Props = new consolidate_object_1.ConsolidateObject();
                let company_class = new company_inputs_1.CompanyInputPost();
                company_class = Props.setClassProps(company_class, body);
                let company_validate = yield class_validator_1.validate(company_class);
                if (company_validate.length) {
                    if (rest)
                        return company_validate;
                    throw new Error(JSON.stringify(company_validate));
                }
                let companies_model = new companies_1.Companies();
                var new_company = companies_model.companiesModel();
                var verify_company_exists = yield new_company.findOne({ email: body.email }).exec();
                if (verify_company_exists) {
                    if (rest)
                        return { status: 409, message: 'Company Already Exists' };
                    throw new Error('Company Already Exists');
                }
                var save_company = new new_company(Object.assign({ _id: new mongoose_1.default.Types.ObjectId() }, body));
                yield save_company.save();
                return save_company;
            }
            catch (err) {
                console.log(err);
                return err;
            }
        });
    }
}
exports.PostCompanyLib = PostCompanyLib;
//# sourceMappingURL=post_company_lib.js.map