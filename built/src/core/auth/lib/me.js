"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const auth_1 = require("../../../db/Mongo/Models/auth");
function me(ctx) {
    return tslib_1.__awaiter(this, void 0, void 0, function* () {
        const auth = new auth_1.Auth();
        var Authentication = auth.authModel();
        const id_user = ctx.req.session.id_user;
        if (!id_user)
            return null;
        return yield Authentication.findOne({ _id: id_user }).exec();
    });
}
exports.me = me;
//# sourceMappingURL=me.js.map