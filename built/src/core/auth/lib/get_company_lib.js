"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const companies_1 = require("../../../db/Mongo/Models/companies");
class GetCompanyLib {
    getCompanyById(company, id, rest) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            try {
                let companies_model = new companies_1.Companies();
                var Company = companies_model.companiesModel();
                let companies_query = yield Company.findOne({ _id: id }).exec();
                if (!companies_query) {
                    if (rest)
                        return { status: 404, message: 'Resource Not Located' };
                    throw new Error('Resource Not Located');
                }
                return companies_query;
            }
            catch (err) {
                console.log(err);
                return err;
            }
        });
    }
}
exports.GetCompanyLib = GetCompanyLib;
//# sourceMappingURL=get_company_lib.js.map