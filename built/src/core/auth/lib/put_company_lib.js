"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const companies_1 = require("../../../db/Mongo/Models/companies");
const consolidate_object_1 = require("../../../helpers/consolidate_object");
const class_validator_1 = require("class-validator");
const company_inputs_1 = require("../validator/company_inputs");
class PutCompanyLib {
    putCompany(company, body, rest) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            try {
                let Props = new consolidate_object_1.ConsolidateObject();
                let company_class = new company_inputs_1.CompanyInputPut();
                company_class = Props.setClassProps(company_class, body);
                let company_validate = yield class_validator_1.validate(company_class);
                if (company_validate.length) {
                    if (rest)
                        return company_validate;
                    throw new Error(JSON.stringify(company_validate));
                }
                let companies_model = new companies_1.Companies();
                var Company = companies_model.companiesModel();
                var verify_company_exists = yield Company.findOne({ email: body.email }).exec();
                if (verify_company_exists) {
                    if (rest)
                        return { status: 409, message: 'Company Already Exists' };
                    throw new Error('Company Already Exists');
                }
                let if_exists = yield Company.findOne({ _id: body._id }).exec();
                if (!if_exists) {
                    if (rest)
                        return { status: 404, message: 'Resource Not Located' };
                    throw new Error('Resource Not Located');
                }
                let save_company = yield Company.findOneAndUpdate({ _id: body._id }, Object.assign({}, body), { new: true }).exec();
                return save_company;
            }
            catch (err) {
                console.log(err);
                return err;
            }
        });
    }
}
exports.PutCompanyLib = PutCompanyLib;
//# sourceMappingURL=put_company_lib.js.map