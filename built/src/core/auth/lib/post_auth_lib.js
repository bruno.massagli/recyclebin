"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const auth_1 = require("../../../db/Mongo/Models/auth");
const customers_1 = require("../../../db/Mongo/Models/customers");
const bcryptjs_1 = tslib_1.__importDefault(require("bcryptjs"));
const mongoose_1 = tslib_1.__importDefault(require("mongoose"));
const class_validator_1 = require("class-validator");
const auth_inputs_1 = require("../validator/auth_inputs");
const consolidate_object_1 = require("../../../helpers/consolidate_object");
class PostAuthLib {
    postAuth(company, body, rest) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            try {
                let Props = new consolidate_object_1.ConsolidateObject();
                let auth_class = new auth_inputs_1.AuthInputPost();
                auth_class = Props.setClassProps(auth_class, body);
                let auth_validate = yield class_validator_1.validate(auth_class);
                if (auth_validate.length) {
                    if (rest)
                        return auth_validate;
                    throw new Error(JSON.stringify(auth_validate));
                }
                let auth_model = new auth_1.Auth();
                var customer_model = new customers_1.Customers();
                var new_auth = auth_model.authModel();
                var new_customer = customer_model.customersModel();
                var verify_auth_exists = yield new_auth.findOne({ email: body.email }).exec();
                if (verify_auth_exists) {
                    if (rest)
                        return { status: 400, message: 'email already taken' };
                    throw new Error('email already taken');
                }
                var save_customer = yield new_customer.findOne({ email: body.email }).exec();
                console.log(save_customer);
                if (!save_customer || !save_customer.length) {
                    save_customer = new new_customer(Object.assign({ _id: new mongoose_1.default.Types.ObjectId() }, body.customer));
                    save_customer.save();
                }
                body.customer = save_customer;
                body.password = yield bcryptjs_1.default.hash(body.password, 12);
                var save_auth = new new_auth(Object.assign({ _id: new mongoose_1.default.Types.ObjectId() }, body));
                console.log(save_auth);
                yield save_auth.save();
                return save_auth;
            }
            catch (err) {
                console.log(err);
                return err;
            }
        });
    }
}
exports.PostAuthLib = PostAuthLib;
//# sourceMappingURL=post_auth_lib.js.map