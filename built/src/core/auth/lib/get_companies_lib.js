"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const companies_1 = require("../../../db/Mongo/Models/companies");
class GetCompaniesLib {
    getCompanies(company, per_page, page, rest) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            try {
                let companies_model = new companies_1.Companies();
                var Company = companies_model.companiesModel();
                let companies_query = yield Company.find({})
                    .skip(per_page * page).limit(per_page).exec();
                return companies_query;
            }
            catch (err) {
                console.log(err);
                return err;
            }
        });
    }
}
exports.GetCompaniesLib = GetCompaniesLib;
//# sourceMappingURL=get_companies_lib.js.map