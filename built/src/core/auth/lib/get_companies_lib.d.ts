export declare class GetCompaniesLib {
    getCompanies(company: any, per_page: any, page: any, rest?: any): Promise<object>;
}
