"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const auth_1 = require("../../../db/Mongo/Models/auth");
const customers_1 = require("../../../db/Mongo/Models/customers");
class GetAuthLib {
    GetAuthByCustomer(company, id_customer, rest) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            try {
                let auth_model = new auth_1.Auth();
                let customers_model = new customers_1.Customers();
                var Authentication = auth_model.authModel();
                var customer = customers_model.customersModel();
                let auth_query = yield Authentication.findOne({ "customer._id": id_customer }).exec();
                if (!auth_query) {
                    if (rest)
                        return { status: 404, message: 'Resource Not Located' };
                    throw new Error('Resource Not Located');
                }
                return auth_query;
            }
            catch (err) {
                console.log(err);
                return err;
            }
        });
    }
}
exports.GetAuthLib = GetAuthLib;
//# sourceMappingURL=get_auth_lib.js.map