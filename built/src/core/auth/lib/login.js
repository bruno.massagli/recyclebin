"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const bcryptjs_1 = require("bcryptjs");
const jsonwebtoken_1 = require("jsonwebtoken");
const auth_1 = require("../../../db/Mongo/Models/auth");
const companies_1 = require("../../../db/Mongo/Models/companies");
function generateToken(company, auth) {
    return jsonwebtoken_1.sign({
        companies: company.name,
        user_id: auth._id,
        username: auth.customer.name,
        email: auth.email
    }, 'secret', { expiresIn: '7 days' });
}
function login(company, email, password, rest) {
    return tslib_1.__awaiter(this, void 0, void 0, function* () {
        const auth = new auth_1.Auth();
        const company_model = new companies_1.Companies();
        const companyData = yield company_model.companiesModel().findOne({ _id: company }).exec();
        const user = yield auth.authModel().findOne({ email: email }).exec();
        if (!user) {
            if (rest)
                return 'Resource Not Located';
            throw Error(`User with email ${email} does not exist!`);
        }
        const valid = yield bcryptjs_1.compare(password, user.password);
        if (!valid) {
            if (rest)
                return 'incorrect password!';
            throw Error('incorrect password!');
        }
        return generateToken(companyData, user);
    });
}
exports.login = login;
//# sourceMappingURL=login.js.map