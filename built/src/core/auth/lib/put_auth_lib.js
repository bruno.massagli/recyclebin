"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const auth_1 = require("../../../db/Mongo/Models/auth");
const consolidate_object_1 = require("../../../helpers/consolidate_object");
const bcryptjs_1 = tslib_1.__importDefault(require("bcryptjs"));
const customers_1 = require("../../../db/Mongo/Models/customers");
const class_validator_1 = require("class-validator");
const auth_inputs_1 = require("../validator/auth_inputs");
class PutAuthLib {
    putAuth(company, body, rest) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            try {
                let Props = new consolidate_object_1.ConsolidateObject();
                let auth_class = new auth_inputs_1.AuthInputPut();
                auth_class = Props.setClassProps(auth_class, body);
                let auth_validate = yield class_validator_1.validate(auth_class);
                if (auth_validate.length) {
                    if (rest)
                        return auth_validate;
                    throw new Error(JSON.stringify(auth_validate));
                }
                var customer_model = new customers_1.Customers();
                let auth_model = new auth_1.Auth();
                var Authentication = auth_model.authModel();
                var new_customer = customer_model.customersModel();
                var verify_auth_exists = yield Authentication.findOne({ email: body.email }).exec();
                if (verify_auth_exists) {
                    if (rest)
                        return { status: 400, message: 'email already taken' };
                    throw new Error('email already taken');
                }
                var save_customer = yield new_customer.findOne({ _id: body.customer._id }).exec();
                if (!save_customer) {
                    if (rest)
                        return { status: 404, message: 'Resource Not Located' };
                    throw new Error('Resource Not Located');
                }
                body.customer = save_customer;
                save_customer.save();
                body.password = yield bcryptjs_1.default.hash(body.password, 12);
                let if_exists = yield Authentication.findOne({ _id: body._id }).exec();
                if (!if_exists) {
                    if (rest)
                        return { status: 404, message: 'Resource Not Located' };
                    throw new Error('Resource Not Located');
                }
                let save_auth = yield Authentication.findOneAndUpdate({ _id: body._id }, Object.assign({}, body), { new: true }).exec();
                return save_auth;
            }
            catch (err) {
                console.log(err);
                return err;
            }
        });
    }
}
exports.PutAuthLib = PutAuthLib;
//# sourceMappingURL=put_auth_lib.js.map