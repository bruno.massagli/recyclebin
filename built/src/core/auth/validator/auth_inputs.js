"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const class_validator_1 = require("class-validator");
class AuthInputPost {
}
tslib_1.__decorate([
    class_validator_1.IsEmail(),
    class_validator_1.IsDefined()
], AuthInputPost.prototype, "email", void 0);
tslib_1.__decorate([
    class_validator_1.MaxLength(20),
    class_validator_1.MinLength(6),
    class_validator_1.IsDefined()
], AuthInputPost.prototype, "password", void 0);
tslib_1.__decorate([
    class_validator_1.IsEnum(['master', 'member']),
    class_validator_1.IsDefined()
], AuthInputPost.prototype, "access_level", void 0);
tslib_1.__decorate([
    class_validator_1.IsDefined()
], AuthInputPost.prototype, "customer", void 0);
exports.AuthInputPost = AuthInputPost;
class AuthInputPut {
}
tslib_1.__decorate([
    class_validator_1.IsDefined()
], AuthInputPut.prototype, "_id", void 0);
tslib_1.__decorate([
    class_validator_1.IsOptional(),
    class_validator_1.IsEmail()
], AuthInputPut.prototype, "email", void 0);
tslib_1.__decorate([
    class_validator_1.IsOptional(),
    class_validator_1.MaxLength(20),
    class_validator_1.MinLength(6)
], AuthInputPut.prototype, "password", void 0);
tslib_1.__decorate([
    class_validator_1.IsOptional(),
    class_validator_1.IsEnum(['master', 'member'])
], AuthInputPut.prototype, "access_level", void 0);
exports.AuthInputPut = AuthInputPut;
//# sourceMappingURL=auth_inputs.js.map