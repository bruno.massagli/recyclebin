import { CustomerInputPost, CustomerInputPut } from '../../orders/validator/customer_inputs';
import { DocumentsInputPost, DocumentsInputPut } from '../../orders/validator/documents_inputs';
import { AddressInputPost, AddressInputPut } from '../../orders/validator/address_inputs';
import { ImagesInputPost, ImagesInputPut } from '../../products/validator/images_inputs';
export declare class CompanyInputPost {
    name: String;
    email: String;
    api_key: String;
    document: DocumentsInputPost;
    phones: String[];
    address: AddressInputPost;
    avatar: ImagesInputPost;
    customers: CustomerInputPost[];
}
export declare class CompanyInputPut {
    _id: String;
    name: String;
    email: String;
    api_key: String;
    document: DocumentsInputPut;
    phones: String[];
    address: AddressInputPut;
    avatar: ImagesInputPut;
    customers: CustomerInputPut[];
}
