import { CustomerInputPost, CustomerInputPut } from '../../orders/validator/customer_inputs';
export declare class AuthInputPost {
    email: String;
    password: String;
    access_level: String;
    customer: CustomerInputPost;
}
export declare class AuthInputPut {
    _id: String;
    email: String;
    password: String;
    access_level: String;
    customer: CustomerInputPut;
}
