"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const class_validator_1 = require("class-validator");
class CompanyInputPost {
}
tslib_1.__decorate([
    class_validator_1.MaxLength(60),
    class_validator_1.IsDefined()
], CompanyInputPost.prototype, "name", void 0);
tslib_1.__decorate([
    class_validator_1.IsEmail(),
    class_validator_1.IsDefined()
], CompanyInputPost.prototype, "email", void 0);
tslib_1.__decorate([
    class_validator_1.IsDefined()
], CompanyInputPost.prototype, "api_key", void 0);
tslib_1.__decorate([
    class_validator_1.IsDefined()
], CompanyInputPost.prototype, "document", void 0);
tslib_1.__decorate([
    class_validator_1.IsPhoneNumber('BR'),
    class_validator_1.IsDefined()
], CompanyInputPost.prototype, "phones", void 0);
tslib_1.__decorate([
    class_validator_1.IsDefined()
], CompanyInputPost.prototype, "address", void 0);
tslib_1.__decorate([
    class_validator_1.IsDefined()
], CompanyInputPost.prototype, "avatar", void 0);
tslib_1.__decorate([
    class_validator_1.IsDefined()
], CompanyInputPost.prototype, "customers", void 0);
exports.CompanyInputPost = CompanyInputPost;
class CompanyInputPut {
}
tslib_1.__decorate([
    class_validator_1.IsDefined()
], CompanyInputPut.prototype, "_id", void 0);
tslib_1.__decorate([
    class_validator_1.IsOptional(),
    class_validator_1.MaxLength(60)
], CompanyInputPut.prototype, "name", void 0);
tslib_1.__decorate([
    class_validator_1.IsOptional(),
    class_validator_1.IsEmail()
], CompanyInputPut.prototype, "email", void 0);
tslib_1.__decorate([
    class_validator_1.IsOptional(),
    class_validator_1.IsPhoneNumber('BR')
], CompanyInputPut.prototype, "phones", void 0);
exports.CompanyInputPut = CompanyInputPut;
//# sourceMappingURL=company_inputs.js.map