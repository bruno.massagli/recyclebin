import { DocumentsInputPost, DocumentsInputPut } from './documents_inputs';
import { AddressInputPost, AddressInputPut } from './address_inputs';
export declare class CustomerInputPost {
    name: string;
    email: string;
    document: DocumentsInputPost;
    phones: string[];
    address: AddressInputPost;
}
export declare class CustomerInputPut {
    _id: string;
    name: string;
    email: string;
    document: DocumentsInputPut;
    phones: string[];
    address: AddressInputPut;
}
