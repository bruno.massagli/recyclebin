"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const class_validator_1 = require("class-validator");
class AddressInputPost {
}
tslib_1.__decorate([
    class_validator_1.MaxLength(100),
    class_validator_1.IsDefined()
], AddressInputPost.prototype, "street", void 0);
tslib_1.__decorate([
    class_validator_1.IsDefined()
], AddressInputPost.prototype, "number", void 0);
tslib_1.__decorate([
    class_validator_1.MaxLength(30),
    class_validator_1.IsDefined()
], AddressInputPost.prototype, "detail", void 0);
tslib_1.__decorate([
    class_validator_1.MaxLength(30),
    class_validator_1.IsDefined()
], AddressInputPost.prototype, "neighborhood", void 0);
tslib_1.__decorate([
    class_validator_1.MaxLength(30),
    class_validator_1.IsDefined()
], AddressInputPost.prototype, "city", void 0);
tslib_1.__decorate([
    class_validator_1.MaxLength(20),
    class_validator_1.IsDefined()
], AddressInputPost.prototype, "region", void 0);
tslib_1.__decorate([
    class_validator_1.MaxLength(30),
    class_validator_1.IsDefined()
], AddressInputPost.prototype, "country", void 0);
tslib_1.__decorate([
    class_validator_1.MaxLength(10),
    class_validator_1.IsDefined()
], AddressInputPost.prototype, "postcode", void 0);
exports.AddressInputPost = AddressInputPost;
class AddressInputPut {
}
tslib_1.__decorate([
    class_validator_1.IsOptional(),
    class_validator_1.MaxLength(100)
], AddressInputPut.prototype, "street", void 0);
tslib_1.__decorate([
    class_validator_1.IsOptional(),
    class_validator_1.MaxLength(30)
], AddressInputPut.prototype, "detail", void 0);
tslib_1.__decorate([
    class_validator_1.IsOptional(),
    class_validator_1.MaxLength(30)
], AddressInputPut.prototype, "neighborhood", void 0);
tslib_1.__decorate([
    class_validator_1.IsOptional(),
    class_validator_1.MaxLength(30)
], AddressInputPut.prototype, "city", void 0);
tslib_1.__decorate([
    class_validator_1.IsOptional(),
    class_validator_1.MaxLength(20)
], AddressInputPut.prototype, "region", void 0);
tslib_1.__decorate([
    class_validator_1.IsOptional(),
    class_validator_1.MaxLength(30)
], AddressInputPut.prototype, "country", void 0);
tslib_1.__decorate([
    class_validator_1.IsOptional(),
    class_validator_1.MaxLength(10)
], AddressInputPut.prototype, "postcode", void 0);
exports.AddressInputPut = AddressInputPut;
//# sourceMappingURL=address_inputs.js.map