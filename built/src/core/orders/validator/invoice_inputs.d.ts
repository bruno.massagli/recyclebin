import { ItemsInputPost, ItemsInputPut } from './item_inputs';
export declare class InvoicesInputPost {
    key: string;
    issue_date: Date;
    items: ItemsInputPost[];
}
export declare class InvoicesInputPut {
    _id: string;
    key: string;
    issue_date: Date;
    items: ItemsInputPut[];
}
