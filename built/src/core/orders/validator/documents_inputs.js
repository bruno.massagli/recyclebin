"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const class_validator_1 = require("class-validator");
class DocumentsInputPost {
}
tslib_1.__decorate([
    class_validator_1.MaxLength(20),
    class_validator_1.IsDefined()
], DocumentsInputPost.prototype, "number", void 0);
tslib_1.__decorate([
    class_validator_1.IsEnum(['RG', 'CPF', 'CNPJ']),
    class_validator_1.IsDefined()
], DocumentsInputPost.prototype, "type", void 0);
exports.DocumentsInputPost = DocumentsInputPost;
class DocumentsInputPut {
}
tslib_1.__decorate([
    class_validator_1.IsOptional(),
    class_validator_1.MaxLength(20)
], DocumentsInputPut.prototype, "number", void 0);
tslib_1.__decorate([
    class_validator_1.IsOptional(),
    class_validator_1.IsEnum(['RG', 'CPF', 'CNPJ'])
], DocumentsInputPut.prototype, "type", void 0);
exports.DocumentsInputPut = DocumentsInputPut;
//# sourceMappingURL=documents_inputs.js.map