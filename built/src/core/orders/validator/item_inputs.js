"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const class_validator_1 = require("class-validator");
class ItemsInputPost {
}
tslib_1.__decorate([
    class_validator_1.IsDefined()
], ItemsInputPost.prototype, "sku", void 0);
tslib_1.__decorate([
    class_validator_1.MaxLength(60),
    class_validator_1.IsDefined()
], ItemsInputPost.prototype, "name", void 0);
tslib_1.__decorate([
    class_validator_1.IsDefined()
], ItemsInputPost.prototype, "quantity", void 0);
tslib_1.__decorate([
    class_validator_1.IsNumber(),
    class_validator_1.IsDefined()
], ItemsInputPost.prototype, "original_price", void 0);
tslib_1.__decorate([
    class_validator_1.IsNumber(),
    class_validator_1.IsDefined()
], ItemsInputPost.prototype, "special_price", void 0);
exports.ItemsInputPost = ItemsInputPost;
class ItemsInputPut {
}
tslib_1.__decorate([
    class_validator_1.IsDefined()
], ItemsInputPut.prototype, "_id", void 0);
tslib_1.__decorate([
    class_validator_1.IsOptional(),
    class_validator_1.MaxLength(60)
], ItemsInputPut.prototype, "name", void 0);
tslib_1.__decorate([
    class_validator_1.IsOptional(),
    class_validator_1.IsNumber()
], ItemsInputPut.prototype, "original_price", void 0);
tslib_1.__decorate([
    class_validator_1.IsOptional(),
    class_validator_1.IsNumber()
], ItemsInputPut.prototype, "special_price", void 0);
exports.ItemsInputPut = ItemsInputPut;
//# sourceMappingURL=item_inputs.js.map