"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const class_validator_1 = require("class-validator");
class ShippingsInputPost {
}
tslib_1.__decorate([
    class_validator_1.IsDefined()
], ShippingsInputPost.prototype, "carrier", void 0);
tslib_1.__decorate([
    class_validator_1.IsDefined()
], ShippingsInputPost.prototype, "shipping_method", void 0);
tslib_1.__decorate([
    class_validator_1.IsISO8601(),
    class_validator_1.IsDefined()
], ShippingsInputPost.prototype, "shipping_date", void 0);
tslib_1.__decorate([
    class_validator_1.IsDefined()
], ShippingsInputPost.prototype, "shipping_code", void 0);
tslib_1.__decorate([
    class_validator_1.IsNumber(),
    class_validator_1.IsDefined()
], ShippingsInputPost.prototype, "shipping_cost", void 0);
tslib_1.__decorate([
    class_validator_1.IsISO8601(),
    class_validator_1.IsDefined()
], ShippingsInputPost.prototype, "shipping_estimated_delivery", void 0);
tslib_1.__decorate([
    class_validator_1.IsISO8601(),
    class_validator_1.IsDefined()
], ShippingsInputPost.prototype, "delivery_date", void 0);
tslib_1.__decorate([
    class_validator_1.IsDefined()
], ShippingsInputPost.prototype, "shipping_address", void 0);
tslib_1.__decorate([
    class_validator_1.IsDefined()
], ShippingsInputPost.prototype, "items", void 0);
tslib_1.__decorate([
    class_validator_1.IsDefined()
], ShippingsInputPost.prototype, "tracks", void 0);
tslib_1.__decorate([
    class_validator_1.IsDefined()
], ShippingsInputPost.prototype, "invoices", void 0);
exports.ShippingsInputPost = ShippingsInputPost;
class ShippingsInputPut {
}
tslib_1.__decorate([
    class_validator_1.IsDefined()
], ShippingsInputPut.prototype, "_id", void 0);
tslib_1.__decorate([
    class_validator_1.IsOptional(),
    class_validator_1.IsISO8601()
], ShippingsInputPut.prototype, "shipping_date", void 0);
tslib_1.__decorate([
    class_validator_1.IsOptional(),
    class_validator_1.IsNumber()
], ShippingsInputPut.prototype, "shipping_cost", void 0);
tslib_1.__decorate([
    class_validator_1.IsOptional(),
    class_validator_1.IsISO8601()
], ShippingsInputPut.prototype, "shipping_estimated_delivery", void 0);
tslib_1.__decorate([
    class_validator_1.IsOptional(),
    class_validator_1.IsISO8601()
], ShippingsInputPut.prototype, "delivery_date", void 0);
exports.ShippingsInputPut = ShippingsInputPut;
//# sourceMappingURL=shipping_inputs.js.map