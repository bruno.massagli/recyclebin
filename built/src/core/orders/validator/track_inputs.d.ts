import { ItemsInputPost, ItemsInputPut } from './item_inputs';
export declare class TracksInputPost {
    tracking_link: string;
    tracking_number: string;
    items: ItemsInputPost[];
}
export declare class TracksInputPut {
    _id: String;
    tracking_link: string;
    tracking_number: string;
    items: ItemsInputPut[];
}
