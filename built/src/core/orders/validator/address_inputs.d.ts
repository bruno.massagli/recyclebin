export declare class AddressInputPost {
    street: string;
    number: number;
    detail: string;
    neighborhood: string;
    city: string;
    region: string;
    country: string;
    postcode: string;
}
export declare class AddressInputPut {
    street: string;
    number: number;
    detail: string;
    neighborhood: string;
    city: string;
    region: string;
    country: string;
    postcode: string;
}
