"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const class_validator_1 = require("class-validator");
class CustomerInputPost {
}
tslib_1.__decorate([
    class_validator_1.MaxLength(60),
    class_validator_1.IsDefined()
], CustomerInputPost.prototype, "name", void 0);
tslib_1.__decorate([
    class_validator_1.IsEmail(),
    class_validator_1.IsDefined()
], CustomerInputPost.prototype, "email", void 0);
tslib_1.__decorate([
    class_validator_1.IsDefined()
], CustomerInputPost.prototype, "document", void 0);
tslib_1.__decorate([
    class_validator_1.IsPhoneNumber('BR'),
    class_validator_1.IsDefined()
], CustomerInputPost.prototype, "phones", void 0);
tslib_1.__decorate([
    class_validator_1.IsDefined()
], CustomerInputPost.prototype, "address", void 0);
exports.CustomerInputPost = CustomerInputPost;
class CustomerInputPut {
}
tslib_1.__decorate([
    class_validator_1.IsDefined()
], CustomerInputPut.prototype, "_id", void 0);
tslib_1.__decorate([
    class_validator_1.IsOptional(),
    class_validator_1.MaxLength(60)
], CustomerInputPut.prototype, "name", void 0);
tslib_1.__decorate([
    class_validator_1.IsOptional(),
    class_validator_1.IsEmail()
], CustomerInputPut.prototype, "email", void 0);
tslib_1.__decorate([
    class_validator_1.IsOptional(),
    class_validator_1.IsPhoneNumber('BR')
], CustomerInputPut.prototype, "phones", void 0);
exports.CustomerInputPut = CustomerInputPut;
//# sourceMappingURL=customer_inputs.js.map