"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const class_validator_1 = require("class-validator");
class OrdersInputPost {
}
tslib_1.__decorate([
    class_validator_1.IsDefined()
], OrdersInputPost.prototype, "company", void 0);
tslib_1.__decorate([
    class_validator_1.IsDefined()
], OrdersInputPost.prototype, "order_number", void 0);
tslib_1.__decorate([
    class_validator_1.IsISO8601(),
    class_validator_1.IsDefined()
], OrdersInputPost.prototype, "order_date", void 0);
tslib_1.__decorate([
    class_validator_1.IsNumber(),
    class_validator_1.IsDefined()
], OrdersInputPost.prototype, "total_gross", void 0);
tslib_1.__decorate([
    class_validator_1.IsEnum(['processed', 'invoiced', 'sent', 'delivered', 'cancelled']),
    class_validator_1.IsDefined()
], OrdersInputPost.prototype, "status", void 0);
tslib_1.__decorate([
    class_validator_1.IsDefined()
], OrdersInputPost.prototype, "shipping", void 0);
tslib_1.__decorate([
    class_validator_1.IsDefined()
], OrdersInputPost.prototype, "customer", void 0);
exports.OrdersInputPost = OrdersInputPost;
class OrdersInputPut {
}
tslib_1.__decorate([
    class_validator_1.IsDefined()
], OrdersInputPut.prototype, "_id", void 0);
tslib_1.__decorate([
    class_validator_1.IsOptional(),
    class_validator_1.IsISO8601()
], OrdersInputPut.prototype, "order_date", void 0);
tslib_1.__decorate([
    class_validator_1.IsOptional(),
    class_validator_1.IsNumber()
], OrdersInputPut.prototype, "total_gross", void 0);
tslib_1.__decorate([
    class_validator_1.IsOptional(),
    class_validator_1.IsEnum(['processed', 'invoiced', 'sent', 'delivered', 'cancelled'])
], OrdersInputPut.prototype, "status", void 0);
exports.OrdersInputPut = OrdersInputPut;
class OrdersStatusInputPut {
}
tslib_1.__decorate([
    class_validator_1.IsDefined()
], OrdersStatusInputPut.prototype, "order_number", void 0);
tslib_1.__decorate([
    class_validator_1.IsOptional(),
    class_validator_1.IsISO8601()
], OrdersStatusInputPut.prototype, "invoiced_date", void 0);
tslib_1.__decorate([
    class_validator_1.IsOptional(),
    class_validator_1.IsISO8601()
], OrdersStatusInputPut.prototype, "date_delivery", void 0);
tslib_1.__decorate([
    class_validator_1.IsOptional(),
    class_validator_1.IsISO8601()
], OrdersStatusInputPut.prototype, "estimated_delivery", void 0);
tslib_1.__decorate([
    class_validator_1.IsOptional(),
    class_validator_1.IsISO8601()
], OrdersStatusInputPut.prototype, "delivered_date", void 0);
exports.OrdersStatusInputPut = OrdersStatusInputPut;
//# sourceMappingURL=order_inputs.js.map