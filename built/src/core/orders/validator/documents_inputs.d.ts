export declare class DocumentsInputPost {
    number: string;
    type: string;
}
export declare class DocumentsInputPut {
    number: string;
    type: string;
}
