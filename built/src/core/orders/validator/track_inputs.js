"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const class_validator_1 = require("class-validator");
class TracksInputPost {
}
tslib_1.__decorate([
    class_validator_1.IsDefined()
], TracksInputPost.prototype, "tracking_link", void 0);
tslib_1.__decorate([
    class_validator_1.IsDefined()
], TracksInputPost.prototype, "tracking_number", void 0);
tslib_1.__decorate([
    class_validator_1.IsDefined()
], TracksInputPost.prototype, "items", void 0);
exports.TracksInputPost = TracksInputPost;
class TracksInputPut {
}
tslib_1.__decorate([
    class_validator_1.IsDefined()
], TracksInputPut.prototype, "_id", void 0);
exports.TracksInputPut = TracksInputPut;
//# sourceMappingURL=track_inputs.js.map