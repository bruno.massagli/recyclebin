export declare class ItemsInputPost {
    sku: string;
    name: string;
    quantity: number;
    original_price: number;
    special_price: number;
}
export declare class ItemsInputPut {
    _id: string;
    sku: string;
    name: string;
    quantity: number;
    original_price: number;
    special_price: number;
}
