import { ShippingsInputPost, ShippingsInputPut } from './shipping_inputs';
import { CustomerInputPost, CustomerInputPut } from './customer_inputs';
import { ItemsInputPut } from './item_inputs';
export declare class OrdersInputPost {
    company: String;
    order_number: String;
    order_date: Date;
    total_gross: Number;
    status: String;
    shipping: ShippingsInputPost;
    customer: CustomerInputPost;
}
export declare class OrdersInputPut {
    _id: String;
    company: String;
    order_number: String;
    order_date: Date;
    total_gross: Number;
    status: String;
    shipping: ShippingsInputPut;
    customer: CustomerInputPut;
}
export declare class OrdersStatusInputPut {
    order_number: String;
    reason_cancelled: String;
    key: String;
    number: String;
    invoiced_date: Date;
    carrier: String;
    method: String;
    tracking_number: String;
    tracking_link: String;
    date_delivery: Date;
    estimated_delivery: Date;
    delivered_date: Date;
    items: ItemsInputPut[];
}
