import { AddressInputPost, AddressInputPut } from './address_inputs';
import { ItemsInputPost, ItemsInputPut } from './item_inputs';
import { TracksInputPost, TracksInputPut } from './track_inputs';
import { InvoicesInputPost, InvoicesInputPut } from './invoice_inputs';
export declare class ShippingsInputPost {
    carrier: string;
    shipping_method: string;
    shipping_date: Date;
    shipping_code: string;
    shipping_cost: number;
    shipping_estimated_delivery: Date;
    delivery_date: Date;
    shipping_address: AddressInputPost;
    items: ItemsInputPost[];
    tracks: TracksInputPost[];
    invoices: InvoicesInputPost[];
}
export declare class ShippingsInputPut {
    _id: String;
    carrier: string;
    shipping_method: string;
    shipping_date: Date;
    shipping_code: string;
    shipping_cost: number;
    shipping_estimated_delivery: Date;
    delivery_date: Date;
    shipping_address: AddressInputPut;
    items: ItemsInputPut[];
    tracks: TracksInputPut[];
    invoices: InvoicesInputPut[];
}
