export interface Address {
    street: string;
    number: number;
    detail: string;
    neighborhood: string;
    city: string;
    region: string;
    country: string;
    postcode: string;
}
