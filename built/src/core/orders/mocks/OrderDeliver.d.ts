export interface OrderDeliver {
    order_number: string;
    delivered_date: Date;
}
