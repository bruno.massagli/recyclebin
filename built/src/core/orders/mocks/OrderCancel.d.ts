export interface OrderCancel {
    order_number: string;
    reason_cancelled: string;
}
