import { Shipping } from './Shipping';
import { Customer } from './Customer';
export interface Order {
    _id: String;
    company: String;
    order_number: String;
    order_date: Date;
    total_gross: Number;
    status: String;
    shipping: Shipping;
    customer: Customer;
}
