export interface OrderInvoice {
    order_number: string;
    key: string;
    number: string;
    invoiced_date: Date;
}
