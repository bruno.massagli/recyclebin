"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const orders_1 = require("../../../db/Mongo/Models/orders");
const shippings_1 = require("../../../db/Mongo/Models//shippings");
const items_1 = require("../../../db/Mongo/Models//items");
const mongoose_1 = tslib_1.__importDefault(require("mongoose"));
const consolidate_object_1 = require("../../../helpers/consolidate_object");
const order_inputs_1 = require("../validator/order_inputs");
const class_validator_1 = require("class-validator");
class PostOrderLib {
    PostOrder(company, body, rest) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            try {
                let Props = new consolidate_object_1.ConsolidateObject();
                let order_class = new order_inputs_1.OrdersInputPost();
                order_class = Props.setClassProps(order_class, body);
                let order_validate = yield class_validator_1.validate(order_class);
                if (order_validate.length) {
                    if (rest)
                        return order_validate;
                    throw new Error(JSON.stringify(order_validate));
                }
                let orders_model = new orders_1.Orders();
                let shipping_model = new shippings_1.Shippings();
                let items_model = new items_1.Items();
                var new_order = orders_model.ordersModel();
                var new_shipping = shipping_model.shippingsModel();
                var new_item = items_model.itemsModel();
                var verify_order_exists = yield new_order.findOne({ company: company, order_number: body.order_number }).exec();
                if (verify_order_exists) {
                    if (rest)
                        return { status: 409, message: 'Order Already Exists' };
                    throw new Error('Order Already Exists');
                }
                if (company !== body.company) {
                    if (rest)
                        return { status: 400, message: 'Company Doesnt Match' };
                    throw new Error('Company Doesnt Match');
                }
                console.log(body.shipping);
                var final_items = body.shipping.items.map((elem, idx) => tslib_1.__awaiter(this, void 0, void 0, function* () {
                    var save_items = yield new_item.findOne({ _id: elem._id }).exec();
                    if (!save_items) {
                        save_items = new new_item(Object.assign({ _id: new mongoose_1.default.Types.ObjectId() }, elem));
                        yield save_items.save();
                    }
                    return save_items;
                }));
                body.shipping.items = yield Promise.all(final_items);
                var save_shipping = new new_shipping(Object.assign({ _id: new mongoose_1.default.Types.ObjectId() }, body.shipping));
                yield save_shipping.save();
                body.shipping = save_shipping;
                var save_orders = new new_order(Object.assign({ _id: new mongoose_1.default.Types.ObjectId() }, body));
                yield save_orders.save();
                return save_orders;
            }
            catch (err) {
                console.log(err);
                return err;
            }
        });
    }
}
exports.PostOrderLib = PostOrderLib;
//# sourceMappingURL=post_order_lib.js.map