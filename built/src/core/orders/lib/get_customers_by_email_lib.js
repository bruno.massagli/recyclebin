"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const customers_1 = require("../../../db/Mongo/Models/customers");
class GetCustomerLib {
    getCustomerByEmail(email, rest) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            try {
                let customers_model = new customers_1.Customers();
                var Customer = customers_model.customersModel();
                let customer_query = yield Customer.findOne({ email: email }).exec();
                if (!customer_query) {
                    if (rest)
                        return { status: 404, message: 'Resource Not Located' };
                    throw new Error('Resource Not Located');
                }
                return customer_query;
            }
            catch (err) {
                console.log(err);
                return err;
            }
        });
    }
}
exports.GetCustomerLib = GetCustomerLib;
//# sourceMappingURL=get_customers_by_email_lib.js.map