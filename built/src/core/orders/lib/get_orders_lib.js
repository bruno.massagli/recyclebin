"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const orders_1 = require("../../../db/Mongo/Models/orders");
class GetOrdersLib {
    getOrders(company, status, per_page, page, date_start, date_end, rest) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            try {
                let orders_model = new orders_1.Orders();
                var Order = orders_model.ordersModel();
                console.log(date_start, date_end);
                let orders_query = yield Order.find({ company: company, createdAt: { "$gte": new Date(date_start).toISOString(), "$lt": new Date(date_end).toISOString() }, "status": status })
                    .skip(per_page * page).limit(per_page).exec();
                return orders_query;
            }
            catch (err) {
                console.log(err);
                return err;
            }
        });
    }
}
exports.GetOrdersLib = GetOrdersLib;
//# sourceMappingURL=get_orders_lib.js.map