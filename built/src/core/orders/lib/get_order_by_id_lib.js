"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const orders_1 = require("../../../db/Mongo/Models/orders");
class GetOrderByIdLib {
    getOrdersById(company, id, rest) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            try {
                let orders_model = new orders_1.Orders();
                var Order = orders_model.ordersModel();
                let order_query = yield Order.findOne({ company: company, order_number: id }).exec();
                if (!order_query) {
                    if (rest)
                        return { status: 404, message: 'Resource Not Located' };
                    throw new Error('Resource Not Located');
                }
                return order_query;
            }
            catch (err) {
                console.log(err);
                return err;
            }
        });
    }
}
exports.GetOrderByIdLib = GetOrderByIdLib;
//# sourceMappingURL=get_order_by_id_lib.js.map