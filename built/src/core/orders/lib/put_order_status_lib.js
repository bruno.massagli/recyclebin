"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const orders_1 = require("../../../db/Mongo/Models/orders");
const invoices_1 = require("../../../db/Mongo/Models/invoices");
const tracks_1 = require("../../../db/Mongo/Models/tracks");
const mongoose_1 = tslib_1.__importDefault(require("mongoose"));
const consolidate_object_1 = require("../../../helpers/consolidate_object");
const order_inputs_1 = require("../validator/order_inputs");
const class_validator_1 = require("class-validator");
class PutOrderStatusLib {
    putOrdersStatus(company, status, body, rest) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            try {
                let Props = new consolidate_object_1.ConsolidateObject();
                let order_class = new order_inputs_1.OrdersStatusInputPut();
                order_class = Props.setClassProps(order_class, body);
                let order_validate = yield class_validator_1.validate(order_class);
                if (order_validate.length) {
                    if (rest)
                        return order_validate;
                    throw new Error(JSON.stringify(order_validate));
                }
                let orders_model = new orders_1.Orders();
                let invoices_model = new invoices_1.Invoices();
                let tracks_model = new tracks_1.Tracks();
                var Order = orders_model.ordersModel();
                var Invoice = invoices_model.invoicesModel();
                var Track = tracks_model.tracksModel();
                let save_orders = yield Order.findOne({ company: company, order_number: body.order_number }).exec();
                if (!save_orders) {
                    if (rest)
                        return { status: 404, message: 'Resource Not Located' };
                    throw new Error('Resource Not Located');
                }
                switch (status) {
                    case 'processed':
                        save_orders.status = 'processed';
                        break;
                    case 'invoiced':
                        if (save_orders.status !== 'processed') {
                            break;
                        }
                        let inv_obj = body;
                        inv_obj.invoiced_date = new Date(inv_obj.invoiced_date).toISOString();
                        save_orders.status = 'invoiced';
                        var save_invoices = yield Invoice.findOne({ key: inv_obj.key }).exec();
                        if (!save_invoices) {
                            save_invoices = new Invoice(Object.assign({ _id: new mongoose_1.default.Types.ObjectId() }, inv_obj));
                            yield save_invoices.save();
                            save_orders.shipping.invoices.push(save_invoices);
                        }
                        break;
                    case 'sent':
                        if (save_orders.status !== 'invoiced') {
                            break;
                        }
                        let sent_obj = body;
                        sent_obj.date_delivery = new Date(sent_obj.date_delivery).toISOString();
                        sent_obj.estimated_delivery = new Date(sent_obj.estimated_delivery).toISOString();
                        save_orders.status = 'sent';
                        var save_tracks = yield Track.findOne({ tracking_number: sent_obj.tracking_number }).exec();
                        if (!save_tracks) {
                            save_tracks = new Track(Object.assign({ _id: new mongoose_1.default.Types.ObjectId() }, sent_obj));
                            yield save_tracks.save();
                            save_orders.shipping.tracks.push(save_tracks);
                        }
                        break;
                    case 'delivered':
                        if (save_orders.status !== 'sent') {
                            break;
                        }
                        save_orders.status = 'delivered';
                        save_orders.delivery_date = body.delivery_date;
                        break;
                    case 'cancelled':
                        if (save_orders.status == 'sent' || save_orders.status == 'delivered') {
                            break;
                        }
                        save_orders.status = 'cancelled';
                        save_orders.reason_cancelled = body.reason_cancelled;
                        break;
                }
                yield save_orders.save();
                return save_orders;
            }
            catch (err) {
                console.log(err);
                return err;
            }
        });
    }
}
exports.PutOrderStatusLib = PutOrderStatusLib;
//# sourceMappingURL=put_order_status_lib.js.map