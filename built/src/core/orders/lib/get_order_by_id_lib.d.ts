export declare class GetOrderByIdLib {
    getOrdersById(company: any, id: any, rest?: any): Promise<object>;
}
