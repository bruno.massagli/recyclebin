export declare class GetOrdersLib {
    getOrders(company: any, status: any, per_page: any, page: any, date_start: any, date_end: any, rest?: any): Promise<object>;
}
