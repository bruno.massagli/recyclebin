import { ShippingsData } from './shippings_type';
import { CustomerData } from './customers_type';
export declare class OrdersData {
    _id: String;
    company: String;
    order_number: String;
    order_date: Date;
    total_gross: Number;
    status: String;
    shipping: ShippingsData;
    customer: CustomerData;
}
export declare const OrdersType = "\n\ntype OrdersType {\n    _id : ID!\n    company: String\n    order_number: String\n    order_date: Date\n    total_gross: Float\n    status: String\n    shipping: ShippingsType\n    customer: CustomerType\n}\n";
export declare const OrdersUpdateInput = "\n\ninput OrdersUpdateInput {\n    _id : ID!\n    company: String\n    order_number: String\n    order_date: Date\n    total_gross: Float\n    status: String\n    shipping: ShippingsUpdateInput\n    customer: CustomerUpdateInput\n}\n";
export declare const OrdersInput = "\n\ninput OrdersInput {\n    _id : ID\n    company: String!\n    order_number: String!\n    order_date: Date!\n    total_gross: Float!\n    status: String!\n    shipping: ShippingsInput!\n    customer: CustomerInput!\n}\n";
export declare const OrderStatusInput = "\n\ninput OrderStatusInput {\n    order_number: String!\n    reason_cancelled: String\n    key: String\n    number: String\n    invoiced_date: Date\n    carrier: String\n    method: String\n    tracking_number: String\n    tracking_link: String\n    date_delivery: Date\n    estimated_delivery: Date\n    delivered_date: Date\n    items: [ItemsUpdateInput]\n\n}\n";
