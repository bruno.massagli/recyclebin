export declare class OrdersDeliverData {
    order_number: string;
    delivered_date: Date;
}
export declare const OrdersDeliverInput = "\n\ninput OrdersDeliverInput {\n    order_number: String!\n    delivered_date: Date!\n}\n";
