"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const type_graphql_1 = require("type-graphql");
let AddressData = class AddressData {
};
tslib_1.__decorate([
    type_graphql_1.Field(type => String)
], AddressData.prototype, "street", void 0);
tslib_1.__decorate([
    type_graphql_1.Field(type => Number)
], AddressData.prototype, "number", void 0);
tslib_1.__decorate([
    type_graphql_1.Field(type => String)
], AddressData.prototype, "detail", void 0);
tslib_1.__decorate([
    type_graphql_1.Field(type => String)
], AddressData.prototype, "neighborhood", void 0);
tslib_1.__decorate([
    type_graphql_1.Field(type => String)
], AddressData.prototype, "city", void 0);
tslib_1.__decorate([
    type_graphql_1.Field(type => String)
], AddressData.prototype, "region", void 0);
tslib_1.__decorate([
    type_graphql_1.Field(type => String)
], AddressData.prototype, "country", void 0);
tslib_1.__decorate([
    type_graphql_1.Field(type => String)
], AddressData.prototype, "postcode", void 0);
AddressData = tslib_1.__decorate([
    type_graphql_1.ObjectType()
], AddressData);
exports.AddressData = AddressData;
exports.AddressType = `
type AddressType {    
    street: String
    number: Int
    detail: String
    neighborhood: String
    city: String
    region: String
    country: String
    postcode: String
}
`;
exports.AddressUpdateInput = `
input AddressUpdateInput {    
    street: String
    number: Int
    detail: String
    neighborhood: String
    city: String
    region: String
    country: String
    postcode: String
}
`;
exports.AddressInput = `
input AddressInput {    
    street: String!
    number: Int!
    detail: String!
    neighborhood: String!
    city: String!
    region: String!
    country: String!
    postcode: String!
}
`;
//# sourceMappingURL=addresses_type.js.map