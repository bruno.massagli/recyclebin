export declare class DocumentsData {
    number: String;
    type: String;
}
export declare const DocumentsType = "\ntype DocumentsType {\n    number: String\n    type: String\n}\n";
export declare const DocumentsUpdateInput = "\ninput DocumentsUpdateInput {\n    number: String\n    type: String\n}\n";
export declare const DocumentsInput = "\ninput DocumentsInput {\n    number: String!\n    type: String!\n}\n";
