export declare class OrdersSentData {
    order_number: string;
    carrier: string;
    method: string;
    tracking_number: string;
    tracking_link: string;
    date_delivery: Date;
    estimated_delivery: Date;
}
export declare const OrdersSentInput = "\n\ninput OrdersSentInput {\n    order_number: String!\n    carrier: String!\n    method: String!\n    tracking_number: String!\n    tracking_link: String!\n    date_delivery: Date!\n    estimated_delivery: Date!\n    items: [ItemsUpdateInput]\n\n}\n";
