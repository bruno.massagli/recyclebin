"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const type_graphql_1 = require("type-graphql");
const items_type_1 = require("./items_type");
let InvoicesData = class InvoicesData {
};
tslib_1.__decorate([
    type_graphql_1.Field(type => String)
], InvoicesData.prototype, "_id", void 0);
tslib_1.__decorate([
    type_graphql_1.Field(type => String)
], InvoicesData.prototype, "key", void 0);
tslib_1.__decorate([
    type_graphql_1.Field(type => Date)
], InvoicesData.prototype, "issue_date", void 0);
tslib_1.__decorate([
    type_graphql_1.Field(type => [items_type_1.ItemsData])
], InvoicesData.prototype, "items", void 0);
InvoicesData = tslib_1.__decorate([
    type_graphql_1.ObjectType()
], InvoicesData);
exports.InvoicesData = InvoicesData;
exports.InvoicesType = `


type InvoicesType {
    _id : ID!
    key: String
    issue_date: Date
    items: [ItemsType]
}
`;
exports.InvoicesUpdateInput = `


input InvoicesUpdateInput {
    _id : ID!
    key: String
    issue_date: Date
    items: [ItemsUpdateInput]
}
`;
exports.InvoicesInput = `


input InvoicesInput {
    _id : ID
    key: String
    issue_date: Date
    items: [ItemsInput!]!
}
`;
//# sourceMappingURL=invoices_type.js.map