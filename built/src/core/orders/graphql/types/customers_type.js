"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const type_graphql_1 = require("type-graphql");
const documents_type_1 = require("./documents_type");
const addresses_type_1 = require("./addresses_type");
let CustomerData = class CustomerData {
};
tslib_1.__decorate([
    type_graphql_1.Field(type => String)
], CustomerData.prototype, "_id", void 0);
tslib_1.__decorate([
    type_graphql_1.Field(type => String)
], CustomerData.prototype, "name", void 0);
tslib_1.__decorate([
    type_graphql_1.Field(type => String)
], CustomerData.prototype, "email", void 0);
tslib_1.__decorate([
    type_graphql_1.Field(type => documents_type_1.DocumentsData)
], CustomerData.prototype, "document", void 0);
tslib_1.__decorate([
    type_graphql_1.Field(type => [String])
], CustomerData.prototype, "phones", void 0);
tslib_1.__decorate([
    type_graphql_1.Field(type => addresses_type_1.AddressData)
], CustomerData.prototype, "address", void 0);
CustomerData = tslib_1.__decorate([
    type_graphql_1.ObjectType()
], CustomerData);
exports.CustomerData = CustomerData;
exports.CustomerType = `

type CustomerType {
    _id : ID!
    name: String
    email: String
    document: DocumentsType
    phones: [String]
    address: AddressType
}
`;
exports.CustomerUpdateInput = `

input CustomerUpdateInput {
    _id : ID!
    name: String
    email: String
    document: DocumentsUpdateInput
    phones: [String]
    address: AddressUpdateInput
}
`;
exports.CustomerInput = `

input CustomerInput {
    _id : ID
    name: String!
    email: String!
    document: DocumentsInput!
    phones: [String!]!
    address: AddressInput!
}
`;
//# sourceMappingURL=customers_type.js.map