export declare class ItemsData {
    _id: String;
    sku: String;
    name: String;
    quantity: number;
    original_price: number;
    special_price: number;
}
export declare const ItemsType = "\ntype ItemsType {\n    _id : ID!\n    sku: String\n    name: String\n    quantity: Float\n    original_price: Float\n    special_price: Float\n}\n";
export declare const ItemsUpdateInput = "\ninput ItemsUpdateInput {\n    _id : ID!\n    sku: String\n    name: String\n    quantity: Float\n    original_price: Float\n    special_price: Float\n}\n";
export declare const ItemsInput = "\ninput ItemsInput {\n    _id : ID\n    sku: String!\n    name: String!\n    quantity: Float!\n    original_price: Float!\n    special_price: Float!\n}\n";
