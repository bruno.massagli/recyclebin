export declare class OrdersCancelData {
    order_number: string;
    reason_cancelled: string;
}
export declare const OrdersCancelInput = "\n\ninput OrdersCancelInput {\n    order_number: String!\n    reason_cancelled: String!\n}\n";
