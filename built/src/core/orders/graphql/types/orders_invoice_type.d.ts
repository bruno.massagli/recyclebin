export declare class OrdersInvoiceData {
    order_number: string;
    key: string;
    number: string;
    invoiced_date: Date;
}
export declare const OrdersInvoiceInput = "\n\ninput OrdersInvoiceInput {\n    order_number: String!\n    key: String!\n    number: String!\n    invoiced_date: Date!\n    items: [ItemsUpdateInput]\n\n}\n";
