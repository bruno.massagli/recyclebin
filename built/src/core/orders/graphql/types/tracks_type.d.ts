import { ItemsData } from './items_type';
export declare class TracksData {
    _id: String;
    tracking_link: String;
    tracking_number: String;
    items: ItemsData[];
}
export declare const TracksType = "\n\ntype TracksType {\n    _id : ID!\n    tracking_link: String\n    tracking_number: String\n    items: [ItemsType]\n}\n";
export declare const TracksUpdateInput = "\n\ninput TracksUpdateInput {\n    _id : ID!\n    tracking_link: String\n    tracking_number: String\n    items: [ItemsUpdateInput]\n}\n";
export declare const TracksInput = "\n\ninput TracksInput {\n    _id : ID\n    tracking_link: String\n    tracking_number: String\n    items: [ItemsInput!]!\n}\n";
