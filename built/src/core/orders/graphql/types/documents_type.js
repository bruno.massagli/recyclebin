"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const type_graphql_1 = require("type-graphql");
let DocumentsData = class DocumentsData {
};
tslib_1.__decorate([
    type_graphql_1.Field(type => String)
], DocumentsData.prototype, "number", void 0);
tslib_1.__decorate([
    type_graphql_1.Field(type => String)
], DocumentsData.prototype, "type", void 0);
DocumentsData = tslib_1.__decorate([
    type_graphql_1.ObjectType()
], DocumentsData);
exports.DocumentsData = DocumentsData;
exports.DocumentsType = `
type DocumentsType {
    number: String
    type: String
}
`;
exports.DocumentsUpdateInput = `
input DocumentsUpdateInput {
    number: String
    type: String
}
`;
exports.DocumentsInput = `
input DocumentsInput {
    number: String!
    type: String!
}
`;
//# sourceMappingURL=documents_type.js.map