"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const type_graphql_1 = require("type-graphql");
let OrdersInvoiceData = class OrdersInvoiceData {
};
tslib_1.__decorate([
    type_graphql_1.Field(type => String)
], OrdersInvoiceData.prototype, "order_number", void 0);
tslib_1.__decorate([
    type_graphql_1.Field(type => String)
], OrdersInvoiceData.prototype, "key", void 0);
tslib_1.__decorate([
    type_graphql_1.Field(type => String)
], OrdersInvoiceData.prototype, "number", void 0);
tslib_1.__decorate([
    type_graphql_1.Field(type => Date)
], OrdersInvoiceData.prototype, "invoiced_date", void 0);
OrdersInvoiceData = tslib_1.__decorate([
    type_graphql_1.InputType()
], OrdersInvoiceData);
exports.OrdersInvoiceData = OrdersInvoiceData;
exports.OrdersInvoiceInput = `

input OrdersInvoiceInput {
    order_number: String!
    key: String!
    number: String!
    invoiced_date: Date!
    items: [ItemsUpdateInput]

}
`;
//# sourceMappingURL=orders_invoice_type.js.map