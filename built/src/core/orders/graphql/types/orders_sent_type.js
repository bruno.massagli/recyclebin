"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const type_graphql_1 = require("type-graphql");
let OrdersSentData = class OrdersSentData {
};
tslib_1.__decorate([
    type_graphql_1.Field(type => String)
], OrdersSentData.prototype, "order_number", void 0);
tslib_1.__decorate([
    type_graphql_1.Field(type => String)
], OrdersSentData.prototype, "carrier", void 0);
tslib_1.__decorate([
    type_graphql_1.Field(type => String)
], OrdersSentData.prototype, "method", void 0);
tslib_1.__decorate([
    type_graphql_1.Field(type => String)
], OrdersSentData.prototype, "tracking_number", void 0);
tslib_1.__decorate([
    type_graphql_1.Field(type => String)
], OrdersSentData.prototype, "tracking_link", void 0);
tslib_1.__decorate([
    type_graphql_1.Field(type => Date)
], OrdersSentData.prototype, "date_delivery", void 0);
tslib_1.__decorate([
    type_graphql_1.Field(type => Date)
], OrdersSentData.prototype, "estimated_delivery", void 0);
OrdersSentData = tslib_1.__decorate([
    type_graphql_1.InputType()
], OrdersSentData);
exports.OrdersSentData = OrdersSentData;
exports.OrdersSentInput = `

input OrdersSentInput {
    order_number: String!
    carrier: String!
    method: String!
    tracking_number: String!
    tracking_link: String!
    date_delivery: Date!
    estimated_delivery: Date!
    items: [ItemsUpdateInput]

}
`;
//# sourceMappingURL=orders_sent_type.js.map