"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const type_graphql_1 = require("type-graphql");
let OrdersCancelData = class OrdersCancelData {
};
tslib_1.__decorate([
    type_graphql_1.Field(type => String)
], OrdersCancelData.prototype, "order_number", void 0);
tslib_1.__decorate([
    type_graphql_1.Field(type => String)
], OrdersCancelData.prototype, "reason_cancelled", void 0);
OrdersCancelData = tslib_1.__decorate([
    type_graphql_1.InputType()
], OrdersCancelData);
exports.OrdersCancelData = OrdersCancelData;
exports.OrdersCancelInput = `

input OrdersCancelInput {
    order_number: String!
    reason_cancelled: String!
}
`;
//# sourceMappingURL=orders_cancel_type.js.map