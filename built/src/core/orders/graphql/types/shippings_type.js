"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const type_graphql_1 = require("type-graphql");
const addresses_type_1 = require("./addresses_type");
const items_type_1 = require("./items_type");
const tracks_type_1 = require("./tracks_type");
const invoices_type_1 = require("./invoices_type");
let ShippingsData = class ShippingsData {
};
tslib_1.__decorate([
    type_graphql_1.Field(type => String)
], ShippingsData.prototype, "_id", void 0);
tslib_1.__decorate([
    type_graphql_1.Field(type => String)
], ShippingsData.prototype, "carrier", void 0);
tslib_1.__decorate([
    type_graphql_1.Field(type => String)
], ShippingsData.prototype, "shipping_method", void 0);
tslib_1.__decorate([
    type_graphql_1.Field(type => Date)
], ShippingsData.prototype, "shipping_date", void 0);
tslib_1.__decorate([
    type_graphql_1.Field(type => String)
], ShippingsData.prototype, "shipping_code", void 0);
tslib_1.__decorate([
    type_graphql_1.Field(type => Number)
], ShippingsData.prototype, "shipping_cost", void 0);
tslib_1.__decorate([
    type_graphql_1.Field(type => Date)
], ShippingsData.prototype, "shipping_estimated_delivery", void 0);
tslib_1.__decorate([
    type_graphql_1.Field(type => Date)
], ShippingsData.prototype, "delivery_date", void 0);
tslib_1.__decorate([
    type_graphql_1.Field(type => addresses_type_1.AddressData)
], ShippingsData.prototype, "shipping_address", void 0);
tslib_1.__decorate([
    type_graphql_1.Field(type => [items_type_1.ItemsData])
], ShippingsData.prototype, "items", void 0);
tslib_1.__decorate([
    type_graphql_1.Field(type => [tracks_type_1.TracksData])
], ShippingsData.prototype, "tracks", void 0);
tslib_1.__decorate([
    type_graphql_1.Field(type => [invoices_type_1.InvoicesData])
], ShippingsData.prototype, "invoices", void 0);
ShippingsData = tslib_1.__decorate([
    type_graphql_1.ObjectType()
], ShippingsData);
exports.ShippingsData = ShippingsData;
exports.ShippingsType = `

type ShippingsType {
    _id : ID!
    carrier: String
    shipping_method: String
    shipping_date: Date
    shipping_code: String
    shipping_cost: Float
    shipping_estimated_delivery: Date
    delivery_date: Date
    shipping_address: AddressType
    reason_cancelled: String
    items: [ItemsType]
    tracks: [TracksType]
    invoices: [InvoicesType]
}
`;
exports.ShippingsUpdateInput = `

input ShippingsUpdateInput {
    _id : ID!
    carrier: String
    shipping_method: String
    shipping_date: Date
    shipping_code: String
    shipping_cost: Float
    shipping_estimated_delivery: Date
    delivery_date: Date
    shipping_address: AddressUpdateInput
    reason_cancelled: String
    items: [ItemsUpdateInput]
    tracks: [TracksUpdateInput]
    invoices: [InvoicesUpdateInput]
}
`;
exports.ShippingsInput = `

input ShippingsInput {
    _id : ID
    carrier: String!
    shipping_method: String!
    shipping_date: Date!
    shipping_code: String!
    shipping_cost: Float!
    shipping_estimated_delivery: Date!
    delivery_date: Date!
    shipping_address: AddressInput!
    reason_cancelled: String
    items: [ItemsInput!]!
    tracks: [TracksInput]!
    invoices: [InvoicesInput]!
}
`;
//# sourceMappingURL=shippings_type.js.map