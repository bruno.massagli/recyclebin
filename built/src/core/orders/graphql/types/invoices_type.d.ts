import { ItemsData } from './items_type';
export declare class InvoicesData {
    _id: String;
    key: String;
    issue_date: Date;
    items: ItemsData[];
}
export declare const InvoicesType = "\n\n\ntype InvoicesType {\n    _id : ID!\n    key: String\n    issue_date: Date\n    items: [ItemsType]\n}\n";
export declare const InvoicesUpdateInput = "\n\n\ninput InvoicesUpdateInput {\n    _id : ID!\n    key: String\n    issue_date: Date\n    items: [ItemsUpdateInput]\n}\n";
export declare const InvoicesInput = "\n\n\ninput InvoicesInput {\n    _id : ID\n    key: String\n    issue_date: Date\n    items: [ItemsInput!]!\n}\n";
