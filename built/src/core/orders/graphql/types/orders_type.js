"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const type_graphql_1 = require("type-graphql");
const shippings_type_1 = require("./shippings_type");
const customers_type_1 = require("./customers_type");
let OrdersData = class OrdersData {
};
tslib_1.__decorate([
    type_graphql_1.Field(type => String)
], OrdersData.prototype, "_id", void 0);
tslib_1.__decorate([
    type_graphql_1.Field(type => String)
], OrdersData.prototype, "company", void 0);
tslib_1.__decorate([
    type_graphql_1.Field(type => String)
], OrdersData.prototype, "order_number", void 0);
tslib_1.__decorate([
    type_graphql_1.Field(type => Date)
], OrdersData.prototype, "order_date", void 0);
tslib_1.__decorate([
    type_graphql_1.Field(type => Number)
], OrdersData.prototype, "total_gross", void 0);
tslib_1.__decorate([
    type_graphql_1.Field(type => String)
], OrdersData.prototype, "status", void 0);
tslib_1.__decorate([
    type_graphql_1.Field(type => shippings_type_1.ShippingsData)
], OrdersData.prototype, "shipping", void 0);
tslib_1.__decorate([
    type_graphql_1.Field(type => customers_type_1.CustomerData)
], OrdersData.prototype, "customer", void 0);
OrdersData = tslib_1.__decorate([
    type_graphql_1.ObjectType()
], OrdersData);
exports.OrdersData = OrdersData;
exports.OrdersType = `

type OrdersType {
    _id : ID!
    company: String
    order_number: String
    order_date: Date
    total_gross: Float
    status: String
    shipping: ShippingsType
    customer: CustomerType
}
`;
exports.OrdersUpdateInput = `

input OrdersUpdateInput {
    _id : ID!
    company: String
    order_number: String
    order_date: Date
    total_gross: Float
    status: String
    shipping: ShippingsUpdateInput
    customer: CustomerUpdateInput
}
`;
exports.OrdersInput = `

input OrdersInput {
    _id : ID
    company: String!
    order_number: String!
    order_date: Date!
    total_gross: Float!
    status: String!
    shipping: ShippingsInput!
    customer: CustomerInput!
}
`;
exports.OrderStatusInput = `

input OrderStatusInput {
    order_number: String!
    reason_cancelled: String
    key: String
    number: String
    invoiced_date: Date
    carrier: String
    method: String
    tracking_number: String
    tracking_link: String
    date_delivery: Date
    estimated_delivery: Date
    delivered_date: Date
    items: [ItemsUpdateInput]

}
`;
//# sourceMappingURL=orders_type.js.map