import { DocumentsData } from './documents_type';
import { AddressData } from './addresses_type';
export declare class CustomerData {
    _id: String;
    name: String;
    email: String;
    document: DocumentsData;
    phones: String[];
    address: AddressData;
}
export declare const CustomerType = "\n\ntype CustomerType {\n    _id : ID!\n    name: String\n    email: String\n    document: DocumentsType\n    phones: [String]\n    address: AddressType\n}\n";
export declare const CustomerUpdateInput = "\n\ninput CustomerUpdateInput {\n    _id : ID!\n    name: String\n    email: String\n    document: DocumentsUpdateInput\n    phones: [String]\n    address: AddressUpdateInput\n}\n";
export declare const CustomerInput = "\n\ninput CustomerInput {\n    _id : ID\n    name: String!\n    email: String!\n    document: DocumentsInput!\n    phones: [String!]!\n    address: AddressInput!\n}\n";
