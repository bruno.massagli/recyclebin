import { AddressData } from './addresses_type';
import { ItemsData } from './items_type';
import { TracksData } from './tracks_type';
import { InvoicesData } from './invoices_type';
export declare class ShippingsData {
    _id: String;
    carrier: String;
    shipping_method: String;
    shipping_date: Date;
    shipping_code: String;
    shipping_cost: number;
    shipping_estimated_delivery: Date;
    delivery_date: Date;
    shipping_address: AddressData;
    items: ItemsData[];
    tracks: TracksData[];
    invoices: InvoicesData[];
}
export declare const ShippingsType = "\n\ntype ShippingsType {\n    _id : ID!\n    carrier: String\n    shipping_method: String\n    shipping_date: Date\n    shipping_code: String\n    shipping_cost: Float\n    shipping_estimated_delivery: Date\n    delivery_date: Date\n    shipping_address: AddressType\n    reason_cancelled: String\n    items: [ItemsType]\n    tracks: [TracksType]\n    invoices: [InvoicesType]\n}\n";
export declare const ShippingsUpdateInput = "\n\ninput ShippingsUpdateInput {\n    _id : ID!\n    carrier: String\n    shipping_method: String\n    shipping_date: Date\n    shipping_code: String\n    shipping_cost: Float\n    shipping_estimated_delivery: Date\n    delivery_date: Date\n    shipping_address: AddressUpdateInput\n    reason_cancelled: String\n    items: [ItemsUpdateInput]\n    tracks: [TracksUpdateInput]\n    invoices: [InvoicesUpdateInput]\n}\n";
export declare const ShippingsInput = "\n\ninput ShippingsInput {\n    _id : ID\n    carrier: String!\n    shipping_method: String!\n    shipping_date: Date!\n    shipping_code: String!\n    shipping_cost: Float!\n    shipping_estimated_delivery: Date!\n    delivery_date: Date!\n    shipping_address: AddressInput!\n    reason_cancelled: String\n    items: [ItemsInput!]!\n    tracks: [TracksInput]!\n    invoices: [InvoicesInput]!\n}\n";
