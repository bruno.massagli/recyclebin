"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const type_graphql_1 = require("type-graphql");
let ItemsData = class ItemsData {
};
tslib_1.__decorate([
    type_graphql_1.Field(type => String)
], ItemsData.prototype, "_id", void 0);
tslib_1.__decorate([
    type_graphql_1.Field(type => String)
], ItemsData.prototype, "sku", void 0);
tslib_1.__decorate([
    type_graphql_1.Field(type => String)
], ItemsData.prototype, "name", void 0);
tslib_1.__decorate([
    type_graphql_1.Field(type => Number)
], ItemsData.prototype, "quantity", void 0);
tslib_1.__decorate([
    type_graphql_1.Field(type => Number)
], ItemsData.prototype, "original_price", void 0);
tslib_1.__decorate([
    type_graphql_1.Field(type => Number)
], ItemsData.prototype, "special_price", void 0);
ItemsData = tslib_1.__decorate([
    type_graphql_1.ObjectType()
], ItemsData);
exports.ItemsData = ItemsData;
exports.ItemsType = `
type ItemsType {
    _id : ID!
    sku: String
    name: String
    quantity: Float
    original_price: Float
    special_price: Float
}
`;
exports.ItemsUpdateInput = `
input ItemsUpdateInput {
    _id : ID!
    sku: String
    name: String
    quantity: Float
    original_price: Float
    special_price: Float
}
`;
exports.ItemsInput = `
input ItemsInput {
    _id : ID
    sku: String!
    name: String!
    quantity: Float!
    original_price: Float!
    special_price: Float!
}
`;
//# sourceMappingURL=items_type.js.map