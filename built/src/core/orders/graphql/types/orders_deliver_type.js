"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const type_graphql_1 = require("type-graphql");
let OrdersDeliverData = class OrdersDeliverData {
};
tslib_1.__decorate([
    type_graphql_1.Field(type => String)
], OrdersDeliverData.prototype, "order_number", void 0);
tslib_1.__decorate([
    type_graphql_1.Field(type => Date)
], OrdersDeliverData.prototype, "delivered_date", void 0);
OrdersDeliverData = tslib_1.__decorate([
    type_graphql_1.InputType()
], OrdersDeliverData);
exports.OrdersDeliverData = OrdersDeliverData;
exports.OrdersDeliverInput = `

input OrdersDeliverInput {
    order_number: String!
    delivered_date: Date!
}
`;
//# sourceMappingURL=orders_deliver_type.js.map