export declare class AddressData {
    street: String;
    number: number;
    detail: String;
    neighborhood: String;
    city: String;
    region: String;
    country: String;
    postcode: String;
}
export declare const AddressType = "\ntype AddressType {    \n    street: String\n    number: Int\n    detail: String\n    neighborhood: String\n    city: String\n    region: String\n    country: String\n    postcode: String\n}\n";
export declare const AddressUpdateInput = "\ninput AddressUpdateInput {    \n    street: String\n    number: Int\n    detail: String\n    neighborhood: String\n    city: String\n    region: String\n    country: String\n    postcode: String\n}\n";
export declare const AddressInput = "\ninput AddressInput {    \n    street: String!\n    number: Int!\n    detail: String!\n    neighborhood: String!\n    city: String!\n    region: String!\n    country: String!\n    postcode: String!\n}\n";
