"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const type_graphql_1 = require("type-graphql");
const items_type_1 = require("./items_type");
let TracksData = class TracksData {
};
tslib_1.__decorate([
    type_graphql_1.Field(type => String)
], TracksData.prototype, "_id", void 0);
tslib_1.__decorate([
    type_graphql_1.Field(type => String)
], TracksData.prototype, "tracking_link", void 0);
tslib_1.__decorate([
    type_graphql_1.Field(type => String)
], TracksData.prototype, "tracking_number", void 0);
tslib_1.__decorate([
    type_graphql_1.Field(type => [items_type_1.ItemsData])
], TracksData.prototype, "items", void 0);
TracksData = tslib_1.__decorate([
    type_graphql_1.ObjectType()
], TracksData);
exports.TracksData = TracksData;
exports.TracksType = `

type TracksType {
    _id : ID!
    tracking_link: String
    tracking_number: String
    items: [ItemsType]
}
`;
exports.TracksUpdateInput = `

input TracksUpdateInput {
    _id : ID!
    tracking_link: String
    tracking_number: String
    items: [ItemsUpdateInput]
}
`;
exports.TracksInput = `

input TracksInput {
    _id : ID
    tracking_link: String
    tracking_number: String
    items: [ItemsInput!]!
}
`;
//# sourceMappingURL=tracks_type.js.map