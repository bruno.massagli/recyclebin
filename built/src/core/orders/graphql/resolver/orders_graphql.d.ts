export declare const resolverOrders: {
    Query: {
        get_customer_by_email: (parent: any, { email }: {
            email: any;
        }, context: any, info: any) => Promise<object>;
        get_order_by_id: (parent: any, { company, id }: {
            company: any;
            id: any;
        }, context: any, info: any) => Promise<object>;
        get_orders: (parent: any, { company, status, per_page, page, date_start, date_end }: {
            company: any;
            status: any;
            per_page: any;
            page: any;
            date_start: any;
            date_end: any;
        }, context: any, info: any) => Promise<object>;
    };
    Mutation: {
        create_orders: (parent: any, { company, body }: {
            company: any;
            body: any;
        }, context: any, info: any) => Promise<object>;
        update_orders: (parent: any, { company, status, body }: {
            company: any;
            status: any;
            body: any;
        }, context: any, info: any) => Promise<object>;
    };
};
export declare const orders_query = "\n  get_customer_by_email(email: String): CustomerType\n\n  get_order_by_id(company: String, id: String): OrdersType\n\n  get_orders(company: String, status: String, per_page: Int, page: Int, date_start: Date, date_end: Date): [OrdersType]\n";
export declare const orders_mutation = "\n  create_orders(company: String, body: OrdersInput): OrdersType\n\n  update_orders(company: String, status: String, body: OrderStatusInput): OrdersType\n";
