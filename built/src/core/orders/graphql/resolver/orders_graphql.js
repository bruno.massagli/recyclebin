"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const get_order_by_id_lib_1 = require("../../lib/get_order_by_id_lib");
const get_orders_lib_1 = require("../../lib/get_orders_lib");
const get_customers_by_email_lib_1 = require("../../lib/get_customers_by_email_lib");
const post_order_lib_1 = require("../../lib/post_order_lib");
const put_order_status_lib_1 = require("../../lib/put_order_status_lib");
exports.resolverOrders = {
    Query: {
        get_customer_by_email: (parent, { email }, context, info) => tslib_1.__awaiter(this, void 0, void 0, function* () {
            let customer = new get_customers_by_email_lib_1.GetCustomerLib();
            return customer.getCustomerByEmail(email);
        }),
        get_order_by_id: (parent, { company, id }, context, info) => tslib_1.__awaiter(this, void 0, void 0, function* () {
            let order = new get_order_by_id_lib_1.GetOrderByIdLib();
            return order.getOrdersById(company, id);
        }),
        get_orders: (parent, { company, status, per_page, page, date_start, date_end }, context, info) => tslib_1.__awaiter(this, void 0, void 0, function* () {
            let order = new get_orders_lib_1.GetOrdersLib();
            return yield order.getOrders(company, status, per_page, page, date_start, date_end);
        })
    },
    Mutation: {
        create_orders: (parent, { company, body }, context, info) => tslib_1.__awaiter(this, void 0, void 0, function* () {
            let order = new post_order_lib_1.PostOrderLib();
            return yield order.PostOrder(company, body);
        }),
        update_orders: (parent, { company, status, body }, context, info) => tslib_1.__awaiter(this, void 0, void 0, function* () {
            let order = new put_order_status_lib_1.PutOrderStatusLib();
            return yield order.putOrdersStatus(company, status, body);
        })
    }
};
exports.orders_query = `
  get_customer_by_email(email: String): CustomerType

  get_order_by_id(company: String, id: String): OrdersType

  get_orders(company: String, status: String, per_page: Int, page: Int, date_start: Date, date_end: Date): [OrdersType]
`;
exports.orders_mutation = `
  create_orders(company: String, body: OrdersInput): OrdersType

  update_orders(company: String, status: String, body: OrderStatusInput): OrdersType
`;
//# sourceMappingURL=orders_graphql.js.map