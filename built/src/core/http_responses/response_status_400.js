class ResponseStatus400 {
    constructor(data) {
        return {
            status: 400,
            description: 'Requisição mal formulada'
        };
    }
}
module.exports = (data) => {
    return {
        status: 400,
        description: 'Requisição mal formulada'
    };
};
//# sourceMappingURL=response_status_400.js.map