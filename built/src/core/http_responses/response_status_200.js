"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class ResponseStatus200 {
    constructor(data) {
        return {
            data: data,
            status: 200,
            description: 'requisição realizada com sucesso'
        };
    }
}
exports.ResponseStatus200 = ResponseStatus200;
module.exports = (data) => {
    return {
        data: data,
        status: 200,
        description: 'requisição realizada com sucesso'
    };
};
//# sourceMappingURL=response_status_200.js.map