class ResponseStatus404 {
    constructor(data) {
        return {
            status: 404,
            description: 'Recurso não encontrado'
        };
    }
}
module.exports = (data) => {
    return {
        status: 404,
        description: 'Recurso não encontrado'
    };
};
//# sourceMappingURL=response_status_404.js.map