export interface Images {
    name: String;
    url: String;
    id: String;
}
