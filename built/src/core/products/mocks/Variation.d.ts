import { Stock } from './Stock';
import { Images } from '../../products/mocks/Images';
import { Video } from './Video';
import { Price } from './Price';
import { Dimension } from './Dimension';
import { Specification } from './Specification';
export interface Variation {
    _id: String;
    sku: number;
    stock: Stock;
    ean: string;
    nbm: string;
    prices: Price;
    dimensions: Dimension;
    images: Images[];
    videos: Video[];
    specifications: Specification[];
}
