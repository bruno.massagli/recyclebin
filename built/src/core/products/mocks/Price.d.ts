export interface Price {
    list_price: Number;
    sale_price: Number;
}
