export interface Stock {
    quantity: Number;
    reserved: Number;
    unit: String;
}
