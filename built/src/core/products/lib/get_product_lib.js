"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const products_1 = require("../../../db/Mongo/Models/products");
class GetProductLib {
    getProducts(company, per_page, page, date_start, date_end, rest) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            try {
                let products_model = new products_1.Products();
                var Product = products_model.productsModel();
                let products_query = yield Product.find({ company: company, createdAt: { "$gte": new Date(date_start).toISOString(), "$lt": new Date(date_end).toISOString() } })
                    .skip(per_page * page).limit(per_page).exec();
                return products_query;
            }
            catch (err) {
                console.log(err);
                return err;
            }
        });
    }
}
exports.GetProductLib = GetProductLib;
//# sourceMappingURL=get_product_lib.js.map