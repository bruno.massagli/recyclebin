export declare class GetAllProductsLib {
    getAllProducts(search: any, per_page: any, page: any, rest?: any): Promise<object>;
}
