"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const products_1 = require("../../../db/Mongo/Models/products");
const categories_1 = require("../../../db/Mongo/Models/categories");
class GetAllProductsLib {
    getAllProducts(search, per_page, page, rest) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            try {
                let products_model = new products_1.Products();
                let categories_model = new categories_1.Categories();
                var Product = products_model.productsModel();
                var Category = categories_model.categoriesModel();
                let products_query = yield Product.find({ $or: [{ "categories.name": search }, { name: search }, { "categories.name": { $regex: search, $options: 'g' } }, { name: { $regex: search, $options: 'g' } }] })
                    .skip(per_page * page).limit(per_page).exec();
                return products_query;
            }
            catch (err) {
                console.log(err);
                return err;
            }
        });
    }
}
exports.GetAllProductsLib = GetAllProductsLib;
//# sourceMappingURL=get_all_products_lib.js.map