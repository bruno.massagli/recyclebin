"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const products_1 = require("../../../db/Mongo/Models/products");
class GetProductByIdLib {
    getProductsById(company, id, rest) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            try {
                let products_model = new products_1.Products();
                var Product = products_model.productsModel();
                let products_query = yield Product.findOne({ company: company, _id: id }).exec();
                if (!products_query) {
                    if (rest)
                        return { status: 404, message: 'Resource Not Located' };
                    throw new Error('Resource Not Located');
                }
                return products_query;
            }
            catch (err) {
                console.log(err);
                return err;
            }
        });
    }
}
exports.GetProductByIdLib = GetProductByIdLib;
//# sourceMappingURL=get_product_by_id_lib.js.map