"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const consolidate_object_1 = require("../../../helpers/consolidate_object");
const variations_1 = require("../../../db/Mongo/Models/variations");
const mongoose_1 = tslib_1.__importDefault(require("mongoose"));
const products_1 = require("../../../db/Mongo/Models/products");
const class_validator_1 = require("class-validator");
const product_inputs_1 = require("../validator/product_inputs");
class PutProductlib {
    putProducts(company, body, rest) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            try {
                let Props = new consolidate_object_1.ConsolidateObject();
                let product_class = new product_inputs_1.ProductsInputPut();
                product_class = Props.setClassProps(product_class, body);
                let product_validate = yield class_validator_1.validate(product_class);
                if (product_validate.length) {
                    if (rest)
                        return product_validate;
                    throw new Error(JSON.stringify(product_validate));
                }
                let products_model = new products_1.Products();
                var variation_model = new variations_1.Variations();
                var Product = products_model.productsModel();
                var new_variation = variation_model.variationsModel();
                let if_exists = yield Product.findOne({ company: company, _id: body._id }).exec();
                if (!if_exists) {
                    if (rest)
                        return { status: 404, message: 'Resource Not Located' };
                    throw new Error('Resource Not Located');
                }
                var verify_product_exists = yield Product.findOne({ company: company, id_product: body.id_product }).exec();
                if (verify_product_exists) {
                    if (rest)
                        return { message: 'Product Already Exists' };
                    throw new Error('Product Already Exists');
                }
                if (company !== body.company) {
                    if (rest)
                        return { status: 400, message: 'Company Doesnt Match' };
                    throw new Error('Company Doesnt Match');
                }
                var final_variations = body.variations.map((elem, idx) => tslib_1.__awaiter(this, void 0, void 0, function* () {
                    var save_variations = yield new_variation.findOne({ _id: elem._id, sku: elem.sku }).exec();
                    console.log(save_variations);
                    if (!save_variations) {
                        save_variations = new new_variation(Object.assign({ _id: new mongoose_1.default.Types.ObjectId() }, elem));
                        yield save_variations.save();
                    }
                    else {
                        save_variations = yield new_variation.findOneAndUpdate({ _id: elem._id, sku: elem.sku }, Object.assign({}, elem), { new: true }).exec();
                    }
                    return save_variations;
                }));
                body.variations = yield Promise.all(final_variations);
                let save_products = yield Product.findOneAndUpdate({ company: company, _id: body._id }, Object.assign({}, body), { new: true }).exec();
                return save_products;
            }
            catch (err) {
                console.log(err);
                return err;
            }
        });
    }
}
exports.PutProductlib = PutProductlib;
//# sourceMappingURL=put_product_lib.js.map