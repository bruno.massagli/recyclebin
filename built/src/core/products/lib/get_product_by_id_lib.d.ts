export declare class GetProductByIdLib {
    getProductsById(company: any, id: any, rest?: any): Promise<object>;
}
