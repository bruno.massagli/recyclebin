export declare class GetProductLib {
    getProducts(company: any, per_page: any, page: any, date_start: any, date_end: any, rest?: any): Promise<object>;
}
