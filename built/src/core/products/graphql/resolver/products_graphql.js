"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const get_all_products_lib_1 = require("../../lib/get_all_products_lib");
const get_product_by_id_lib_1 = require("../../lib/get_product_by_id_lib");
const get_product_lib_1 = require("../../lib/get_product_lib");
const post_product_lib_1 = require("../../lib/post_product_lib");
const put_product_lib_1 = require("../../lib/put_product_lib");
const put_product_prices_lib_1 = require("../../lib/put_product_prices_lib");
const put_product_stocks_lib_1 = require("../../lib/put_product_stocks_lib");
exports.resolverProducts = {
    Query: {
        get_all_products: (parent, { search, per_page, page }, context, info) => tslib_1.__awaiter(this, void 0, void 0, function* () {
            let prod = new get_all_products_lib_1.GetAllProductsLib();
            return yield prod.getAllProducts(search, per_page, page);
        }),
        get_product_by_id: (parent, { company, id }, context, info) => tslib_1.__awaiter(this, void 0, void 0, function* () {
            let prod = new get_product_by_id_lib_1.GetProductByIdLib();
            return yield prod.getProductsById(company, id);
        }),
        get_products: (parent, { company, per_page, page, date_start, date_end }, context, info) => tslib_1.__awaiter(this, void 0, void 0, function* () {
            let prod = new get_product_lib_1.GetProductLib();
            return yield prod.getProducts(company, per_page, page, date_start, date_end);
        })
    },
    Mutation: {
        create_products: (parent, { company, body }, context, info) => tslib_1.__awaiter(this, void 0, void 0, function* () {
            let prod = new post_product_lib_1.PostProductLib();
            return yield prod.postProducts(company, body);
        }),
        update_products: (parent, { company, body }, context, info) => tslib_1.__awaiter(this, void 0, void 0, function* () {
            let prod = new put_product_lib_1.PutProductlib();
            return yield prod.putProducts(company, body);
        }),
        update_prices: (parent, { company, body }, context, info) => tslib_1.__awaiter(this, void 0, void 0, function* () {
            let prod = new put_product_prices_lib_1.PutProductPricesLib();
            return yield prod.putProductsPrices(company, body);
        }),
        update_stocks: (parent, { company, body }, context, info) => tslib_1.__awaiter(this, void 0, void 0, function* () {
            let prod = new put_product_stocks_lib_1.PutProductStocksLib();
            return yield prod.putProductsStocks(company, body);
        })
    }
};
exports.products_query = `
  get_all_products(search: String, per_page: Int, page: Int, date_start: Date, date_end: Date ): [ProductsType]
  
  get_product_by_id(company: String, id: String): ProductsType
  
  get_products(company: String, per_page: Int, page: Int, date_start: Date, date_end: Date) : [ProductsType]
`;
exports.products_mutation = `
  create_products(company: String, body: ProductsInput): ProductsType
  
  update_products(company: String, body: ProductsUpdateInput): ProductsType
  
  update_prices(company: String, body: PricesUpdateInput): ProductsType
  
  update_stocks(company: String, body: StocksUpdateInput): ProductsType
`;
//# sourceMappingURL=products_graphql.js.map