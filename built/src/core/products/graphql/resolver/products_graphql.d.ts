export declare const resolverProducts: {
    Query: {
        get_all_products: (parent: any, { search, per_page, page }: {
            search: any;
            per_page: any;
            page: any;
        }, context: any, info: any) => Promise<object>;
        get_product_by_id: (parent: any, { company, id }: {
            company: any;
            id: any;
        }, context: any, info: any) => Promise<object>;
        get_products: (parent: any, { company, per_page, page, date_start, date_end }: {
            company: any;
            per_page: any;
            page: any;
            date_start: any;
            date_end: any;
        }, context: any, info: any) => Promise<object>;
    };
    Mutation: {
        create_products: (parent: any, { company, body }: {
            company: any;
            body: any;
        }, context: any, info: any) => Promise<object>;
        update_products: (parent: any, { company, body }: {
            company: any;
            body: any;
        }, context: any, info: any) => Promise<object>;
        update_prices: (parent: any, { company, body }: {
            company: any;
            body: any;
        }, context: any, info: any) => Promise<object>;
        update_stocks: (parent: any, { company, body }: {
            company: any;
            body: any;
        }, context: any, info: any) => Promise<object>;
    };
};
export declare const products_query = "\n  get_all_products(search: String, per_page: Int, page: Int, date_start: Date, date_end: Date ): [ProductsType]\n  \n  get_product_by_id(company: String, id: String): ProductsType\n  \n  get_products(company: String, per_page: Int, page: Int, date_start: Date, date_end: Date) : [ProductsType]\n";
export declare const products_mutation = "\n  create_products(company: String, body: ProductsInput): ProductsType\n  \n  update_products(company: String, body: ProductsUpdateInput): ProductsType\n  \n  update_prices(company: String, body: PricesUpdateInput): ProductsType\n  \n  update_stocks(company: String, body: StocksUpdateInput): ProductsType\n";
