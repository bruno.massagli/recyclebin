"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const type_graphql_1 = require("type-graphql");
let VideosData = class VideosData {
};
tslib_1.__decorate([
    type_graphql_1.Field(type => String)
], VideosData.prototype, "name", void 0);
tslib_1.__decorate([
    type_graphql_1.Field(type => String)
], VideosData.prototype, "url", void 0);
tslib_1.__decorate([
    type_graphql_1.Field(type => String)
], VideosData.prototype, "id", void 0);
VideosData = tslib_1.__decorate([
    type_graphql_1.ObjectType()
], VideosData);
exports.VideosData = VideosData;
exports.VideosType = `
type VideosType {
    name: String
    url: String
    id: String

}
`;
exports.VideosUpdateInput = `
input VideosUpdateInput {
    name: String
    url: String
    id: String

}
`;
exports.VideosInput = `
input VideosInput {
    name: String!
    url: String!
    id: String!

}
`;
//# sourceMappingURL=videos_type.js.map