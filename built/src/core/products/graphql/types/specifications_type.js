"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const type_graphql_1 = require("type-graphql");
let SpecificationsData = class SpecificationsData {
};
tslib_1.__decorate([
    type_graphql_1.Field(type => String)
], SpecificationsData.prototype, "key", void 0);
tslib_1.__decorate([
    type_graphql_1.Field(type => String)
], SpecificationsData.prototype, "value", void 0);
SpecificationsData = tslib_1.__decorate([
    type_graphql_1.ObjectType()
], SpecificationsData);
exports.SpecificationsData = SpecificationsData;
exports.SpecificationsType = `
type SpecificationsType {
    key: String
    value: String
}
`;
exports.SpecificationsUpdateInput = `
input SpecificationsUpdateInput {
    key: String
    value: String
}
`;
exports.SpecificationsInput = `
input SpecificationsInput {
    key: String!
    value: String!
}
`;
//# sourceMappingURL=specifications_type.js.map