export declare class StocksData {
    quantity: Number;
    reserved: Number;
    unit: String;
}
export declare const StocksType = "\ntype StocksType {\n    quantity: Float\n    reserved: Float\n    unit: String\n\n}\n";
export declare const StocksUpdateInput = "\ninput StocksUpdateInput {\n    quantity: Float\n    reserved: Float\n    unit: String\n\n}\n";
export declare const StocksInput = "\ninput StocksInput {\n    quantity: Float!\n    reserved: Float!\n    unit: String!\n\n}\n";
