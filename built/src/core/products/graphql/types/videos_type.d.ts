export declare class VideosData {
    name: String;
    url: String;
    id: String;
}
export declare const VideosType = "\ntype VideosType {\n    name: String\n    url: String\n    id: String\n\n}\n";
export declare const VideosUpdateInput = "\ninput VideosUpdateInput {\n    name: String\n    url: String\n    id: String\n\n}\n";
export declare const VideosInput = "\ninput VideosInput {\n    name: String!\n    url: String!\n    id: String!\n\n}\n";
