export declare class ImagesData {
    name: String;
    url: String;
    id: String;
}
export declare const ImagesType = "\ntype ImagesType {\n    name: String\n    url: String\n    id: String\n}\n";
export declare const ImagesUpdateInput = "\ninput ImagesUpdateInput {\n    name: String\n    url: String\n    id: String\n}\n";
export declare const ImagesInput = "\ninput ImagesInput {\n    name: String!\n    url: String!\n    id: String!\n}\n";
