import { CategoriesData } from '../../../categories/graphql/types/categories_type';
import { VariationsData } from './vatriations_type';
export declare class ProductsData {
    _id: String;
    name: String;
    company: String;
    id_product: String;
    brand: String;
    description: String;
    categories: CategoriesData[];
    warranty: String;
    variations: VariationsData[];
}
export declare const ProductsType = "\n\n\ntype ProductsType {\n    _id : ID!\n    name: String\n    company: String\n    id_product: String\n    brand: String\n    description: String\n    categories: [CategoriesType]\n    warranty: String\n    variations: [VariationsType]\n}\n";
export declare const ProductsUpdateInput = "\n\ninput ProductsUpdateInput {\n    _id : ID!\n    name: String\n    company: String\n    id_product: String\n    brand: String\n    description: String\n    categories: [CategoriesUpdateInput]\n    warranty: String\n    variations: [VariationsUpdateInput]\n}\n";
export declare const ProductsInput = "\n\n\ninput ProductsInput {\n    _id : ID\n    name: String!\n    company: String!\n    id_product: String!\n    brand: String!\n    description: String!\n    categories: [CategoriesInput!]!\n    warranty: String!\n    variations: [VariationsInput!]!\n}\n";
