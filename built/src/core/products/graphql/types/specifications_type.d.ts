export declare class SpecificationsData {
    key: String;
    value: String;
}
export declare const SpecificationsType = "\ntype SpecificationsType {\n    key: String\n    value: String\n}\n";
export declare const SpecificationsUpdateInput = "\ninput SpecificationsUpdateInput {\n    key: String\n    value: String\n}\n";
export declare const SpecificationsInput = "\ninput SpecificationsInput {\n    key: String!\n    value: String!\n}\n";
