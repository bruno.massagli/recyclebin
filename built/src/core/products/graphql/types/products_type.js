"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const type_graphql_1 = require("type-graphql");
const categories_type_1 = require("../../../categories/graphql/types/categories_type");
const vatriations_type_1 = require("./vatriations_type");
let ProductsData = class ProductsData {
};
tslib_1.__decorate([
    type_graphql_1.Field(type => String)
], ProductsData.prototype, "_id", void 0);
tslib_1.__decorate([
    type_graphql_1.Field(type => String)
], ProductsData.prototype, "name", void 0);
tslib_1.__decorate([
    type_graphql_1.Field(type => String)
], ProductsData.prototype, "company", void 0);
tslib_1.__decorate([
    type_graphql_1.Field(type => String)
], ProductsData.prototype, "id_product", void 0);
tslib_1.__decorate([
    type_graphql_1.Field(type => String)
], ProductsData.prototype, "brand", void 0);
tslib_1.__decorate([
    type_graphql_1.Field(type => String)
], ProductsData.prototype, "description", void 0);
tslib_1.__decorate([
    type_graphql_1.Field(type => [categories_type_1.CategoriesData])
], ProductsData.prototype, "categories", void 0);
tslib_1.__decorate([
    type_graphql_1.Field(type => String)
], ProductsData.prototype, "warranty", void 0);
tslib_1.__decorate([
    type_graphql_1.Field(type => [vatriations_type_1.VariationsData])
], ProductsData.prototype, "variations", void 0);
ProductsData = tslib_1.__decorate([
    type_graphql_1.ObjectType()
], ProductsData);
exports.ProductsData = ProductsData;
exports.ProductsType = `


type ProductsType {
    _id : ID!
    name: String
    company: String
    id_product: String
    brand: String
    description: String
    categories: [CategoriesType]
    warranty: String
    variations: [VariationsType]
}
`;
exports.ProductsUpdateInput = `

input ProductsUpdateInput {
    _id : ID!
    name: String
    company: String
    id_product: String
    brand: String
    description: String
    categories: [CategoriesUpdateInput]
    warranty: String
    variations: [VariationsUpdateInput]
}
`;
exports.ProductsInput = `


input ProductsInput {
    _id : ID
    name: String!
    company: String!
    id_product: String!
    brand: String!
    description: String!
    categories: [CategoriesInput!]!
    warranty: String!
    variations: [VariationsInput!]!
}
`;
//# sourceMappingURL=products_type.js.map