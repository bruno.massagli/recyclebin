"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const type_graphql_1 = require("type-graphql");
let ImagesData = class ImagesData {
};
tslib_1.__decorate([
    type_graphql_1.Field(type => String)
], ImagesData.prototype, "name", void 0);
tslib_1.__decorate([
    type_graphql_1.Field(type => String)
], ImagesData.prototype, "url", void 0);
tslib_1.__decorate([
    type_graphql_1.Field(type => String)
], ImagesData.prototype, "id", void 0);
ImagesData = tslib_1.__decorate([
    type_graphql_1.ObjectType()
], ImagesData);
exports.ImagesData = ImagesData;
exports.ImagesType = `
type ImagesType {
    name: String
    url: String
    id: String
}
`;
exports.ImagesUpdateInput = `
input ImagesUpdateInput {
    name: String
    url: String
    id: String
}
`;
exports.ImagesInput = `
input ImagesInput {
    name: String!
    url: String!
    id: String!
}
`;
//# sourceMappingURL=images_type.js.map