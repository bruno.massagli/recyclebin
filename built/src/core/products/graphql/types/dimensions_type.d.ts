export declare class DimensionsData {
    weight: Number;
    length: Number;
    width: Number;
    height: Number;
    cubic_wheight: Number;
}
export declare const DimensionsType = "\ntype DimensionsType {\n    weight: Float\n    length: Float\n    width: Float\n    height: Float\n    cubic_wheight: Float\n}\n";
export declare const DimensionsUpdateInput = "\ninput DimensionsUpdateInput {\n    weight: Float\n    length: Float\n    width: Float\n    height: Float\n    cubic_wheight: Float\n}\n";
export declare const DimensionsInput = "\ninput DimensionsInput {\n    weight: Float!\n    length: Float!\n    width: Float!\n    height: Float!\n    cubic_wheight: Float!\n}\n";
