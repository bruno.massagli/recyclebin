"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const type_graphql_1 = require("type-graphql");
let StocksData = class StocksData {
};
tslib_1.__decorate([
    type_graphql_1.Field(type => Number)
], StocksData.prototype, "quantity", void 0);
tslib_1.__decorate([
    type_graphql_1.Field(type => Number)
], StocksData.prototype, "reserved", void 0);
tslib_1.__decorate([
    type_graphql_1.Field(type => String)
], StocksData.prototype, "unit", void 0);
StocksData = tslib_1.__decorate([
    type_graphql_1.ObjectType()
], StocksData);
exports.StocksData = StocksData;
exports.StocksType = `
type StocksType {
    quantity: Float
    reserved: Float
    unit: String

}
`;
exports.StocksUpdateInput = `
input StocksUpdateInput {
    quantity: Float
    reserved: Float
    unit: String

}
`;
exports.StocksInput = `
input StocksInput {
    quantity: Float!
    reserved: Float!
    unit: String!

}
`;
//# sourceMappingURL=stocks_type.js.map