"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const type_graphql_1 = require("type-graphql");
let DimensionsData = class DimensionsData {
};
tslib_1.__decorate([
    type_graphql_1.Field(type => Number)
], DimensionsData.prototype, "weight", void 0);
tslib_1.__decorate([
    type_graphql_1.Field(type => Number)
], DimensionsData.prototype, "length", void 0);
tslib_1.__decorate([
    type_graphql_1.Field(type => Number)
], DimensionsData.prototype, "width", void 0);
tslib_1.__decorate([
    type_graphql_1.Field(type => Number)
], DimensionsData.prototype, "height", void 0);
tslib_1.__decorate([
    type_graphql_1.Field(type => Number)
], DimensionsData.prototype, "cubic_wheight", void 0);
DimensionsData = tslib_1.__decorate([
    type_graphql_1.ObjectType()
], DimensionsData);
exports.DimensionsData = DimensionsData;
exports.DimensionsType = `
type DimensionsType {
    weight: Float
    length: Float
    width: Float
    height: Float
    cubic_wheight: Float
}
`;
exports.DimensionsUpdateInput = `
input DimensionsUpdateInput {
    weight: Float
    length: Float
    width: Float
    height: Float
    cubic_wheight: Float
}
`;
exports.DimensionsInput = `
input DimensionsInput {
    weight: Float!
    length: Float!
    width: Float!
    height: Float!
    cubic_wheight: Float!
}
`;
//# sourceMappingURL=dimensions_type.js.map