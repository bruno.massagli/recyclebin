import { StocksData } from './stocks_type';
import { PricesData } from './prices_type';
import { DimensionsData } from './dimensions_type';
import { ImagesData } from './images_type';
import { VideosData } from './videos_type';
import { SpecificationsData } from './specifications_type';
export declare class VariationsData {
    _id: String;
    sku: number;
    stock: StocksData;
    ean: String;
    nbm: String;
    prices: PricesData;
    dimensions: DimensionsData;
    images: ImagesData[];
    videos: VideosData[];
    specifications: SpecificationsData[];
}
export declare const VariationsType = "\n\ntype VariationsType {\n    _id : ID!\n    sku: String\n    id_product: String\n    stock: StocksType\n    ean: String\n    nbm: String\n    prices: PricesType\n    dimensions: DimensionsType\n    images: [ImagesType]\n    videos: [VideosType]\n    specifications: [SpecificationsType]\n}\n";
export declare const VariationsUpdateInput = "\n\ninput VariationsUpdateInput {\n    _id : ID\n    sku: String\n    id_product: String\n    stock: StocksUpdateInput\n    ean: String\n    nbm: String\n    prices: PricesUpdateInput\n    dimensions: DimensionsUpdateInput\n    images: [ImagesUpdateInput]\n    videos: [VideosUpdateInput]\n    specifications: [SpecificationsUpdateInput]\n}\n";
export declare const VariationsInput = "\n\ninput VariationsInput {\n    _id : ID\n    sku: String!\n    id_product: String!\n    stock: StocksInput!\n    ean: String!\n    nbm: String!\n    prices: PricesInput!\n    dimensions: DimensionsInput!\n    images: [ImagesInput!]!\n    videos: [VideosInput!]!\n    specifications: [SpecificationsInput!]!\n}\n";
