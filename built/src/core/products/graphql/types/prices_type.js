"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const type_graphql_1 = require("type-graphql");
let PricesData = class PricesData {
};
tslib_1.__decorate([
    type_graphql_1.Field(type => Number)
], PricesData.prototype, "list_price", void 0);
tslib_1.__decorate([
    type_graphql_1.Field(type => Number)
], PricesData.prototype, "sale_price", void 0);
PricesData = tslib_1.__decorate([
    type_graphql_1.ObjectType()
], PricesData);
exports.PricesData = PricesData;
exports.PricesType = `
type PricesType {
    list_price: Float
    sale_price: Float
}
`;
exports.PricesUpdateInput = `
input PricesUpdateInput {
    list_price: Float
    sale_price: Float
}
`;
exports.PricesInput = `
input PricesInput {
    list_price: Float!
    sale_price: Float!
}
`;
//# sourceMappingURL=prices_type.js.map