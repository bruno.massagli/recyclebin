export declare class PricesData {
    list_price: Number;
    sale_price: Number;
}
export declare const PricesType = "\ntype PricesType {\n    list_price: Float\n    sale_price: Float\n}\n";
export declare const PricesUpdateInput = "\ninput PricesUpdateInput {\n    list_price: Float\n    sale_price: Float\n}\n";
export declare const PricesInput = "\ninput PricesInput {\n    list_price: Float!\n    sale_price: Float!\n}\n";
