"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const type_graphql_1 = require("type-graphql");
const stocks_type_1 = require("./stocks_type");
const prices_type_1 = require("./prices_type");
const dimensions_type_1 = require("./dimensions_type");
const images_type_1 = require("./images_type");
const videos_type_1 = require("./videos_type");
const specifications_type_1 = require("./specifications_type");
let VariationsData = class VariationsData {
};
tslib_1.__decorate([
    type_graphql_1.Field(type => String)
], VariationsData.prototype, "_id", void 0);
tslib_1.__decorate([
    type_graphql_1.Field(type => Number)
], VariationsData.prototype, "sku", void 0);
tslib_1.__decorate([
    type_graphql_1.Field(type => stocks_type_1.StocksData)
], VariationsData.prototype, "stock", void 0);
tslib_1.__decorate([
    type_graphql_1.Field(type => String)
], VariationsData.prototype, "ean", void 0);
tslib_1.__decorate([
    type_graphql_1.Field(type => String)
], VariationsData.prototype, "nbm", void 0);
tslib_1.__decorate([
    type_graphql_1.Field(type => prices_type_1.PricesData)
], VariationsData.prototype, "prices", void 0);
tslib_1.__decorate([
    type_graphql_1.Field(type => dimensions_type_1.DimensionsData)
], VariationsData.prototype, "dimensions", void 0);
tslib_1.__decorate([
    type_graphql_1.Field(type => [images_type_1.ImagesData])
], VariationsData.prototype, "images", void 0);
tslib_1.__decorate([
    type_graphql_1.Field(type => [videos_type_1.VideosData])
], VariationsData.prototype, "videos", void 0);
tslib_1.__decorate([
    type_graphql_1.Field(type => [specifications_type_1.SpecificationsData])
], VariationsData.prototype, "specifications", void 0);
VariationsData = tslib_1.__decorate([
    type_graphql_1.ObjectType()
], VariationsData);
exports.VariationsData = VariationsData;
exports.VariationsType = `

type VariationsType {
    _id : ID!
    sku: String
    id_product: String
    stock: StocksType
    ean: String
    nbm: String
    prices: PricesType
    dimensions: DimensionsType
    images: [ImagesType]
    videos: [VideosType]
    specifications: [SpecificationsType]
}
`;
exports.VariationsUpdateInput = `

input VariationsUpdateInput {
    _id : ID
    sku: String
    id_product: String
    stock: StocksUpdateInput
    ean: String
    nbm: String
    prices: PricesUpdateInput
    dimensions: DimensionsUpdateInput
    images: [ImagesUpdateInput]
    videos: [VideosUpdateInput]
    specifications: [SpecificationsUpdateInput]
}
`;
exports.VariationsInput = `

input VariationsInput {
    _id : ID
    sku: String!
    id_product: String!
    stock: StocksInput!
    ean: String!
    nbm: String!
    prices: PricesInput!
    dimensions: DimensionsInput!
    images: [ImagesInput!]!
    videos: [VideosInput!]!
    specifications: [SpecificationsInput!]!
}
`;
//# sourceMappingURL=vatriations_type.js.map