"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const class_validator_1 = require("class-validator");
class StocksInputPost {
}
tslib_1.__decorate([
    class_validator_1.IsNumber(),
    class_validator_1.IsDefined()
], StocksInputPost.prototype, "quantity", void 0);
tslib_1.__decorate([
    class_validator_1.IsNumber(),
    class_validator_1.IsDefined()
], StocksInputPost.prototype, "reserved", void 0);
tslib_1.__decorate([
    class_validator_1.IsDefined()
], StocksInputPost.prototype, "unit", void 0);
exports.StocksInputPost = StocksInputPost;
class StocksInputPut {
}
tslib_1.__decorate([
    class_validator_1.IsOptional(),
    class_validator_1.IsNumber()
], StocksInputPut.prototype, "quantity", void 0);
tslib_1.__decorate([
    class_validator_1.IsOptional(),
    class_validator_1.IsNumber()
], StocksInputPut.prototype, "reserved", void 0);
tslib_1.__decorate([
    class_validator_1.IsOptional()
], StocksInputPut.prototype, "unit", void 0);
exports.StocksInputPut = StocksInputPut;
//# sourceMappingURL=stock_inputs.js.map