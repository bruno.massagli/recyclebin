"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const class_validator_1 = require("class-validator");
class VariationsInputPost {
}
tslib_1.__decorate([
    class_validator_1.IsDefined()
], VariationsInputPost.prototype, "sku", void 0);
tslib_1.__decorate([
    class_validator_1.IsDefined()
], VariationsInputPost.prototype, "stock", void 0);
tslib_1.__decorate([
    class_validator_1.IsDefined()
], VariationsInputPost.prototype, "ean", void 0);
tslib_1.__decorate([
    class_validator_1.IsDefined()
], VariationsInputPost.prototype, "nbm", void 0);
tslib_1.__decorate([
    class_validator_1.IsDefined()
], VariationsInputPost.prototype, "prices", void 0);
tslib_1.__decorate([
    class_validator_1.IsDefined()
], VariationsInputPost.prototype, "dimensions", void 0);
tslib_1.__decorate([
    class_validator_1.IsDefined()
], VariationsInputPost.prototype, "images", void 0);
tslib_1.__decorate([
    class_validator_1.IsDefined()
], VariationsInputPost.prototype, "videos", void 0);
tslib_1.__decorate([
    class_validator_1.IsDefined()
], VariationsInputPost.prototype, "specifications", void 0);
exports.VariationsInputPost = VariationsInputPost;
class VariationsInputPut {
}
exports.VariationsInputPut = VariationsInputPut;
//# sourceMappingURL=variation_inputs.js.map