import { VariationsInputPost, VariationsInputPut } from './variation_inputs';
import { CategoriesInputPost, CategoriesInputPut } from '../../categories/validator/category_inputs';
export declare class ProductsInputPost {
    name: String;
    product_id: String;
    company: String;
    brand: String;
    description: String;
    categories: CategoriesInputPost[];
    warranty: String;
    variations: VariationsInputPost[];
}
export declare class ProductsInputPut {
    _id: String;
    name: String;
    product_id: String;
    company: String;
    brand: String;
    description: String;
    categories: CategoriesInputPut[];
    warranty: String;
    variations: VariationsInputPut[];
}
