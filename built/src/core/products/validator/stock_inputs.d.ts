export declare class StocksInputPost {
    quantity: Number;
    reserved: Number;
    unit: String;
}
export declare class StocksInputPut {
    quantity: Number;
    reserved: Number;
    unit: String;
}
