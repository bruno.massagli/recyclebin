export declare class ImagesInputPost {
    name: String;
    url: String;
    id: String;
}
export declare class ImagesInputPut {
    name: String;
    url: String;
    id: String;
}
