import { StocksInputPost, StocksInputPut } from './stock_inputs';
import { ImagesInputPost, ImagesInputPut } from './images_inputs';
import { VideosInputPost, VideosInputPut } from './video_inputs';
import { PricesInputPost, PricesInputPut } from './price_inputs';
import { DimensionsInputPost, DimensionsInputPut } from './dimension_inputs';
import { SpecificationsInputPost, SpecificationsInputPut } from './specification_inputs';
export declare class VariationsInputPost {
    _id: String;
    sku: number;
    stock: StocksInputPost;
    ean: String;
    nbm: String;
    prices: PricesInputPost;
    dimensions: DimensionsInputPost;
    images: ImagesInputPost[];
    videos: VideosInputPost[];
    specifications: SpecificationsInputPost[];
}
export declare class VariationsInputPut {
    _id: String;
    sku: number;
    stock: StocksInputPut;
    ean: String;
    nbm: String;
    prices: PricesInputPut;
    dimensions: DimensionsInputPut;
    images: ImagesInputPut[];
    videos: VideosInputPut[];
    specifications: SpecificationsInputPut[];
}
