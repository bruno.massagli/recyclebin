"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const class_validator_1 = require("class-validator");
class ImagesInputPost {
}
tslib_1.__decorate([
    class_validator_1.IsDefined()
], ImagesInputPost.prototype, "name", void 0);
tslib_1.__decorate([
    class_validator_1.IsUrl(),
    class_validator_1.IsDefined()
], ImagesInputPost.prototype, "url", void 0);
tslib_1.__decorate([
    class_validator_1.IsDefined()
], ImagesInputPost.prototype, "id", void 0);
exports.ImagesInputPost = ImagesInputPost;
class ImagesInputPut {
}
tslib_1.__decorate([
    class_validator_1.IsOptional(),
    class_validator_1.IsUrl()
], ImagesInputPut.prototype, "url", void 0);
exports.ImagesInputPut = ImagesInputPut;
//# sourceMappingURL=images_inputs.js.map