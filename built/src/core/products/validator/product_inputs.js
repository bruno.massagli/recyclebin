"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const class_validator_1 = require("class-validator");
class ProductsInputPost {
}
tslib_1.__decorate([
    class_validator_1.MaxLength(100),
    class_validator_1.IsDefined()
], ProductsInputPost.prototype, "name", void 0);
tslib_1.__decorate([
    class_validator_1.IsDefined()
], ProductsInputPost.prototype, "product_id", void 0);
tslib_1.__decorate([
    class_validator_1.IsDefined()
], ProductsInputPost.prototype, "company", void 0);
tslib_1.__decorate([
    class_validator_1.MaxLength(40),
    class_validator_1.IsDefined()
], ProductsInputPost.prototype, "brand", void 0);
tslib_1.__decorate([
    class_validator_1.IsDefined()
], ProductsInputPost.prototype, "description", void 0);
tslib_1.__decorate([
    class_validator_1.IsDefined()
], ProductsInputPost.prototype, "categories", void 0);
tslib_1.__decorate([
    class_validator_1.IsDefined()
], ProductsInputPost.prototype, "warranty", void 0);
tslib_1.__decorate([
    class_validator_1.IsDefined()
], ProductsInputPost.prototype, "variations", void 0);
exports.ProductsInputPost = ProductsInputPost;
class ProductsInputPut {
}
tslib_1.__decorate([
    class_validator_1.IsDefined()
], ProductsInputPut.prototype, "_id", void 0);
tslib_1.__decorate([
    class_validator_1.IsOptional(),
    class_validator_1.MaxLength(100)
], ProductsInputPut.prototype, "name", void 0);
tslib_1.__decorate([
    class_validator_1.IsOptional(),
    class_validator_1.MaxLength(40)
], ProductsInputPut.prototype, "brand", void 0);
exports.ProductsInputPut = ProductsInputPut;
//# sourceMappingURL=product_inputs.js.map