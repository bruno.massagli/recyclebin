"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const class_validator_1 = require("class-validator");
class SpecificationsInputPost {
}
tslib_1.__decorate([
    class_validator_1.IsDefined()
], SpecificationsInputPost.prototype, "key", void 0);
tslib_1.__decorate([
    class_validator_1.IsDefined()
], SpecificationsInputPost.prototype, "value", void 0);
exports.SpecificationsInputPost = SpecificationsInputPost;
class SpecificationsInputPut {
}
exports.SpecificationsInputPut = SpecificationsInputPut;
//# sourceMappingURL=specification_inputs.js.map