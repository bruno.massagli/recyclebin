export declare class PricesInputPost {
    list_price: Number;
    sale_price: Number;
}
export declare class PricesInputPut {
    list_price: Number;
    sale_price: Number;
}
