export declare class VideosInputPost {
    name: String;
    url: String;
    id: String;
}
export declare class VideosInputPut {
    name: String;
    url: String;
    id: String;
}
