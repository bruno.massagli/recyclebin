"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const class_validator_1 = require("class-validator");
class DimensionsInputPost {
}
tslib_1.__decorate([
    class_validator_1.IsDefined(),
    class_validator_1.IsNumber()
], DimensionsInputPost.prototype, "weight", void 0);
tslib_1.__decorate([
    class_validator_1.IsDefined(),
    class_validator_1.IsNumber()
], DimensionsInputPost.prototype, "length", void 0);
tslib_1.__decorate([
    class_validator_1.IsDefined(),
    class_validator_1.IsNumber()
], DimensionsInputPost.prototype, "width", void 0);
tslib_1.__decorate([
    class_validator_1.IsDefined(),
    class_validator_1.IsNumber()
], DimensionsInputPost.prototype, "height", void 0);
tslib_1.__decorate([
    class_validator_1.IsDefined(),
    class_validator_1.IsNumber()
], DimensionsInputPost.prototype, "cubic_wheight", void 0);
exports.DimensionsInputPost = DimensionsInputPost;
class DimensionsInputPut {
}
tslib_1.__decorate([
    class_validator_1.IsOptional(),
    class_validator_1.IsNumber()
], DimensionsInputPut.prototype, "weight", void 0);
tslib_1.__decorate([
    class_validator_1.IsOptional(),
    class_validator_1.IsNumber()
], DimensionsInputPut.prototype, "length", void 0);
tslib_1.__decorate([
    class_validator_1.IsOptional(),
    class_validator_1.IsNumber()
], DimensionsInputPut.prototype, "width", void 0);
tslib_1.__decorate([
    class_validator_1.IsOptional(),
    class_validator_1.IsNumber()
], DimensionsInputPut.prototype, "height", void 0);
tslib_1.__decorate([
    class_validator_1.IsOptional(),
    class_validator_1.IsNumber()
], DimensionsInputPut.prototype, "cubic_wheight", void 0);
exports.DimensionsInputPut = DimensionsInputPut;
//# sourceMappingURL=dimension_inputs.js.map