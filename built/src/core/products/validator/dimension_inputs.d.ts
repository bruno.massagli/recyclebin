export declare class DimensionsInputPost {
    weight: Number;
    length: Number;
    width: Number;
    height: Number;
    cubic_wheight: Number;
}
export declare class DimensionsInputPut {
    weight: Number;
    length: Number;
    width: Number;
    height: Number;
    cubic_wheight: Number;
}
