"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const class_validator_1 = require("class-validator");
class VideosInputPost {
}
tslib_1.__decorate([
    class_validator_1.MaxLength(50),
    class_validator_1.IsDefined()
], VideosInputPost.prototype, "name", void 0);
tslib_1.__decorate([
    class_validator_1.IsUrl(),
    class_validator_1.IsDefined()
], VideosInputPost.prototype, "url", void 0);
tslib_1.__decorate([
    class_validator_1.IsDefined()
], VideosInputPost.prototype, "id", void 0);
exports.VideosInputPost = VideosInputPost;
class VideosInputPut {
}
tslib_1.__decorate([
    class_validator_1.IsOptional(),
    class_validator_1.MaxLength(50)
], VideosInputPut.prototype, "name", void 0);
tslib_1.__decorate([
    class_validator_1.IsOptional(),
    class_validator_1.IsUrl()
], VideosInputPut.prototype, "url", void 0);
exports.VideosInputPut = VideosInputPut;
//# sourceMappingURL=video_inputs.js.map