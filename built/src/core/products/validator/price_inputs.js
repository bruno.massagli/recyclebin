"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const class_validator_1 = require("class-validator");
class PricesInputPost {
}
tslib_1.__decorate([
    class_validator_1.IsNumber(),
    class_validator_1.IsDefined()
], PricesInputPost.prototype, "list_price", void 0);
tslib_1.__decorate([
    class_validator_1.IsNumber(),
    class_validator_1.IsDefined()
], PricesInputPost.prototype, "sale_price", void 0);
exports.PricesInputPost = PricesInputPost;
class PricesInputPut {
}
tslib_1.__decorate([
    class_validator_1.IsOptional(),
    class_validator_1.IsNumber()
], PricesInputPut.prototype, "list_price", void 0);
tslib_1.__decorate([
    class_validator_1.IsOptional(),
    class_validator_1.IsNumber()
], PricesInputPut.prototype, "sale_price", void 0);
exports.PricesInputPut = PricesInputPut;
//# sourceMappingURL=price_inputs.js.map