"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
require("reflect-metadata");
require('dotenv').config();
const express_1 = tslib_1.__importDefault(require("express"));
const apollo_server_express_1 = require("apollo-server-express");
const morgan_1 = tslib_1.__importDefault(require("morgan"));
const path = tslib_1.__importStar(require("path"));
const bodyParser = tslib_1.__importStar(require("body-parser"));
const bodyParserGraphQl = tslib_1.__importStar(require("body-parser-graphql"));
const method_override_1 = tslib_1.__importDefault(require("method-override"));
const router_1 = require("./src/router/products/router");
const router_2 = require("./src/router/orders/router");
const router_3 = require("./src/router/categories/router");
const router_4 = require("./src/router/auth/router");
const products_graphql_1 = require("./src/core/products/graphql/resolver/products_graphql");
const orders_graphql_1 = require("./src/core/orders/graphql/resolver/orders_graphql");
const categories_graphql_1 = require("./src/core/categories/graphql/resolver/categories_graphql");
const auth_graphql_1 = require("./src/core/auth/graphql/resolver/auth_graphql");
const index_1 = require("./src/core/general_types/index");
const server = tslib_1.__importStar(require("./src/server"));
const http = tslib_1.__importStar(require("http"));
const index_2 = require("./src/db/index");
const lodash_1 = require("lodash");
const router = express_1.default.Router();
var httpPort = normalizePort(process.env.PORT || 3030);
var app = server.Server.bootstrap().app;
app.set("port", httpPort);
var httpServer = http.createServer(app);
httpServer.on("error", onError);
httpServer.on("listening", onListening);
app.use(express_1.default.static(path.join(__dirname, 'public')));
app.use(morgan_1.default('dev'));
app.use(bodyParser.json());
app.use(bodyParserGraphQl.graphql());
app.use(bodyParser.urlencoded({
    extended: true
}));
let product_routes = new router_1.ProductsRouter();
let orders_routes = new router_2.OrdersRouter();
let categories_routes = new router_3.CategoriesRouter();
let auth_routes = new router_4.AuthRouter();
app.use('/', product_routes.router);
app.use('/', orders_routes.router);
app.use('/', categories_routes.router);
app.use('/', auth_routes.router);
app.use(method_override_1.default());
let resolvers = {};
let execSchema = {
    typeDefs: apollo_server_express_1.gql `${index_1.types_graphql}`,
    resolvers: lodash_1.merge(resolvers, products_graphql_1.resolverProducts, orders_graphql_1.resolverOrders, categories_graphql_1.resolverCategories, auth_graphql_1.resolverAuth)
};
const serverApollo = new apollo_server_express_1.ApolloServer({
    typeDefs: execSchema.typeDefs,
    resolvers: execSchema.resolvers,
    introspection: true,
    playground: true
});
let db = new index_2.IndexDB().mongoose
    .then(obj => {
    serverApollo.applyMiddleware({ app, cors: { credentials: true, origin: true } });
    httpServer.listen(httpPort);
    console.log('MongoDb Connected');
})
    .catch(console.error);
function normalizePort(val) {
    var port = parseInt(val, 10);
    if (isNaN(port)) {
        return val;
    }
    if (port >= 0) {
        return port;
    }
    return false;
}
function onError(error) {
    if (error.syscall !== "listen") {
        throw error;
    }
    var bind = typeof httpPort === "string"
        ? "Pipe " + httpPort
        : "Port " + httpPort;
    switch (error.code) {
        case "EACCES":
            console.error(bind + " requires elevated privileges");
            process.exit(1);
            break;
        case "EADDRINUSE":
            console.error(bind + " is already in use");
            process.exit(1);
            break;
        default:
            throw error;
    }
}
function onListening() {
    var addr = httpServer.address();
    var bind = typeof addr === "string"
        ? "pipe " + addr
        : "port " + addr;
    console.log(`Listening on ${process.env.HOST}:${httpPort}${serverApollo.graphqlPath}`);
}
//# sourceMappingURL=app.js.map