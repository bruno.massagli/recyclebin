# **Recycle Bin Api**

Recycle Bin is an API for e-commerce based on recycling products.

Its Modules includes

- Categories
- Products
- Orders
- Authentication
- Companies

Here I show you the full bodies and its usages:
## **Category Module**
### **Category Body**

Used to manage companies categories, follows its full body
```
{
    _id: String,
    name: String,
    company: String,
    parent_id: String
}
```
### **Category Endpoints**
```
GET /categories/:id
GET /categories -q page&per_page
PUT /categories
POST /categories
GET /graphql
```

## **Orders Module**
### **Orders Body**

Used to manage companies Orders, follows its full body:
```
{
    company: String,
    order_number: String,
    order_date: String,
    total_gross: Float,
    status: String,
    shipping: {
      carrier: String,
      shipping_method: String,
      shipping_date: Date,
      shipping_code: String,
      shipping_cost: Float,
      shipping_estimated_delivery: Date,
      delivery_date: Date,
      shipping_address: {
        street: String,
        number: Int,
        detail: String,
        neighborhood: String,
        city: String,
        region: String,
        country: String,
        postcode: String
      },
      reason_cancelled: String,
      items: [
        {
          sku: String,
          name: String,
          quantity: Int,
          original_price: Float,
          special_price: Float
        }
      ],
      tracks: [],
      invoices: []
    },
    customer: {
      name: String,
      email: String,
      document: {
        number: String,
        type: String
      },
      phones: [String],
      address: {
        street: String,
        number: Int,
        detail: String,
        neighborhood: String,
        city: String,
        region: String,
        country: String,
        postcode: String
      }
    }
}
```
### **Orders Status Body**
```
Status Cancelled
{
    order_number: String,
    reason_cancelled: String
}

Status Invoiced
{
    order_number: String,
    key: String,
    number: String,
    invoiced_date: Date,
    items: [
        {
            _id : String
            sku: String
            name: String
            quantity: Float
            original_price: Float
            special_price: Float
        }
    ]
}

Status Sent
{
    order_number: String,
    carrier: String,
    method: String,
    tracking_number: String,
    tracking_link: String,
    date_delivery: Date,
    estimated_delivery: Date,
    items: [
        {
            _id : String
            sku: String
            name: String
            quantity: Float
            original_price: Float
            special_price: Float
        }
    ]
}

Status Delivered
{
    order_number: String,
    delivered_date: Date
} 
```
### **Orders Endpoints**
```
GET /customer/:email
GET /orders/:id
GET /orders -q page&per_page&status&start_date&end_date
POST /orders
PUT /orders/cancelled
PUT /orders/invoiced
PUT /orders/sent
PUT /orders/delivered
GET /graphql
```

## **Products Module**
### **Products Body**

Used to manage companies Products, follows its full body:
```
{
    name: String,
    company: String,
    id_product: String,
    brand: String,
    description: String,
    categories: [
      {
        _id: String,
        name: String,
        company: String,
        parent_id: String
      }
    ],
    warranty: String,
    variations: [
      {
        sku: String,
        id_product: String,
        stock: {
          quantity: Int,
          reserved: Int,
          unit: String
        },
        ean: String,
        nbm: String,
        prices: {
          list_price: Float,
          sale_price: Float
        },
        dimensions: {
           weight: Float, 
          length: Float,
          width: Float,
          height: Float,
          cubic_wheight: Float
        },
        images: [
          {
            name: String,
            url: String,
            id: String
          }
        ],
        videos: [
          {
            name: String,
            url: String,
            id: String
          }
        ],
        specifications: [
          {
            key: String,
            value: String
          }
        ]
      } 
    ]
}
```
### **Products Endpoints**
```
GET /products/:search -q page&per_page
GET /product/:id
GET /products -q page&per_page&start_date&end_date
POST /products
PUT /products
GET /graphql
```

## **Company Module**
### **Company Body** 

Used to manage general companies, follows its full body:

```
{
    name: String,
    email: String,
    api_key: String,
    document: {
        number: String,
        type: String
    },
    phones: [String],
    address: {
        street: String,
        number: Int,
        detail: String,
        neighborhood: String,
        city: String,
        region: String,
        country: String,
        postcode: String
    },
    avatar: {
      name: String,
      url: String,
      id: String
    },
    
    customers: [{
      _id: String,
      name: String,
      email: String,
      document: {
        number: String,
        type: String
      },
      phones: [String],
      address: {
        street: String,
        number: Int,
        detail: String,
        neighborhood: String,
        city: String,
        region: String,
        country: String,
        postcode: String
      }
    }]
}
```
### **Company Endpoints**
```
GET /company -q page&per_page
GET /company/:id
POST /company
PUT /company
GET /graphql
```



## **Auth Module**
### **Auth Body**

Used to manage general Users, follows its full body:

```
{
    email: String,
    password: String,
    access_level: String,
    customer: {
      name: String,
      email: String,
      document: {
        number: String,
        type: String
      },
      phones: [String],
      address: {
        street: String,
        number: Int,
        detail: String,
        neighborhood: String,
        city: String,
        region: String,
        country: String,
        postcode: String
      }
    }
}
```
### **Auth Endpoints**
```
GET /auth/:id
POST /auth
PUT /auth
GET /graphql
```

## Install Process

```
Install Npm with Node js
Clone the Repository
On Command Line: $npm i
On Comman Line: $npm run dev

Now you are ready to use the API
```

For more info here are reference links:

https://recycle-bin.herokuapp.com/ - Deploy domain

https://recycle-bin.herokuapp.com/graphql - GraphQl Api

In Development - Rest Documentation API