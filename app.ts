"use strict"
import "reflect-metadata"
require('dotenv').config();
//module dependencies
// require('rootpath')()

import express from 'express'
import graphqlHTTP from 'express-graphql'
// import { buildSchema } from 'graphql'
import { ApolloServer, makeExecutableSchema, gql } from 'apollo-server-express'
import logger from 'morgan'
import * as path from 'path'
import * as bodyParser from 'body-parser'
import * as bodyParserGraphQl from 'body-parser-graphql'
import methodOverride from 'method-override'
import {ProductsRouter} from './src/router/products/router'
import {OrdersRouter} from './src/router/orders/router'
import {CategoriesRouter} from './src/router/categories/router'
import {AuthRouter} from './src/router/auth/router'
import {resolverProducts} from './src/core/products/graphql/resolver/products_graphql'
import {resolverOrders} from './src/core/orders/graphql/resolver/orders_graphql'
import {resolverCategories} from './src/core/categories/graphql/resolver/categories_graphql'
import {resolverAuth} from './src/core/auth/graphql/resolver/auth_graphql'
import {types_graphql} from './src/core/general_types/index'
import * as server from "./src/server"
import debug from 'express'
import * as http from "http"
import {IndexDB} from './src/db/index'
import { merge } from 'lodash'

const router = express.Router()

//create http server
var httpPort = normalizePort(process.env.PORT || 3030)
var app = server.Server.bootstrap().app
app.set("port", httpPort)
var httpServer = http.createServer(app)


//listen on provided ports

//add error handler
httpServer.on("error", onError)

//start listening on port
httpServer.on("listening", onListening)

app.use(express.static(path.join(__dirname, 'public')))
app.use(logger('dev'))

app.use(bodyParser.json())

app.use(bodyParserGraphQl.graphql())

app.use(bodyParser.urlencoded({
  extended: true
}))

let product_routes = new ProductsRouter()
let orders_routes = new OrdersRouter()
let categories_routes = new CategoriesRouter()
let auth_routes = new AuthRouter()

app.use('/', product_routes.router)
app.use('/', orders_routes.router)
app.use('/', categories_routes.router)
app.use('/', auth_routes.router)

app.use(methodOverride())

// app.use('/graphql-products', graphqlHTTP({
//   schema: schemaProducts,
//   rootValue: resolverProducts,
//   graphiql: true
// }))

// app.use('/graphql-orders', graphqlHTTP({
//   schema: schemaOrders,
//   rootValue: resolverOrders,
//   graphiql: true
// }))

// app.use('/graphql-categories', graphqlHTTP({
//   schema: schemaCategories,
//   rootValue: resolverCategories,
//   graphiql: true
// }))

// app.use('/graphql-auth', graphqlHTTP({
//   schema: schemaAuth,
//   rootValue: resolverAuth,
//   graphiql: true,
// }))
let resolvers = {}
let execSchema = {
  typeDefs: gql`${types_graphql}`,
  resolvers: merge(resolvers, resolverProducts, resolverOrders, resolverCategories, resolverAuth)
}

const serverApollo = new ApolloServer({ 
  typeDefs: execSchema.typeDefs,
  resolvers: execSchema.resolvers,
  introspection: true,  
  playground: true
})

/**
 * Normalize a port into a number, string, or false.
 */

let db = new IndexDB().mongoose
.then(obj => {
  serverApollo.applyMiddleware({ app, cors: { credentials: true, origin: true} });
  httpServer.listen(httpPort)
  console.log('MongoDb Connected')
})
.catch(console.error)

function normalizePort(val): any {
  var port = parseInt(val, 10)

  if (isNaN(port)) {
    // named pipe
    return val
  }

  if (port >= 0) {
    // port number
    return port
  }

  return false
}

/**
 * Event listener for HTTP server "error" event.
 */
function onError(error): void {
  if (error.syscall !== "listen") {
    throw error
  }

  var bind = typeof httpPort === "string"
    ? "Pipe " + httpPort
    : "Port " + httpPort

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case "EACCES":
      console.error(bind + " requires elevated privileges")
      process.exit(1)
      break
    case "EADDRINUSE":
      console.error(bind + " is already in use")
      process.exit(1)
      break
    default:
      throw error
  }
}

/**
 * Event listener for HTTP server "listening" event.
 */
function onListening(): void {
  var addr = httpServer.address()
  var bind = typeof addr === "string"
    ? "pipe " + addr
    : "port " + addr
  console.log(`Listening on ${process.env.HOST}:${httpPort}${serverApollo.graphqlPath}` )
}
